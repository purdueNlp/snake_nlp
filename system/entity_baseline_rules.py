import os
from pprint import pprint
import numpy as np
from sklearn import metrics

# order is fixed, do not edit
PROTOCOLS = ['DCCP', 'GRE', 'IP', 'IPv6', 'SCTP', 'TCP']
FEAT_TEMPLATE = 'tmp/tmp-ment-{0}/examples'
MARGIN = 0

def check_feature(j, examples, classes):
    pos_on = 0; neg_on = 0; pos = 0; neg = 0
    for x in range(examples.shape[0]):
        if examples[x][j] == 1 and classes[x] == 1:
            pos_on += 1
        elif examples[x][j] == 1 and classes[x] == -1:
            neg_on += 1

        if classes[x] == 1:
            pos += 1
        elif classes[x] == -1:
            neg += 1

    return pos_on, pos, neg_on, neg

Y = []; Y_pred = []
for i, proto in enumerate(PROTOCOLS):
    print "Processing protocol", i, proto
    examples = []; classes = []
    filename = FEAT_TEMPLATE.format(i)
    with open(filename) as f:
        for line in f:
            features = [0]*17
            elements = line.strip().split()
            classes.append(int(elements[0]))
            for elem in elements[1:]:
                feat_id, feat_val = elem.split(':')
                features[int(feat_id)-10] = int(feat_val)
            examples.append(features)

    Y += classes
    X = np.asarray(examples); y = np.asarray(classes)
    print X.shape, y.shape

    if X.shape[0] == 0:
        continue

    weights = []
    for j in range(X.shape[1]):
        pos_on, pos, neg_on, neg = check_feature(j, X, y)
        pos_rate = float(pos_on) / (pos)
        neg_rate = float(neg_on) / (neg)


        # RB 1
        if pos_rate > (neg_rate + MARGIN):
            weights.append(pos_rate)
        elif pos_rate == 1 and neg_rate == 1:
            weights.append(-1)
        elif pos_rate < (neg_rate - MARGIN):
            weights.append(-neg_rate)
        else:
            weights.append(0)

        # RB 2
        '''
        if pos_rate > (neg_rate + MARGIN):
            weights.append(1)
        elif pos_rate == 1 and neg_rate == 1:
            weights.append(-1)
        elif pos_rate < (neg_rate - MARGIN):
            weights.append(-1)
        else:
            weights.append(0)
        '''


    print weights
    y_pred = []
    for x, y_ in zip(X, y):
        pred = np.dot(x, weights)
        if pred > 0:
            y_pred.append(1)
        else:
            y_pred.append(-1)

    Y_pred += y_pred
    y_pred = np.asarray(y_pred)

    #print metrics.classification_report(y, y_pred)

Y = np.asarray(Y); Y_pred = np.asarray(Y_pred)
print metrics.classification_report(Y, Y_pred)
tn, fp, fn, tp = metrics.confusion_matrix(Y, Y_pred).ravel()
print "TP", tp, "FP", fp
