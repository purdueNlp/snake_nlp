#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR

#Preprocessing (no entities yet)
python $DIR/run.py --preprocess $DIR/../Annotation/DCCP.annote.txt.short $DIR/../Annotation/TCP.annote.txt.short $DIR/../Annotation/IP.annote.txt.short $DIR/../Annotation/IPv6.annote.txt.short $DIR/../Annotation/GRE.annote.txt.short $DIR/../Annotation/SCTP.annote.txt.short

#Entity Extraction (i.e. no point in gold entities)
python $DIR/run.py --entities $DIR/tmp/pre/*.json
#-g for gold entities, nothing for gold extracted entities
python $DIR/run.py --mentions $DIR/tmp/ent/*.json

#-g for gold entities, nothing for gold extracted entities
$DIR/relationextract.sh -o $DIR/tmp/ $DIR/tmp/ent/*.json

cd $DIR/tmp/
mkdir -p rels
cd $DIR/tmp/rels
python $DIR/classify_relns.py -r $DIR/tmp/relations.json --unary --write

cd $DIR
python $DIR/run.py --formats $DIR/tmp/rels/*_pred.csv

echo "Done!"
echo "Remember to set the IP Protocol Number in the format files!"
