#!/bin/env python
import json
import numpy as np
import csv
import sys
from collections import Counter
import random
import optparse
from sklearn import svm, metrics

from relextractor.reltrigger_classifier import RelTriggerClassifier
from relextractor.binaryrel_classifier import BinaryRelClassifier
from relextractor import utils


def get_entity_def(reln):
    results = []
    for entdef in reln['entity_defs']:
        matched_entity = entdef
        for t in reln['pred_types']:
            results.append((reln, t, matched_entity))
    if len(reln['entity_defs']) == 0:
        for t in reln['pred_types']:
            results.append((reln, t, None))

    return results

def link_unary_relns(sect_predicted_relns, sect_entities, unary_rlns):
    results = []
    for sect_id in range(len(sect_predicted_relns)):
        for reln in sect_predicted_relns[sect_id]:
            #print reln['pred_types']
            if len(reln['pred_types'] & unary_rlns) > 0:
                results += get_entity_def(reln)

    return results


def false_positives(test_y, pred_y):
    # calculate the metrics that we care about
    n_fake_chunks = len([1 for (y_p, y_g) in zip(pred_y, test_y) \
                         if y_p == 1 and y_g == 0])
    n_chunks = len(test_y)
    return n_fake_chunks, n_chunks

def relation_metrics(compressed_results, gold_tuples):

    relations_found = Counter([r[0] for r in compressed_results])
    relations_gold = Counter([r[0] for r in gold_tuples])

    relations_type_found = Counter([(r[0], r[1]) for r in compressed_results])
    relations_type_gold = Counter([(r[0], r[1]) for r in gold_tuples])

    '''
    not_found = 0
    for rel in relations_gold:
        not_found += max(0, relations_gold[rel] - relations_found[rel])
    n_reln_identif = len(gold_tuples) - not_found

    not_found = 0
    for rel in relations_type_gold:
        not_found += max(0, relations_type_gold[rel] - relations_type_found[rel])
    n_reln_identif_type = len(gold_tuples) - not_found
    '''
    not_found = 0; total = 0
    for rel in relations_gold:
        if rel not in relations_found:
            #print rel
            not_found += 1
        total += 1
    n_reln_identif = total - not_found

    not_found = 0; total = 0
    for rel in relations_type_gold:
        if rel not in relations_type_found:
            #print rel
            not_found += 1
        total += 1
    n_reln_identif_type = total - not_found

    return n_reln_identif, n_reln_identif_type, total

def write_to_file(csvfilename, results):
    with open(csvfilename, 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=";")
        spamwriter.writerow(["#section", "reln_sent", "reln_beg_char", "reln_beg_word", "reln_end_char",
                             "reln_end_word", "reln_type", "entity_type_UNARY", "entity_type_A", "entity_type_B", "full_sentence"])
        for r in results:

            entity_type_A = "None"
            entity_type_B = "None"
            entity_type_UNARY = "None"
            if len(r) == 3: # unary case
                if r[2] is not None:
                    entity_type_UNARY = r[2]
            elif len(r) == 4: # binary case
                if r[2] is not None:
                    entity_type_A = r[2]
                if r[3] is not None:
                    entity_type_B = r[3]

            # r[1] is the predicted type
            spamwriter.writerow([r[0]["section"], r[0]['sent'], r[0]['begin_char'], r[0]['begin_word'],
                                 r[0]['end_char'], r[0]['end_word'], r[1],
                                 entity_type_UNARY, entity_type_A, entity_type_B, r[0]['fsent']])

def parse_args():
    parser = optparse.OptionParser()
    parser.add_option('-r', help='relations file', dest='data', type='string')
    parser.add_option('-b', '--binary', help='extract binary relations', dest='include_binary',
                      default=False, action='store_true')
    parser.add_option('-u', '--unary', help='extract unary relations', dest='include_unary',
                      default=False, action='store_true')
    parser.add_option('-w', '--write', help='write CSV output', dest='write_output',
                      default=False, action='store_true')
    (opts, args) = parser.parse_args()

    # Making sure all mandatory options appeared
    mandatories = ['data']
    for m in mandatories:
        if not opts.__dict__[m]:
            print("mandatory option is missing\n)")
            parser.print_help()
            exit(-1)

    return opts


def main():

    opts = parse_args()

    with open(opts.data, 'rb') as f:
        dataset = json.loads(f.read())

        #
    # iterate per folds
    n_reln_ALL = 0; n_reln_identif_ALL = 0;
    n_reln_identif_type_ALL = 0; n_fake_chunks_ALL = 0
    n_chunks_ALL = 0
    n_found_pred_single_ALL = 0
    n_found_pred_all_ALL = 0
    n_found_gold_single_ALL = 0
    n_found_gold_all_ALL = 0
    n_total_single_ALL = 0
    n_total_all_ALL = 0
    n_reln_mentions_ALL = 0
    n_extra_pred_single_ALL = 0

    reltrigger = RelTriggerClassifier()
    binaryrels = BinaryRelClassifier()

    for i in range(len(dataset['files'])):
        print("fold", i, dataset['files'][i])

        # obtain train and test data for current fold
        train_files = [dataset['files'][j] \
                        for j in range(len(dataset['files'])) if j != i]
        test_file = dataset['files'][i]

        print("Training trigger classifier...")
        reltrigger.train(dataset, train_files)
        print("Predicting triggers...")
        test_y, test_y_types, pred_y, pred_y_types = \
                reltrigger.predict(dataset, test_file, opts.include_unary,
                                   opts.include_binary)

        # Get gold data to be able to extract metrics
        print("Gathering expected results...")
        sect_gold_relns, sect_entities = \
                utils.get_relations_and_entities(dataset, test_file, test_y,
                                                 test_y_types)
        gold_tuples_single, gold_tuples_all = \
                utils.get_gold_tuples(sect_gold_relns,
                                      reltrigger.ontology.UNARY_RLNS,
                                      reltrigger.ontology.BINARY_RLNS,
                                      include_binary=opts.include_binary,
                                      include_unary=opts.include_unary)
        results_gold = utils.mock_results(sect_gold_relns)

        unary_single = set([]); unary_all = set([])
        binary_single = set([]); binary_all = set([])
        unary_results_pred = []; binary_results_pred = []

        if opts.include_unary:
            print("Linking unary relations...")
            sect_predicted_relns, sect_entities = \
                utils.get_relations_and_entities(dataset, test_file,
                                                 pred_y, pred_y_types)
            unary_results_pred = link_unary_relns(sect_predicted_relns, sect_entities,
                                                  reltrigger.ontology.UNARY_RLNS)
            unary_single, unary_all = utils.compress_results(unary_results_pred)

        if opts.include_binary:
            print("Training binary linker...")
            binaryrels.train(dataset, train_files)
            # predict over predicted relations
            # binaryrel_preds = binaryrels.predict(dataset, test_file, pred_y, pred_y_types)

            # predict over gold relations
            print("Predicting binary links...")
            binary_single, binary_all, binary_results_pred \
                    = binaryrels.predict(dataset, test_file, test_y, test_y_types)

        # Aggregate results
        pred_all = unary_all.union(binary_all)
        pred_single= unary_single.union(binary_single)
        results_pred = unary_results_pred + binary_results_pred

        # Calculate and report metrics ---------
        print("Calculating metrics...")

        # Measure how good we did on the trigger
        n_fake_chunks, n_chunks = false_positives(test_y, pred_y)
        n_reln_identif, n_reln_identif_type, n_reln_mentions = relation_metrics(pred_all, gold_tuples_all)

        n_found_pred_single = len(gold_tuples_single & pred_single)
        n_extra_pred_single = len(pred_single - gold_tuples_single)
        n_total_single = len(gold_tuples_single)

        '''
        print(pred_all)
        print
        print(gold_tuples_all)
        exit()
        '''

        n_found_pred_all = len(gold_tuples_all & pred_all)
        n_total_all = len(gold_tuples_all)

        # aggregate results
        n_reln_identif_ALL += n_reln_identif
        n_reln_identif_type_ALL += n_reln_identif_type
        n_fake_chunks_ALL += n_fake_chunks
        n_chunks_ALL += n_chunks
        n_found_pred_single_ALL += n_found_pred_single
        n_found_pred_all_ALL += n_found_pred_all
        n_total_single_ALL += n_total_single
        n_total_all_ALL += n_total_all
        n_reln_mentions_ALL += n_reln_mentions
        n_extra_pred_single_ALL += n_extra_pred_single

        print("RESULTS")
        if n_reln_mentions == 0:
            print("overlap_recall", "N/A")
            print("overlap_type_recall", "N/A")
        else:
            print("overlap_recall", n_reln_identif / (1.0 * n_reln_mentions))
            print("overlap_type_recall", n_reln_identif_type / (1.0 * n_reln_mentions))
        print("noise_ratio", n_fake_chunks / (1.0 * n_chunks))
        print("n_reln", n_reln_mentions, "n_reln_identif", n_reln_identif, "n_reln_identif_type", n_reln_identif_type)

        if n_total_all == 0:
            print("found_link_pred_all", "N/A")
        else:
            print("found_link_pred_all", n_found_pred_all / (1.0 * n_total_all))
            print("linked", n_found_pred_all, "instances", n_total_all)
            print("---- Snake type eval ----")
            print("found_link_pred_single", n_found_pred_single / (1.0 * n_total_single))
            print("gold # of relations", n_total_single)
            print("single # of found relns", n_found_pred_single)
            print("noise in link prediction (# of relations)", n_extra_pred_single)
            print()

        if opts.write_output:
            # if flag is ON, dump results to CSV
            csvfilename = test_file[:-10] + "_results_pred.csv"
            write_to_file(csvfilename, results_pred)
            # dump gold annotations too
            csvfilename = test_file[:-10] + "_results_gold.csv"
            write_to_file(csvfilename, results_gold)



    print()
    print("TOTAL RESULTS")
    print("overlap_recall", n_reln_identif_ALL / (1.0 * n_reln_mentions_ALL))
    print("overlap_type_recall", n_reln_identif_type_ALL / (1.0 * n_reln_mentions_ALL))
    print("noise_ratio", n_fake_chunks_ALL / (1.0 * n_chunks_ALL))
    print("n_reln", n_reln_mentions_ALL, "n_reln_identif", n_reln_identif_ALL, "n_reln_identif_type", n_reln_identif_type_ALL)
    print("found_link_gold_all", n_found_gold_all_ALL / (1.0 * n_total_all_ALL))
    print("found_link_pred_all", n_found_pred_all_ALL / (1.0 * n_total_all_ALL))
    print("linked", n_found_pred_all_ALL, "instances", n_total_all_ALL)
    print("---- Snake type eval ----")
    #print("found_link_gold_single", n_found_gold_single_ALL / (1.0 * n_total_single_ALL))
    print("found_link_pred_single", n_found_pred_single_ALL / (1.0 * n_total_single_ALL))
    print("gold # of relations", n_total_single_ALL)
    print("single # of found relns", n_found_pred_single_ALL)
    print("noise in link prediction (# of relations)", n_extra_pred_single_ALL)
    print()


if __name__ == "__main__":
    main()
