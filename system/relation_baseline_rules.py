import json
import os
from pprint import pprint
import optparse 
from collections import Counter
from sklearn import metrics
import random
import numpy as np
MARGIN = 0

UNARY_RLNS = set(['r_sequence_number', 'r_checksum', 'r_port', 'r_packet_type',
                  'r_header_length', 'r_multiple', 'r_mbz', 'r_monotonic_inc', 'r_range'])

BINARY_RLNS = set(['r_contains', 'r_field_present', 'r_significant', 'r_offset',
                   'r_greater', 'r_less'])

KEYWORDS = \
        {'r_sequence_number': ['data octect', 'sequence number', 'acknowledgement number'],
         'r_checksum': ['checksum'],
         'r_port': ['port'],
         'r_packet_type': ['type', 'packet type', 'packet', 'control'],
         'r_header_length': ['length', 'header', 'data offset', 'size'],
         'r_multiple': ['integral number', 'multiple'],
         'r_mbz': ['must be zero', 'reserved', 'zeros', 'zero', "zeroes", '0'],
         'r_monotonic_inc': ['should not be lessened'],
         'r_contains': ['from left to right', 'these parameters'],
         'r_field_present': ['contain', 'carry', 'present', 'following format', 'following parameters', 'packets'],
         'r_significant': ['signficant', 'only interpreted with', '=', 'valid'],
         'r_offset': ['offset', 'indicated in'],
         'r_greater': ['greater than'],
         'r_less': ['must not be greater', 'less'],
         'r_range': ['takes value of', 'range']}

BOW = \
        {'r_sequence_number': ['data', 'octect', 'sequence', 'number', 'acknowledgement', 'number'],
         'r_checksum': ['checksum'],
         'r_port': ['port'],
         'r_packet_type': ['type', 'packet'],
         'r_header_length': ['length', 'header', 'data', 'offset'],
         'r_multiple': ['integral', 'number'],
         'r_mbz': ['zero', 'reserved', 'zeros', "zeroes", '0'],
         'r_monotonic_inc': ['lessened'],
         'r_contains': ['left', 'right', 'parameters'],
         'r_field_present': ['contain', 'carry', 'present'],
         'r_significant': ['signficant', 'interpreted'],
         'r_offset': ['offset', 'indicated'],
         'r_greater': ['greater'],
         'r_less': ['less'],
         'r_range': ['value', 'range']}

def get_greatest_overlap_perc(chunk, key_phrases):
    maximum = 0.0
    for kp in key_phrases:
        overlap = get_overlap_perc(chunk, kp.split())
        if overlap > maximum:
            maximum = overlap
    return maximum

def has_overlap(chunk, key_phrases):
    for kp in key_phrases:
        overlap = get_overlap_perc(chunk, kp.split())
        if overlap > 1:
            return 1
    return 0

def is_keyword_subsequence(chunk, key_phrases):
    for kp in key_phrases:
        if (kp in chunk.lower()):
            return 1
    return 0

def is_exact_match(chunk, key_phrases):
    for kp in key_phrases:
        if (kp == chunk.lower()):
            return 1
    return 0

def false_positives(test_y, pred_y):
    # calculate the metrics that we care about
    n_fake_chunks = len([1 for (y_p, y_g) in zip(pred_y, test_y) \
                         if y_p == 1 and y_g == 0])
    n_chunks = len(test_y)
    return n_fake_chunks, n_chunks

def relation_metrics(compressed_results, gold_tuples):
    relations_found = Counter([r[0] for r in compressed_results])
    relations_gold = Counter([r[0] for r in gold_tuples])

    relations_type_found = Counter([(r[0], r[1]) for r in compressed_results])
    relations_type_gold = Counter([(r[0], r[1]) for r in gold_tuples])

    '''
    not_found = 0
    for rel in relations_gold:
        not_found += max(0, relations_gold[rel] - relations_found[rel])
    n_reln_identif = len(gold_tuples) - not_found

    not_found = 0
    for rel in relations_type_gold:
        not_found += max(0, relations_type_gold[rel] - relations_type_found[rel])
    n_reln_identif_type = len(gold_tuples) - not_found
    '''
    not_found = 0; total = 0
    for rel in relations_gold:
        if rel not in relations_found:
            not_found += 1
        total += 1
    n_reln_identif = total - not_found

    not_found = 0; total = 0
    for rel in relations_type_gold:
        if rel not in relations_type_found:
            not_found += 1
        total += 1
    n_reln_identif_type = total - not_found

    return n_reln_identif, n_reln_identif_type, total


def get_all_keyphrases(include_binary, include_unary):
    key_phrases = []
    for key in KEYWORDS:
        if (include_binary and key in BINARY_RLNS) or\
            (include_unary and key in UNARY_RLNS):
            for value in KEYWORDS[key]:
                key_phrases.append(value)
    return key_phrases

def longest_common_substring(s1, s2):
   #print s1, "|", realid, s2
   m = [[0] * (1 + len(s2)) for i in xrange(1 + len(s1))]
   longest, x_longest = 0, 0
   for x in xrange(1, 1 + len(s1)):
       for y in xrange(1, 1 + len(s2)):
           if s1[x - 1] == s2[y - 1]:
               m[x][y] = m[x - 1][y - 1] + 1
               if m[x][y] > longest:
                   longest = m[x][y]
                   x_longest = x
           else:
               m[x][y] = 0
   return s1[x_longest - longest: x_longest]

def get_overlap_perc(chunk, key_phrase):
    lcs = longest_common_substring(chunk, key_phrase)
    lcs_size = len(lcs)

    #if lcs_size > 0:
    #    print lcs

    return (1.0 * lcs_size) / len(key_phrase)

def overlap_index(words1, words2):
    num = len(set(words1) & set(words2))
    denom = min(len(set(words1)), len(set(words2)))
    return (1.0 * num) / denom

def best_match_types(dataset, test_file, pred_y, include_binary=False, include_unary=False):
    pred_y_types = []; pred_index = 0
    for section in dataset[test_file]['sections']:
        for chunk in section['chunks']:

            if pred_y[pred_index] == 1:
                reln_type = best_match(chunk['words'], include_binary, include_unary)
                pred_y_types.append(reln_type)
            else:
                pred_y_types.append(set([]))
            pred_index += 1

    return pred_y_types

def get_relations_and_entities(dataset, test_file, pred_y, pred_y_types, test_y):
    sect_predicted_relns = []; sect_entities = []
    pred_index = 0

    for section in dataset[test_file]['sections']:
        for chunk in section['chunks']:
            predicted_relns = []; entities = []

            if pred_y[pred_index] == 1 and test_y[pred_index] == 1:
                chunk['pred_type'] = pred_y_types[pred_index]
                predicted_relns.append(chunk)
            if 'ann_ent_id' in chunk:
                entities.append(chunk)
            pred_index += 1

            sect_entities.append(entities)
            sect_predicted_relns.append(predicted_relns)

    return sect_predicted_relns, sect_entities

def get_gold_tuples(sect_relns, include_binary=False, include_unary=False):
    tuples_single = set([]); tuples_all = set([])
    results = []
    for sect_id in range(len(sect_relns)):
        for reln in sect_relns[sect_id]:
            if reln['ann_rel'] in UNARY_RLNS and include_unary:
                for j, ent in enumerate(reln['ann_to_ent'].split(',')):
                    if ent != '':
                        tuples_single.add((reln['ann_rel'], int(ent)))
                        tuples_all.add((reln['ann_rel_id'], reln['ann_rel'], int(ent)))

            elif reln['ann_rel'] in BINARY_RLNS and include_binary:
                if 'ann_to_ent_A' in reln and 'ann_to_ent_B' in reln:
                    for ent_A in reln['ann_to_ent_A'].split(','):
                        for ent_B in reln['ann_to_ent_B'].split(','):
                            if ent_A != '' and ent_B != '':
                                tuples_single.add((reln['ann_rel'], int(ent_A), int(ent_B)))
                                tuples_all.add((reln['ann_rel_id'], reln['ann_rel'], int(ent_A), int(ent_B)))
                else:
                    tuples_single.add((reln['ann_rel'], None, None))
                    tuples_all.add((reln['ann_rel_id'], reln['ann_rel'], None, None))
    return tuples_single, tuples_all

def get_results(sect_predicted_relns, sect_entities):
    results = []
    for sect_id in range(len(sect_predicted_relns)):
        for reln in sect_predicted_relns[sect_id]:
            #print reln['pred_type']
            if len(reln['pred_type'] & UNARY_RLNS) > 0:
                results += get_entity_def(reln)
            elif len(reln['pred_type'] & BINARY_RLNS) > 0:
                results += get_closest_left_right(reln, sect_entities[sect_id])
    return results

def compress_results(results):
    compressed_single = set([]); compressed_all = set([])
    for r in results:
        if len(r) == 2:
            for _type in r[0]['pred_type']:
                compressed_single.add((_type, r[1]))
                if 'ann_rel_id' in r[0]:
                    compressed_all.add((r[0]['ann_rel_id'], _type, r[1]))
        if len(r) == 3:
            for _type in r[0]['pred_type']:
                compressed_single.add((_type, r[1], r[2]))
                if 'ann_rel_id' in r[0]:
                    compressed_all.add((r[0]['ann_rel_id'], _type, r[1], r[2]))
    return compressed_single, compressed_all

def get_entity_def(reln):
    results = []
    for entdef in reln['entity_defs']:
        matched_entity = entdef
        results.append((reln, matched_entity))
    if len(reln['entity_defs']) == 0:
        results.append((reln, None))
    return results

# Right now we only match one A and B for a binary rel
# How do we deal with the cases where there are multiple?
def get_closest_left_right(reln, sect_entities):
    results = []
    matched_left = None; matched_right = None
    closest_distance_left = float('inf')
    closest_distance_right = float('inf')

    for ent in sect_entities:
        distance = (reln['id'] - ent['id'])
        if distance > 0:
            #left
            if distance < closest_distance_left:
                closest_distance_left = distance
                matched_left = ent
        elif distance < 0:
            #right
            if abs(distance) < closest_distance_right:
                closest_distance_right = abs(distance)
                matched_right = ent
    results.append((reln, matched_left, matched_right))
    return results

def best_match(words, include_binary=False, include_unary=False):
    words = [w.lower() for w in words]
    maxim = 0
    max_relns = set([])

    scores = {}

    for reln in KEYWORDS:
        if (include_binary and reln in BINARY_RLNS) or (include_unary and reln in UNARY_RLNS):
            scores[reln] = 0
            for keyw in KEYWORDS[reln]:
                overlap = overlap_index(words, keyw.split())
                #print reln, overlap

                if overlap >= maxim:
                    scores[reln] = overlap
                    #print "HERE", reln
                    maxim = overlap
                    #print "MAX_RELN", max_reln

            if scores[reln] == 0:
                for syns in BOW[reln]:
                    overlap = overlap_index(words, syns.split())
                    if overlap >= maxim:
                        scores[reln] = overlap
                        maxim = overlap

    for reln in KEYWORDS:
        if (include_binary and reln in BINARY_RLNS) or (include_unary and reln in UNARY_RLNS):
            if scores[reln] == maxim and maxim != 0:
                max_relns.add(reln)

    # if no relation type was found, choose a random one
    if len(max_relns) == 0:
        rand_rel = random.randint(0, len(KEYWORDS) - 1)
        max_relns.add(KEYWORDS.keys()[rand_rel])
    return max_relns

def has_degree_overlap(chunk, key_phrases, degree):
    greatest_overlap = get_greatest_overlap_perc(chunk, key_phrases)
    if greatest_overlap >= degree:
        return 1
    else:
        return 0

def chunk_features(chunk, key_phrases, pos_phrases, synwords):
    words = [w.lower() for w in chunk['words']]
    kp_overlap = get_greatest_overlap_perc(words, key_phrases)
    #key_phraseskp_overlap = 0
    hs_50_overlap = has_degree_overlap(words, key_phrases, 0.50)
    hs_70_overlap = has_degree_overlap(words, key_phrases, 0.70)
    hs_85_overlap = has_degree_overlap(words, key_phrases, 0.85)
    exact_match = has_degree_overlap(words, key_phrases, 1.0)
    #exact_match = 0
    #hs_50_overlap = 0
    hs_overlap = has_overlap(words, key_phrases)
    #a lhs_overlap = 0
    is_subseq = is_keyword_subsequence(" ".join(words), key_phrases)
    #is_subseq = 0
    length = len(words)
    #length = 0
    '''
    syn_overlap = get_greatest_overlap_perc(words, synwords)
    hs_syn_overlap = has_overlap(words, synwords)
    is_subseq_syn = is_keyword_subsequence(" ".join(words), synwords)
    exact_match_syn = is_exact_match(" ".join(words), synwords)

    '''
    return kp_overlap, hs_overlap, is_subseq,\
           exact_match, length, hs_50_overlap, hs_70_overlap, hs_85_overlap

def featurize_file(dataset, filename, key_phrases, pos_phrases, synwords,
                   include_binary=False, include_unary=False):
    print "\tconstructing ", filename
    X = []; y = []; y_types = []
    for section in dataset[filename]['sections']:
        for i in range(len(section['chunks'])):

            # previous chunk
            if i - 1 > 0:
                chunk = section['chunks'][i - 1]
                kp_overlap_prev, hs_overlap_prev, is_subseq_prev,\
                exact_match_prev, len_prev, hs_50_overlap_prev,\
                hs_70_overlap_prev, hs_85_overlap_prev =\
                    chunk_features(chunk, key_phrases, pos_phrases, synwords)
            else:
                kp_overlap_prev = 0.0; hs_overlap_prev = 0; is_subseq_prev = 0
                exact_match_prev = 0; len_prev = 0; hs_50_overlap_prev = 0
                hs_70_overlap_prev = 0; hs_85_overlap_prev = 0
                #syn_overlap_prev = 0.0; hs_syn_overlap_prev = 0
                #is_subseq_syn_prev = 0; exact_match_syn_prev = 0

            # next chunk
            if i + 1 < len(section['chunks']) - 1:
                chunk = section['chunks'][i + 1]
                kp_overlap_next, hs_overlap_next, is_subseq_next,\
                exact_match_next, len_next, hs_50_overlap_next,\
                hs_70_overlap_next, hs_85_overlap_next =\
                    chunk_features(chunk, key_phrases, pos_phrases, synwords)
            else:
                kp_overlap_next = 0.0; hs_overlap_next = 0; is_subseq_next = 0
                exact_match_next = 0; len_next = 0; hs_50_overlap_next = 0
                hs_70_overlap_next = 0; hs_85_overlap_next = 0
                #syn_overlap_next = 0.0; hs_syn_overlap_next = 0
                #is_subseq_syn_next = 0; exact_match_syn_next = 0

            # current chunk
            chunk = section['chunks'][i]
            kp_overlap, hs_overlap, is_subseq, exact_match, len_curr, hs_50_overlap,\
            hs_70_overlap, hs_85_overlap =\
                chunk_features(chunk, key_phrases, pos_phrases, synwords)

            X.append([is_subseq, hs_overlap, exact_match, kp_overlap,
                      is_subseq_prev, hs_overlap_prev, exact_match_prev, kp_overlap_prev,
                      is_subseq_next, hs_overlap_next, exact_match_next, kp_overlap_next,
                      len_prev, len_curr, len_next,
                      hs_50_overlap_prev, hs_50_overlap, hs_50_overlap_next,
                      hs_70_overlap_prev, hs_70_overlap, hs_70_overlap_next,
                      hs_85_overlap_prev, hs_85_overlap, hs_85_overlap_next])
                      #is_subseq_syn, hs_syn_overlap, exact_match_syn, syn_overlap,
                      #is_subseq_syn_prev, hs_syn_overlap_prev, exact_match_syn_prev, syn_overlap_prev,
                      #is_subseq_syn_next, hs_syn_overlap_next, exact_match_syn_next, syn_overlap_next])

            # indicates whether there is a relation in this chunk
            if include_binary and include_unary:
                y.append(int('ann_rel' in chunk))
            elif include_unary and not include_binary:
                y.append(int('ann_rel' in chunk and chunk['ann_rel'] in UNARY_RLNS))
            elif include_binary and not include_unary:
                y.append(int('ann_rel' in chunk and chunk['ann_rel'] in BINARY_RLNS))
            else:
                y.append(0)

            # gets the actual type of the rel, type is empty string if no reln in chunk
            if 'ann_rel' in chunk and include_binary and include_unary:
                y_types.append(set([chunk['ann_rel']]))
            elif 'ann_rel' in chunk and include_unary and not include_binary:
                if chunk['ann_rel'] in UNARY_RLNS:
                    y_types.append(set([chunk['ann_rel']]))
                else:
                    y_types.append(set([]))
            elif 'ann_rel' in chunk and include_binary and not include_unary:
                if chunk['ann_rel'] in BINARY_RLNS:
                    y_types.append(set([chunk['ann_rel']]))
                else:
                    y_types.append(set([]))
            else:
                y_types.append(set([]))

    return X, y, y_types


def check_feature(j, examples, classes):
    pos_on = 0; neg_on = 0; pos = 0; neg = 0
    for x in range(examples.shape[0]):
        if examples[x][j] == 1 and classes[x] == 1:
            pos_on += 1
        elif examples[x][j] == 1 and classes[x] == 0:
            neg_on += 1

        if classes[x] == 1:
            pos += 1
        elif classes[x] == 0:
            neg += 1
    print pos_on, pos, neg_on, neg
    return pos_on, pos, neg_on, neg

def main():
    parser = optparse.OptionParser()
    parser.add_option('-r', help='relations file', dest='data', type='string')
    parser.add_option('-b', '--binary', help='extract binary relations', dest='include_binary',
                      default=False, action='store_true')
    parser.add_option('-u', '--unary', help='extract unary relations', dest='include_unary',
                      default=False, action='store_true')

    parser.add_option('-m', help='rule based mode [1|2]', dest='mode', type='int')
    (opts, args) = parser.parse_args()

    # Making sure all mandatory options appeared
    mandatories = ['data', 'mode']
    for m in mandatories:
        if not opts.__dict__[m]:
            print "mandatory option is missing\n"
            parser.print_help()
            exit(-1)

    with open(opts.data, 'rb') as f:
        dataset = json.loads(f.read())

    key_phrases = get_all_keyphrases(include_binary=opts.include_binary,
                                     include_unary=opts.include_unary)

    n_reln_ALL = 0; n_reln_identif_ALL = 0;
    n_reln_identif_type_ALL = 0; n_fake_chunks_ALL = 0
    n_reln_mentions_ALL = 0; n_chunks_ALL = 0

    for i in range(len(dataset['files'])):
        print "fold", i, dataset['files'][i]
        test_file = dataset['files'][i]

        # construct test vectorized data
        test_X, test_y, test_y_types = featurize_file(dataset, test_file,
                                                      key_phrases, None,
                                                      None, include_binary=opts.include_binary,
                                                      include_unary=opts.include_unary)

        weights = []
        test_X = np.asarray(test_X)
        test_y = np.asarray(test_y)

        for j in range(test_X.shape[1]):
            pos_on, pos, neg_on, neg = check_feature(j, test_X, test_y)
            if pos == 0:
                pos_rate = 0.0
            else:
                pos_rate = float(pos_on) / (pos)
            if neg == 0:
                neg_rate = 0.0
            else:
                neg_rate = float(neg_on) / (neg)


            # RB 2
            if opts.mode == 1:
                if pos_rate > (neg_rate + MARGIN):
                    weights.append(pos_rate)
                elif pos_rate == 1 and neg_rate == 1:
                    weights.append(-1)
                elif pos_rate < (neg_rate - MARGIN):
                    weights.append(-neg_rate)
                else:
                    weights.append(0)

            elif opts.mode == 2:
                if pos_rate > (neg_rate + MARGIN):
                    weights.append(1)
                elif pos_rate == 1 and neg_rate == 1:
                    weights.append(-1)
                elif pos_rate < (neg_rate - MARGIN):
                    weights.append(-1)
                else:
                    weights.append(0)
            else:
                "Invalid rule based mode, use 1 or 2"
                exit()

        pred_y = []
        for x, y_ in zip(test_X, test_y):
            pred = np.dot(x, weights)
            if pred > 0:
                pred_y.append(1)
            else:
                pred_y.append(0)

        pred_y = np.asarray(pred_y)


        pred_y_types = best_match_types(dataset, test_file, pred_y,
                                include_binary=opts.include_binary,
                                include_unary=opts.include_unary)


        #print metrics.classification_report(test_y, pred_y)

        n_fake_chunks, n_chunks = false_positives(test_y, pred_y)

        sect_predicted_relns, sect_entities = \
                get_relations_and_entities(dataset, test_file, pred_y, pred_y_types, test_y)
        sect_gold_relns, sect_entities = \
                get_relations_and_entities(dataset, test_file, test_y, test_y_types, test_y)
        gold_tuples_single, gold_tuples_all = get_gold_tuples(sect_gold_relns,
                                                              include_binary=opts.include_binary,
                                                              include_unary=opts.include_unary)

        results_pred = get_results(sect_predicted_relns, sect_entities)
        results_gold = get_results(sect_gold_relns, sect_entities)

        compressed_pred_single, compressed_pred_all = compress_results(results_pred)
        compressed_gold_single, compressed_gold_all = compress_results(results_gold)

        n_reln_identif, n_reln_identif_type, n_reln_mentions = relation_metrics(compressed_pred_all, gold_tuples_all)

        # aggregate results
        n_reln_identif_ALL += n_reln_identif
        n_reln_identif_type_ALL += n_reln_identif_type
        n_fake_chunks_ALL += n_fake_chunks
        n_chunks_ALL += n_chunks
        n_reln_mentions_ALL += n_reln_mentions

        print "RESULTS"
        if n_reln_mentions == 0:
            print "overlap_recall", "N/A"
            print "overlap_type_recall", "N/A"
        else:
            print "overlap_recall", n_reln_identif / (1.0 * n_reln_mentions)
            print "overlap_type_recall", n_reln_identif_type / (1.0 * n_reln_mentions)
        print "noise_ratio", n_fake_chunks / (1.0 * n_chunks)
        print "n_reln", n_reln_mentions, "n_reln_identif", n_reln_identif, "n_reln_identif_type", n_reln_identif_type


    print
    print "TOTAL RESULTS"
    print "overlap_recall", n_reln_identif_ALL / (1.0 * n_reln_mentions_ALL)
    print "overlap_type_recall", n_reln_identif_type_ALL / (1.0 * n_reln_mentions_ALL)
    print "noise_ratio", n_fake_chunks_ALL / (1.0 * n_chunks_ALL)
    print "n_reln", n_reln_mentions_ALL, "n_reln_identif", n_reln_identif_ALL, "n_reln_identif_type", n_reln_identif_type_ALL

if __name__ == "__main__":
    main()
