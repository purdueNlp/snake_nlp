#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

exec java -Xmx8g -cp $DIR/bin/:$DIR/lib/libsvm-3.21/java/*:$DIR/lib/mallet-2.0.8RC3/*:$DIR/lib/stanford-corenlp-full-2015-12-09/*:$DIR/lib/json-simple-1.1.1.jar nlp.snake.io.MainPreProcess $@
