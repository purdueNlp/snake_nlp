#!/bin/env python
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# Samuel Jero <sjero@purdue.edu>
import os
import sys
import shutil
import argparse
import itertools
import time
import subprocess
import shlex

mydir = os.path.dirname(os.path.realpath(__file__))
svmlightdir = os.path.abspath(os.path.join(os.path.join(mydir, "lib"),"svm_light"))
snakeconvertdir = os.path.abspath(os.path.join(os.path.join(mydir, ".."),"snake"))

def accuracy(true_positive, true_negative, total):
        if total <= 0:
                return 0
        return (true_positive + true_negative) / (total *1.0)

def precision(true_positive, false_positive):
        if true_positive + false_positive <= 0:
                return 0
        return true_positive / ((true_positive + false_positive)*1.0)

def recall(true_positive, false_negative):
        if true_positive + false_negative <= 0:
                return 0
        return true_positive / ((true_positive + false_negative)*1.0)

def fscore(true_positive, false_positive, false_negative):
        p = precision(true_positive, false_positive)
        r = recall(true_positive, false_negative)
        if p <= 0 or r <= 0:
            return 0
        return 2*((p*r)/(p+r))

def mentions(files, use_gold):
    #Create final output directories
    tmpdir = os.path.join(mydir, "tmp")
    mentdir = os.path.abspath(os.path.join(tmpdir, "ment"))
    if os.path.exists(mentdir):
        shutil.rmtree(mentdir)
    os.mkdir(mentdir)
    
    #Cross validation
    records = []
    for i in range(0,len(files)):
        #Create partial results directory
        tedir = os.path.abspath(os.path.join(tmpdir, "tmp-ment-%i" % i))
        if os.path.exists(tedir):
            shutil.rmtree(tedir)
        os.mkdir(tedir)

        #split files
        testf = files[i]
        trainf = [x for x in files if not x == testf]

        #Train model
        goldstr = ""
        filestr = ""
        for f in trainf:
            filestr += " %s " % f
        if use_gold == True:
            goldstr = " -g "
        model = "-m " + tedir + "/model"
        cmd = mydir + "/entitymentiontrain.sh -e " + goldstr + " -b " + svmlightdir + "/ -t " + tedir + "/ " + model + filestr
        output = subprocess.check_output(shlex.split(cmd))

        lines = output.split(b"\n")
        for i in range(1,len(lines)):
            if lines[i].find(b"Entity Reference Detection Evaluation") >= 0 and i + 11  < len(lines):
                r = parse_entities(lines[i:i+11],str(trainf),"Training")
                print_record(r)

        #Classify
        goldstr = ""
        if use_gold == True:
            goldstr = " -g "
        out = " -o %s/" % str(mentdir)
        model = "-m " + tedir + "/model"
        cmd = mydir + "/entitymentionclassify.sh -d -d -e " + goldstr +  " -b " + svmlightdir + "/ -t " + tedir + "/ " + model + out + " " + testf
        print(cmd)
        output = subprocess.check_output(shlex.split(cmd))
        print("finished")

        lines = output.split(b"\n")
        for i in range(1,len(lines)):
            if lines[i].find(b"Entity Reference Detection Evaluation") >= 0 and i + 11  < len(lines):
                r = parse_entities(lines[i:i+11],str(testf),"Testing")
                print_record(r)
                records.append(r)
    print_record(total_record(records))

def parse_entities(lines, name, tp):
    if len(lines) != 11:
        print("Error")
        return dict()
    record = dict()
    record['name'] = name
    record['type'] = "Entity Mention Detection - " + tp
    record['tp'] = int(lines[2].split(b":")[1])
    record['tn'] = int(lines[3].split(b":")[1])
    record['fp'] = int(lines[4].split(b":")[1])
    record['fn'] = int(lines[5].split(b":")[1])
    record['accuracy'] = float(lines[7].split(b":")[1])
    record['recall'] = float(lines[8].split(b":")[1])
    record['precision'] = float(lines[9].split(b":")[1])
    record['fscore'] = float(lines[10].split(b":")[1])
    return record

def preprocess(files):
    #Create pre directory
    tmpdir = os.path.join(mydir, "tmp")
    predir = os.path.abspath(os.path.join(tmpdir, "pre"))
    if os.path.exists(predir):
        shutil.rmtree(predir)
    os.mkdir(predir)

    #Run PreProcess Command
    filestr = ""
    for f in files:
        filestr += " %s " % f
    out = "-o %s/ " % str(predir)
    cmd = mydir + "/preprocess.sh " + out + filestr
    output = subprocess.check_output(shlex.split(cmd))

def entities(files):
    #Create ent directory
    tmpdir = os.path.join(mydir, "tmp")
    predir = os.path.abspath(os.path.join(tmpdir, "ent"))
    if os.path.exists(predir):
        shutil.rmtree(predir)
    os.mkdir(predir)

    #Run Entity Extraction Command
    filestr = ""
    for f in files:
        filestr += " %s " % f
    out = "-o %s/ " % str(predir)
    cmd = mydir + "/entityextract.sh -e " + out + filestr
    output = subprocess.check_output(shlex.split(cmd))

    records = []
    lines = output.split(b"\n")
    for i in range(1,len(lines)):
        if lines[i].find(b"Entity Extraction Evaluation") >= 0 and i + 9  < len(lines):
            records.append(parse_preprocess(lines[i-1:i+9]))

    for r in records:
        print_record(r)
    print_record(total_record(records))

def parse_preprocess(lines):
    if len(lines) != 10:
        print("Error")
        return dict()
    record = dict()
    record['name'] = lines[0]
    record['type'] = "Entity Extraction"
    record['tp'] = int(lines[3].split(b":")[1])
    record['fp'] = int(lines[4].split(b":")[1])
    record['fn'] = int(lines[5].split(b":")[1])
    record['tn'] = 0
    record['accuracy'] = 0.0
    record['recall'] = float(lines[7].split(b":")[1])
    record['precision'] = float(lines[8].split(b":")[1])
    record['fscore'] = float(lines[9].split(b":")[1])
    return record

def total_record(records):
    tp = 0
    fp = 0
    fn = 0
    tn = 0
    for r in records:
        tp += r['tp']
        fp += r['fp']
        fn += r['fn']
        tn += r['tn']
    record = dict()
    record['name'] = 'Total':
    record['type'] = ""
    record['tp'] = tp
    record['fp'] = fp
    record['fn'] = fn
    record['tn'] = tn
    record['accuracy'] = accuracy(tp,tn,tp+tn+fp+fn)
    record['recall'] = recall(tp,fn)
    record['precision'] = precision(tp,fp)
    record['fscore'] = fscore(tp,fp,fn)
    return record

def print_record(record):
    print("##############")
    print(str(record['name']))
    print(str(record['type']))
    print("------------")
    print("True Positives: " + str(record['tp']))
    print("False Positives: " + str(record['fp']))
    print("True Negatives: " + str(record['tn']))
    print("False Negatives: " + str(record['fn']))
    print("------------")
    print("Accuracy: " + str(record['accuracy']))
    print("Recall: " + str(record['recall']))
    print("Precision: " + str(record['precision']))
    print("F-score: " + str(record['fscore']))
    print("##############")


def formats(files):
    #Create formats directory
    tmpdir = os.path.join(mydir, "tmp")
    fdir = os.path.abspath(os.path.join(tmpdir, "formats"))
    if os.path.exists(fdir):
        shutil.rmtree(fdir)
    os.mkdir(fdir)

    #Iterate over files
    for f in files:
        #Build paths
        rel_dir = ""
        ent_dir = ""
        f_dir = ""
        base = os.path.basename(f)
        if base.find("csv") >= 0:
            rel_dir = os.path.abspath(os.path.normpath(os.path.dirname(f)))
            ent_dir = os.path.abspath(os.path.normpath(rel_dir + "/../ent/"))
            f_dir = os.path.abspath(os.path.normpath(rel_dir + "/../formats/"))
        elif base.find("ent") >= 0:
            ent_dir = os.path.abspath(os.path.normpath(os.path.dirname(f)))
            rel_dir = os.path.abspath(os.path.normpath(ent_dir + "/../rels/"))
            f_dir = os.path.abspath(os.path.normpath(ent_dir + "/../formats/"))
        else:
            print("Error")
            return

        base = base.split(".")[0]
        ent_file = ent_dir + "/" + base + ".annote.txt.short.ent"
        rel_file = rel_dir + "/" + base + ".annote_results_pred.csv"
        format_file = f_dir + "/" + base + ".format"
        
        print("")
        print(base)
        
        cmd = 'python ' + snakeconvertdir + "/2snake.py --ipnum -1 " + ent_file + " " + rel_file + " " + format_file
        output = subprocess.check_output(shlex.split(cmd))
        print(output)


def main(args):
        argp = argparse.ArgumentParser(description='Test Executor')
        argp.add_argument('--preprocess', '-p',action='store_true')
        argp.add_argument('--entities', '-e',action='store_true')
        argp.add_argument('--mentions', '-m',action='store_true')
        argp.add_argument('--formats', '-f',action='store_true')
        argp.add_argument('--gold', '-g', action='store_true')
        argp.add_argument('files',metavar="file",type=str, nargs='*')
        args = vars(argp.parse_args(args[1:]))

        #Check that files exist
        start = time.time()
        files = args['files']
        for f in files:
                if not os.path.exists(f) or not os.path.isfile(f) or not os.access(f,os.R_OK):
                        print("Error: Cannot read file (%s)" % f)
                        sys.exit(1)

        if len(files) == 0:
            print("Error: No files!")
            sys.exit(1)

        #Check that tmp directory exists
        tmpdir = os.path.abspath(os.path.join(mydir, 'tmp'))
        if not os.path.exists(tmpdir):
            os.mkdir(tmpdir)

        #PreProcess
        if args['preprocess']:
            preprocess(files)


        #Entity Extraction
        if args['entities']:
            entities(files)

        #Entity Mention Identification
        if args['mentions']:
            mentions(files, args['gold'])

        #Format Creation
        if args['formats']:
            formats(files)

        print("---------------------")
        print("Runtime: " + str(time.time() - start))
        print("---------------------")

if __name__ == "__main__":
        main(sys.argv)
