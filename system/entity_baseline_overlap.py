import json
import os
from pprint import pprint

PATH = 'tmp/ent/'
FILE_TEMPLATE = '{0}.annote.txt.short.json'
FILES = ['TCP', 'DCCP', 'IP', 'SCTP', 'IPv6', 'GRE']
OVERLAP_DEGREE = 0.5


def get_chunks_in_section(section, index, accum_chunks, accum_fieldrefs):
    #print "index", index[0]
    curr_chunks_title = [chunk for chunk in section['titleannotations'] if chunk['type'] == 'int:chunk']
    curr_chunks_body = [chunk for chunk in section['bodyannotations'] if chunk['type'] == 'int:chunk']
    curr_fieldrefs_title = [ann for ann in section['titleannotations'] if ann['type'] == 'fieldref' or ann['type'] == 'typeref' ]
    curr_fieldrefs_body = [ann for ann in section['bodyannotations'] if ann['type'] == 'fieldref' or ann['type'] == 'typeref' ]

    for chunk in curr_chunks_title:
        chunk['section_id'] = index[0]
        chunk['part'] = 'title'
    for chunk in curr_chunks_body:
        chunk['section_id'] = index[0]
        chunk['part'] = 'body'

    for fieldref in curr_fieldrefs_title:
        fieldref['section_id'] = index[0]
        fieldref['part'] = 'title'
    for fieldref in curr_fieldrefs_body:
        fieldref['section_id'] = index[0]
        fieldref['part'] = 'body'

    #accum_chunks += curr_chunks_title
    accum_chunks += curr_chunks_body
    #accum_fieldrefs += curr_fieldrefs_title
    accum_fieldrefs += curr_fieldrefs_body

    index[0] += 1

    if len(section['subsections']) > 0:
        for subsection in section['subsections']:
            get_chunks_in_section(subsection, index, accum_chunks, accum_fieldrefs)
            index[0] += 1
    else:
        #print len(accum_chunks)
        return

def contains(chunk, ann):
    if (chunk['section_id'] == ann['section_id'] and chunk['part'] == ann['part'] and
        chunk['bg-s'] >= 0 and chunk['ed-s'] >= 0 and chunk['bg-w'] >= 0 and chunk['ed-w'] >= 0 and
        ann['bg-s'] >= 0   and ann['ed-s'] >= 0   and ann['bg-w'] >= 0  and ann['ed-w'] >= 0):

           if (chunk['bg-s'] <= ann['bg-s'] and chunk['bg-w'] <= ann['bg-w'] and
               chunk['ed-s'] >= ann['ed-s'] and chunk['ed-w'] >= ann['ed-w']):

                return True

    return False

def longest_common_substring(s1, s2, realid):
   #print s1, "|", realid, s2
   m = [[0] * (1 + len(s2)) for i in xrange(1 + len(s1))]
   longest, x_longest = 0, 0
   for x in xrange(1, 1 + len(s1)):
       for y in xrange(1, 1 + len(s2)):
           if s1[x - 1] == s2[y - 1]:
               m[x][y] = m[x - 1][y - 1] + 1
               if m[x][y] > longest:
                   longest = m[x][y]
                   x_longest = x
           else:
               m[x][y] = 0
   return s1[x_longest - longest: x_longest]

def get_overlap_perc(chunk, key_phrase, realid):
    lcs = longest_common_substring(key_phrase, chunk, realid)
    lcs_size = len(lcs)

    #if lcs_size > 0:
    #    print lcs

    if len(key_phrase) == 0:
        return 0

    '''
    if (1.0 * lcs_size) / len(key_phrase) >= 1.0:
        print chunk, "|", key_phrase,(1.0 * lcs_size) / len(key_phrase)
    '''

    return (1.0 * lcs_size) / len(key_phrase)

def overlap_index(words1, words2):
    words1 = words1.lower().split()
    words2 = words2.lower().split()
    num = len(set(words1) & set(words2))
    denom = min(len(set(words1)), len(set(words2)))
    return (1.0 * num) / denom


chunks = {}; fieldrefs = {}; true_chunks = {}; metrics = {}
tp = 0.0; fp = 0.0; tn = 0.0; fn = 0.0

count = 0
for f in FILES:
    chunks[f] = []
    fieldrefs[f] = []
    true_chunks[f] = []
    metrics[f] = {'tp': 0.0, 'fp': 0.0, 'tn': 0.0, 'fn': 0.0}

    filename = os.path.join(PATH, FILE_TEMPLATE.format(f))
    #print filename
    with open(filename) as data_file:
        data = json.load(data_file)

        '''
        for entity in data['entities']:
            print entity['name']
        exit()
        '''

        i = [0]
        for section in data['sections']:
            curr_chunks = []; curr_fieldrefs = []
            get_chunks_in_section(section, i, curr_chunks, curr_fieldrefs)

            chunks[f] += curr_chunks
            fieldrefs[f] += curr_fieldrefs

        '''
        for fieldref in fieldrefs[f]:
            print fieldref
        '''


        entity_ids = set([ent['realid'] for ent in data['goldentities'] if ent['name'] != ''])

        for chunk in chunks[f]:

            current_preds = set([])
            for entity in data['goldentities']:
                curr_overlap = get_overlap_perc(chunk['expression'], entity['name'], entity['realid'])
                if curr_overlap >= OVERLAP_DEGREE:
                    current_pred = entity['realid']
                    current_preds.add(current_pred)

            all_refs = set([])
            for fieldref in fieldrefs[f]:
                if contains(chunk, fieldref):
                    for ref in fieldref['refs']:
                        all_refs.add(ref)

            metrics[f]['tp'] += len(all_refs & current_preds)
            metrics[f]['fp'] += len(current_preds - all_refs)
            metrics[f]['fn'] += len(all_refs - current_preds)
            metrics[f]['tn'] += len(entity_ids - current_preds)

        if (metrics[f]['tp'] + metrics[f]['fp']) == 0:
            prec = 0.0
        else:
            prec =  metrics[f]['tp'] / (metrics[f]['tp'] + metrics[f]['fp'])
        if (metrics[f]['tp'] + metrics[f]['fn']) == 0:
            recall = 0.0
        else:
            recall = metrics[f]['tp'] / (metrics[f]['tp'] + metrics[f]['fn'])
        print "precision", prec
        print "recall", recall

        if prec + recall == 0:
            print "F1", 0.0
        else:
            print "F1", 2 * ((prec * recall) / (prec + recall))
        print "-----"

        tp +=  metrics[f]['tp']
        fp +=  metrics[f]['fp']
        fn +=  metrics[f]['fn']
        tn +=  metrics[f]['tn']


print
print "Total"

if tp + fp == 0:
    prec = 0.0
else:
    prec =  tp / (tp + fp)

if tp + fn == 0:
    recall = 0.0
else:
    recall = tp / (tp + fn)

print "precision", prec
print "recall", recall

if prec + recall == 0:
    print "F1", 0.0
else:
    print "F1", 2 * ((prec * recall) / (prec + recall))

print "Correct Ref", tp
print "Incorrect Ref", fp
