def get_gold_tuples(sect_relns, include_binary=False, include_unary=False):
    tuples_single = set([]); tuples_all = set([])
    results = []
    for sect_id in sect_relns:
        for reln in sect_relns[sect_id]:

            for ann_rel in reln['ann_relations']:

                if ann_rel['ann_rel'] in UNARY_RLNS and include_unary:
                    for j, ent in enumerate(ann_rel['ann_to_ent'].split(',')):
                        if ent != '':
                            tuples_single.add((ann_rel['ann_rel'], int(ent)))
                            tuples_all.add((ann_rel['ann_rel_id'], ann_rel['ann_rel'], int(ent)))

                elif ann_rel['ann_rel'] in BINARY_RLNS and include_binary:
                    if 'ann_to_ent_A' in ann_rel and 'ann_to_ent_B' in ann_rel:
                        for ent_A in ann_rel['ann_to_ent_A'].split(','):
                            for ent_B in ann_rel['ann_to_ent_B'].split(','):
                                if ent_A != '' and ent_B != '':
                                    tuples_single.add((ann_rel['ann_rel'], int(ent_A), int(ent_B)))
                                    tuples_all.add((ann_rel['ann_rel_id'], ann_rel['ann_rel'], int(ent_A), int(ent_B)))
                    else:
                        tuples_single.add((ann_rel['ann_rel'], None, None))
                        tuples_all.add((ann_rel['ann_rel_id'], ann_rel['ann_rel'], None, None))

    return tuples_single, tuples_all
