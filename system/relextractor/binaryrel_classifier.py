import numpy as np
from sklearn import svm, metrics

from relextractor.ontology import Ontology
from relextractor import features, utils

class BinaryRelClassifier(object):

    def __init__(self):
        self.ontology = Ontology()

    def rels_and_types(self, dataset, filename):
        y = []; y_types = []
        for section in dataset[filename]['sections']:
            for i in range(len(section['chunks'])):
                chunk = section['chunks'][i]

                # indicates whether there is a relation in this chunk
                relations_found = [ann['ann_rel'] for ann in chunk['ann_relations']]
                relations_found_set = set(relations_found)

                y.append(int(len(relations_found_set & self.ontology.BINARY_RLNS) > 0))
                y_types.append(relations_found_set & self.ontology.BINARY_RLNS)

        return y, y_types

    def recurse_subsections(self, hierarchy, sect_id, relevant=[]):
        if len(hierarchy[sect_id]) > 0:
            relevant += hierarchy[sect_id]
            for subsect_id in hierarchy[sect_id]:
                self.recurse_subsections(hierarchy, str(subsect_id), relevant)

    def reverse_hierarchy(self, hierarchy):
        rev = {}
        for parent, children in hierarchy.items():
            for child in children:
                if child not in rev:
                    rev[child] = []
                rev[child].append(int(parent))
        return rev

    def get_binary_candidates(self, sect_predicted_relns, sect_entities, hierarchy):
        reverse_hierarchy = self.reverse_hierarchy(hierarchy)
        ret = []
        #print("All sections:", sect_predicted_relns.keys())
        for sect_id in range(len(sect_predicted_relns)):
            #print("SECTION", sect_id)
            for reln in sect_predicted_relns[sect_id]:
                '''
                for rel in reln['ann_relations']:
                    print(rel['ann_rel_str'])
                '''

                if len(set(reln['pred_types']) & self.ontology.BINARY_RLNS) > 0:
                    relevant = [sect_id]
                    self.recurse_subsections(hierarchy, str(sect_id), relevant)

                    #for now just consider direct parent
                    relevant += reverse_hierarchy[sect_id]

                    # TO-DO: consider a single entity approach
                    # Generate one example for each side A and B, include it as part of the input
                    for i in range(0, 2):
                        is_a = i; is_b = abs(is_a - 1)
                        for s1 in relevant:
                            #print("\tsection", s1)
                            for ent_A in sect_entities[s1]:
                                #print("\t\t", ent_A[0]['ann_entities'][ent_A[1]]['ann_ent_str'])
                                ret.append((reln, ent_A, is_a, is_b))
        #exit()
        return ret

    def split_tuples(self, gold_tuples_all):
        ret = []
        for (rel_id, rel_type, a_ent_id, b_ent_id) in gold_tuples_all:
            # (rel_id, rel, entity, is_A, is_B)
            ret.append((rel_id, rel_type, a_ent_id, 1, 0))
            ret.append((rel_id, rel_type, b_ent_id, 0, 1))
        return set(ret)

    # TO-DO: can't assume 'ann_relations' is annotated
    def featurize_binary_candidates(self, candidates, hierarchy, gold_tuples_all=None):
        #print(gold_tuples_all)
        if gold_tuples_all is not None:
            split_gold = self.split_tuples(gold_tuples_all)
            '''
            print("GOLD: --")
            for s in split_gold:
                print(s)
            print("--------")
            '''

        X = []; y = []
        for cand in candidates:
            (reln, (ent, pos), is_a, is_b) = cand

            if 'ann_relations' in reln:
                rel_types = [rel['ann_rel'] for rel in reln['ann_relations']]
                rel_ids = [rel['ann_rel_id'] for rel in reln['ann_relations']]

            for _type in reln['pred_types']:

                e = ent['ann_entities'][pos]

                same_subsection_e_rel = int(reln['section_id'] == ent['section_id'])
                reln_subsect_of_e = int(reln['section_id'] in hierarchy[str(ent['section_id'])])
                e_subsect_of_reln = int(ent['section_id'] in hierarchy[str(reln['section_id'])])
                e_subsect_distance = abs(int(reln['section_id']) - int(ent['section_id']))

                distance_e = 0; e_is_left = 0; e_is_right = 0; e_subsect_pos = 0

                if same_subsection_e_rel:
                    distance_e = abs(reln['id'] - ent['id'])
                    e_is_left = int(reln['id'] - ent['id'] > 0)
                    e_is_right = abs(e_is_left - 1)
                else:
                    e_subsect_pos = ent['id']

                '''
                X.append([same_subsection_e_rel, distance_e, reln_subsect_of_e, e_subsect_of_reln,
                          e_is_left, e_is_right, e_subsect_pos, e_subsect_distance, is_a, is_b])
                '''

                X.append([distance_e, e_is_left, e_is_right, is_a, is_b])

                if gold_tuples_all is not None and 'ann_relations' in reln:

                    index = rel_types.index(_type)

                    ent_id = e['ann_ent_id']
                    try:
                        ent_id = int(ent_id)
                    except:
                        pass

                    y.append(int((rel_ids[index], rel_types[index], ent_id, is_a, is_b) in split_gold))
                    #print((rel_ids[index], rel_types[index], ent_id, is_a, is_b), y[-1])
        #exit()
        return X, y

    def extract_file(self, dataset, filename, y, y_types, gold_tuples=False):
        relns, entities = \
                    utils.get_relations_and_entities(dataset, filename, y, y_types)
        candidates = self.get_binary_candidates(relns, entities,
                                                dataset[filename]['hierarchy'])

        if gold_tuples:
            _, gold_tuples_all = utils.get_gold_tuples(relns,
                                                       self.ontology.UNARY_RLNS,
                                                       self.ontology.BINARY_RLNS,
                                                       include_binary=True,
                                                       include_unary=False)
            X_binary, y_binary = self.featurize_binary_candidates(
                        candidates, dataset[filename]['hierarchy'],
                        gold_tuples_all)
        else:
            X_binary, y_binary = self.featurize_binary_candidates(
                        candidates, dataset[filename]['hierarchy'])
            gold_tuples_all = None


        return X_binary, y_binary, candidates, gold_tuples_all

    def recover_predictions(self, candidates, y_binary_pred):
        # first aggregate results by rel_id
        agg_all = {}; agg_simple = {}
        #print("CANDIDATES TO RESULTS ---")

        y_pred_index = 0
        for cand in candidates:
            (reln, (ent, ent_pos), is_a, is_b) = cand

            #print(len(reln['pred_types']), len(reln['ann_relations']))

            for tid, _type in enumerate(reln['pred_types']):

                if y_binary_pred[y_pred_index] == 0:
                    y_pred_index += 1
                    continue

                # unique id for chunk in section
                id_ = "s{0}_c{1}".format(reln['section_id'], reln['id'])

                if 'ann_relations' in reln:

                    rel_types = [rel['ann_rel'] for rel in reln['ann_relations']]
                    rel_ids = [rel['ann_rel_id'] for rel in reln['ann_relations']]
                    index = rel_types.index(_type)

                    ann = reln['ann_relations'][index]

                    # this was here to debug specific relations
                    #if ann['ann_rel_id'] == 36:
                    #    print((ann['ann_rel_id'], ann['ann_rel'], ent['ann_entities'][ent_pos]['ann_ent_id'], is_a, is_b), y_binary_pred[y_pred_index])

                    if id_ not in agg_all:
                        agg_all[id_] = {'A': [], 'B': [], 'type': _type, 'rel_id': ann['ann_rel_id']}

                    # A case
                    if is_a:
                        agg_all[id_]['A'].append(ent['ann_entities'][ent_pos])

                    # B case
                    if is_b:
                        agg_all[id_]['B'].append(ent['ann_entities'][ent_pos])


                if id_ not in agg_simple:
                    agg_simple[id_] = {'A': [], 'B': [], 'type': _type, 'rel': reln}

                if is_a:
                    agg_simple[id_]['A'].append(ent['ann_entities'][ent_pos])

                if is_b:
                    agg_simple[id_]['B'].append(ent['ann_entities'][ent_pos])


                y_pred_index += 1

        #print(y_pred_index)
        #print(len(candidates))
        #print(len(y_binary_pred))
        return agg_all, agg_simple


    def candidates_to_results(self, candidates, y_binary_pred):
        # TO-DO: generate the simple results (no rel id)
        predictions_all = set([]); predictions_single = set([])
        predictions_full = []

        agg_all, agg_simple = self.recover_predictions(candidates, y_binary_pred)

        # now generate the 4-tuples
        for unique_id in agg_all:
            #print(relid)
            _type = agg_all[unique_id]['type']
            relid = agg_all[unique_id]['rel_id']

            a_ids = []; b_ids = []
            for a in agg_all[unique_id]['A']:
                a_ids += a['ann_ent_id'].split(',')
            for b in agg_all[unique_id]['B']:
                b_ids += b['ann_ent_id'].split(',')

            for a_id in a_ids:
                for b_id in b_ids:
                    try:
                        a_id = int(a_id)
                    except:
                        pass
                    try:
                        b_id = int(b_id)
                    except:
                        pass
                    #print('\t', (relid, _type, a_id, b_id))
                    predictions_all.add((relid, _type, a_id, b_id))


        # generate the 3-tuples
        predictions_full = []
        for unique_id in agg_simple:
            _type = agg_simple[unique_id]['type']
            ann = agg_simple[unique_id]['rel']

            a_ids = []; b_ids = []
            for a in agg_simple[unique_id]['A']:
                a_ids += a['ann_ent_id'].split(',')
            for b in agg_simple[unique_id]['B']:
                b_ids += b['ann_ent_id'].split(',')

            for a_id in a_ids:
                for b_id in b_ids:
                    try:
                        a_id = int(a_id)
                    except:
                        pass
                    try:
                        b_id = int(b_id)
                    except:
                        pass
                    predictions_single.add((_type, a_id, b_id))
                    predictions_full.append((ann, _type, a_id, b_id))


        #print(predictions_all)
        #print(predictions_single)
        #exit()
        return predictions_single, predictions_all, predictions_full

    def extract_train(self, dataset, train_files):
        train_X_binary = []; train_y_binary = []
        #for n_f, filename in enumerate(train_files + ['DCCP.annote.txt.short']):
        for n_f, filename in enumerate(train_files):
            #filename = "SCTP.annote.txt.short"
            #print('\t', filename, '...')
            y, y_types = self.rels_and_types(dataset, filename)

            print(len(y), len(y_types))

            X_binary, y_binary, candidates, gold_tuples_all = \
                    self.extract_file(dataset, filename, y, y_types, gold_tuples=True)
            train_X_binary += X_binary
            train_y_binary += y_binary

            #This is here to debug
            #print("\t", y_binary.count(1), "/", len(y_binary))

            generated_single, generated_all, _ = self.candidates_to_results(candidates, y_binary)
            #print(len(generated_all & gold_tuples_all), "/", len(gold_tuples_all))

            #print("FOUND", generated_all & gold_tuples_all)
            #print("MISSED", gold_tuples_all - generated_all)
            #print("------")

        #exit()
        return train_X_binary, train_y_binary

    def train(self, dataset, train_files):
        train_X, train_y =\
                self.extract_train(dataset, train_files)
        self.clf = svm.SVC(class_weight='balanced', kernel='linear')
        print(len(train_X), len(train_y))

        train_X = np.asarray(train_X); train_y = np.asarray(train_y)
        print("Training...")
        self.clf.fit(train_X, train_y)
        y_pred = self.clf.predict(train_X)
        print("Performance on training data:")
        print(metrics.classification_report(train_y, y_pred))
        print("Done.")

    def predict(self, dataset, test_file, pred_y, pred_y_types):
        # this part is only for self evaluation of classifier
        # checking against gold data for test
        y, y_types = self.rels_and_types(dataset, test_file)
        X_binary_g, y_binary_g, _, _ = \
                    self.extract_file(dataset, test_file, y, y_types,
                                      gold_tuples=True)
        y_binary_pred_g = self.clf.predict(X_binary_g)
        print("Performance on test data (assuming gold trigger):")
        print(metrics.classification_report(y_binary_g, y_binary_pred_g))
        #exit()

        # this part is for actual results, includes noise from trigger prediction
        X_binary, _, candidates, _ = self.extract_file(dataset, test_file, pred_y, pred_y_types)
        X_binary = np.asarray(X_binary)
        y_binary_pred = self.clf.predict(X_binary)
        prediction_single, prediction_all, pred_full = self.candidates_to_results(candidates, y_binary_pred)

        return prediction_single, prediction_all, pred_full
