def overlap_index(words1, words2):
    num = len(set(words1) & set(words2))
    denom = min(len(set(words1)), len(set(words2)))
    return (1.0 * num) / denom

def longest_common_substring(s1, s2):
   m = [[0] * (1 + len(s2)) for i in range(1 + len(s1))]
   longest, x_longest = 0, 0
   for x in range(1, 1 + len(s1)):
       for y in range(1, 1 + len(s2)):
           if s1[x - 1] == s2[y - 1]:
               m[x][y] = m[x - 1][y - 1] + 1
               if m[x][y] > longest:
                   longest = m[x][y]
                   x_longest = x
           else:
               m[x][y] = 0
   return s1[x_longest - longest: x_longest]

def get_overlap_perc(chunk, key_phrase):
    lcs = longest_common_substring(chunk, key_phrase)
    lcs_size = len(lcs)

    #if lcs_size > 0:
    #    print lcs

    return (1.0 * lcs_size) / len(key_phrase)

def get_greatest_overlap_perc(chunk, key_phrases):
    maximum = 0.0
    for kp in key_phrases:
        overlap = get_overlap_perc(chunk, kp.split())
        if overlap > maximum:
            maximum = overlap
    return maximum

def has_degree_overlap(chunk, key_phrases, degree):
    greatest_overlap = get_greatest_overlap_perc(chunk, key_phrases)
    if greatest_overlap >= degree:
        return 1
    else:
        return 0

def has_overlap(chunk, key_phrases):
    for kp in key_phrases:
        overlap = get_overlap_perc(chunk, kp.split())
        if overlap > 1:
            return 1
    return 0

def is_subsequence(string1, string2, m, n):
    # Base Cases
    if m == 0:    return True
    if n == 0:    return False

    # If last characters of two strings are matching
    if string1.lower()[m-1] == string2.lower()[n-1]:
        return is_subsequence(string1.lower(), string2.lower(), m-1, n-1)

    # If last characters are not matching
    return is_subsequence(string1.lower(), string2.lower(), m, n-1)

def is_keyword_subsequence(chunk, key_phrases):
    for kp in key_phrases:
        if (kp in chunk.lower()):
            return 1
    return 0

def is_exact_match(chunk, key_phrases):
    for kp in key_phrases:
        if (kp == chunk.lower()):
            return 1
    return 0

def and_vect_scalar(v1, s):
    ret = []
    for a in v1:
        ret.append(a & s)
    return ret

