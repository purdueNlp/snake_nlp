'''
    Methods that deal with processing the annotated dataset
'''

def get_relations_and_entities(dataset, test_file, pred_y, pred_y_types):
    sect_predicted_relns = {}; sect_entities = {}
    pred_index = 0

    for sect_id, section in enumerate(dataset[test_file]['sections']):

        #print test_file, sect_id
        sect_predicted_relns_curr = []
        sect_entities_curr = []

        sentences = {}
        sent_idx = 0
        sent = ""
        for chunk in section['chunks']:
            if sent_idx == chunk['sent']:
                sent += " " + " ".join(chunk['words'])
            else:
                sentences[sent_idx] = sent
                sent_idx = chunk['sent']
                sent = " ".join(chunk['words'])
        sentences[sent_idx] = sent

        for chunk in section['chunks']:
            predicted_relns = []; entities = []

            if pred_y[pred_index] == 1:
                chunk['pred_types'] = pred_y_types[pred_index]
                chunk['fsent'] = sentences[chunk['sent']]
                predicted_relns.append(chunk)

            for ent_pos, entity in enumerate(chunk['ann_entities']):
                #print '\t\t', entity['ann_ent_str']
                entities.append((chunk, ent_pos))

            pred_index += 1

            sect_entities_curr += entities
            sect_predicted_relns_curr += predicted_relns
        sect_predicted_relns[sect_id] = sect_predicted_relns_curr
        sect_entities[sect_id] = sect_entities_curr

    return sect_predicted_relns, sect_entities


def get_gold_tuples(sect_relns, unary_relns, binary_relns,
                    include_binary=False, include_unary=False):
    tuples_single = set([]); tuples_all = set([])
    results = []
    for sect_id in sect_relns:
        for reln in sect_relns[sect_id]:

            for ann_rel in reln['ann_relations']:

                if ann_rel['ann_rel'] in unary_relns and include_unary:
                    for j, ent in enumerate(ann_rel['ann_to_ent'].split(',')):
                        if ent != '':
                            tuples_single.add((ann_rel['ann_rel'], int(ent)))
                            tuples_all.add((ann_rel['ann_rel_id'],
                                            ann_rel['ann_rel'], int(ent)))

                elif ann_rel['ann_rel'] in binary_relns and include_binary:
                    if 'ann_to_ent_A' in ann_rel and 'ann_to_ent_B' in ann_rel:
                        for ent_A in ann_rel['ann_to_ent_A'].split(','):
                            for ent_B in ann_rel['ann_to_ent_B'].split(','):
                                if ent_A != '' and ent_B != '':
                                    tuples_single.add((ann_rel['ann_rel'],
                                                       int(ent_A), int(ent_B)))
                                    tuples_all.add((ann_rel['ann_rel_id'], 
                                                    ann_rel['ann_rel'],
                                                    int(ent_A), int(ent_B)))
                    else:
                        tuples_single.add((ann_rel['ann_rel'], None, None))
                        tuples_all.add((ann_rel['ann_rel_id'], ann_rel['ann_rel'],
                                        None, None))

    return tuples_single, tuples_all


def get_POS_resources(dataset, train_files):
    ## record features
    pos_tags = set([])
    for filename in train_files:
        for section in dataset[filename]['sections']:
            for chunk in section['chunks']:
                for tag in chunk['pos']:
                    pos_tags.add(tag)

    pos_tags_onehot = {}
    for i, pos in enumerate(pos_tags):
        x = [0] * len(pos_tags)
        x[i] = 1
        pos_tags_onehot[pos] = x

    return pos_tags, pos_tags_onehot

def mock_results(sect_relns):
    results = []
    for sect_id in sect_relns:
        for reln in sect_relns[sect_id]:
            for ann in reln['ann_relations']:

                if 'ann_to_ent_A' in ann and 'ann_to_ent_B' in ann:
                    for a in ann['ann_to_ent_A'].split(','):
                        for b in ann['ann_to_ent_B'].split(','):
                            if a != '' and b != '':
                                results.append((reln, ann['ann_rel'], int(a.strip()), int(b.strip())))
                if 'ann_to_ent' in ann:
                    for ent in ann['ann_to_ent'].split(','):
                        if ent != '':
                            results.append((reln, ann['ann_rel'], int(ent.strip())))
    return results

def compress_results(results):
    compressed_single = set([]); compressed_all = set([])
    for r in results:
        if len(r) == 3:
            compressed_single.add((r[1], r[2]))
            if 'ann_relations' in r[0]:
                for ann in r[0]['ann_relations']:
                    compressed_all.add((ann['ann_rel_id'], r[1], r[2]))
        if len(r) == 4:
            compressed_single.add((r[1], r[2], r[3]))
            if 'ann_relations' in r[0]:
                for ann in r[0]['ann_relations']:
                    compressed_all.add((ann['ann_rel_id'], r[1], r[2], r[3]))
    return compressed_single, compressed_all

