from nltk.corpus import wordnet as wn

class Ontology(object):

    def __init__(self):

        self.UNARY_RLNS = set(['r_sequence_number', 'r_checksum', 'r_port', 'r_packet_type',
                  'r_header_length', 'r_multiple', 'r_mbz', 'r_monotonic_inc', 'r_range'])

        self.BINARY_RLNS = set(['r_contains', 'r_field_present', 'r_significant', 'r_offset',
                   'r_greater', 'r_less'])

        self.KEYWORDS = \
            {'r_sequence_number': ['data octect', 'sequence number', 'acknowledgement number'],
             'r_checksum': ['checksum'],
             'r_port': ['port'],
             'r_packet_type': ['type', 'packet type', 'packet', 'control'],
             'r_header_length': ['length', 'header', 'data offset', 'size'],
             'r_multiple': ['integral number', 'multiple'],
             'r_mbz': ['must be zero', 'reserved', 'zeros', 'zero', "zeroes", '0'],
             'r_monotonic_inc': ['should not be lessened'],
             'r_contains': ['from left to right', 'these parameters'],
             'r_field_present': ['contain', 'carry', 'present', 'following format', 'following parameters', 'packets'],
             'r_significant': ['signficant', 'only interpreted with', '=', 'valid'],
             'r_offset': ['offset', 'indicated in'],
             'r_greater': ['greater than'],
             'r_less': ['must not be greater', 'less'],
             'r_range': ['takes value of', 'range']}

        self.BOW = \
            {'r_sequence_number': ['data', 'octect', 'sequence', 'number', 'acknowledgement', 'number'],
             'r_checksum': ['checksum'],
             'r_port': ['port'],
             'r_packet_type': ['type', 'packet'],
             'r_header_length': ['length', 'header', 'data', 'offset'],
             'r_multiple': ['integral', 'number'],
             'r_mbz': ['zero', 'reserved', 'zeros', "zeroes", '0'],
             'r_monotonic_inc': ['lessened'],
             'r_contains': ['left', 'right', 'parameters'],
             'r_field_present': ['contain', 'carry', 'present'],
             'r_significant': ['signficant', 'interpreted'],
             'r_offset': ['offset', 'indicated'],
             'r_greater': ['greater'],
             'r_less': ['less'],
             'r_range': ['value', 'range']}


        self.KEYWORDS_POS = \
                {'r_sequence_number': ['NNS VBP', 'NN NN', 'NN NN'],
                 'r_checksum': ['NN'],
                 'r_port': ['NN'],
                 'r_packet_type': ['NN', 'NN NN'],
                 'r_header_length': ['NN', 'NN', 'NNS VBP'],
                 'r_multiple': ['JJ NN'],
                 'r_mbz': ['MD VB CD', 'VBN', 'CD', 'CD'],
                 'r_monotonic_inc': ['MD RB VB VBN'],
                 'r_contains': ['IN VBN TO NN', 'DT NNS'],
                 'r_field_present': ['VB', 'VB'],
                 'r_significant': ['NN', 'RB VBN IN', '='],
                 'r_offset': ['VB', 'VBN IN'],
                 'r_greater': ['JJR IN'],
                 'r_less': ['MD RB VB JJR'],
                 'r_range': ['VBZ NN IN']}

        self.add_synms()

    def add_synms(self):
        for reln in self.BOW:
            #print reln
            synonyms = set([])
            for word in self.BOW[reln]:
                for ss in wn.synsets(word):
                    synonyms |= set(ss.lemma_names())

            self.BOW[reln] = list(synonyms | set(self.BOW[reln]))
            self.BOW[reln] = [" ".join(word.split("_")) for word in self.BOW[reln]]
            #print BOW[reln]

    def get_all_keyphrases(self):
        key_phrases = []
        for key in self.KEYWORDS:
            for value in self.KEYWORDS[key]:
                key_phrases.append(value)
        return key_phrases

    def get_all_synwords(self):
        synwords = []
        for key in self.BOW:
            for value in self.BOW[key]:
                synwords.append(value)
        return synwords


    def get_all_posphrases(self):
        key_phrases = []
        for key in self.KEYWORDS_POS:
            for value in self.KEYWORDS_POS[key]:
                key_phrases.append(value)
        return key_phrases


