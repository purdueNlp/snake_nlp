import numpy as np
from sklearn import svm, metrics
import random

from relextractor.ontology import Ontology
from relextractor import features

class RelTriggerClassifier(object):

    def __init__(self):
        self.ontology = Ontology()
        self.key_phrases = self.ontology.get_all_keyphrases()
        self.pos_phrases = self.ontology.get_all_posphrases()
        #self.synwords = ontology.get_all_synwords()
        self.synwords = None


    def chunk_features(self, chunk):
        words = [w.lower() for w in chunk['words']]
        kp_overlap = features.get_greatest_overlap_perc(words, self.key_phrases)
        #key_phraseskp_overlap = 0
        hs_50_overlap = features.has_degree_overlap(words, self.key_phrases, 0.50)
        hs_70_overlap = features.has_degree_overlap(words, self.key_phrases, 0.70)
        hs_85_overlap = features.has_degree_overlap(words, self.key_phrases, 0.85)
        exact_match = features.has_degree_overlap(words, self.key_phrases, 1.0)
        #exact_match = 0
        #hs_50_overlap = 0
        hs_overlap = features.has_overlap(words, self.key_phrases)
        #a lhs_overlap = 0
        is_subseq = features.is_keyword_subsequence(" ".join(words), self.key_phrases)
        #is_subseq = 0
        length = len(words)
        #length = 0
        '''
        syn_overlap = get_greatest_overlap_perc(words, synwords)
        hs_syn_overlap = has_overlap(words, synwords)
        is_subseq_syn = is_keyword_subsequence(" ".join(words), synwords)
        exact_match_syn = is_exact_match(" ".join(words), synwords)

        '''
        return kp_overlap, hs_overlap, is_subseq,\
               exact_match, length, hs_50_overlap, hs_70_overlap, hs_85_overlap

    def featurize_file(self, dataset, filename, include_binary=False,
                       include_unary=False):
        #print "\tconstructing ", filename
        X = []; y = []; y_types = []
        for section in dataset[filename]['sections']:
            for i in range(len(section['chunks'])):

                # previous chunk
                if i - 1 > 0:
                    chunk = section['chunks'][i - 1]
                    kp_overlap_prev, hs_overlap_prev, is_subseq_prev,\
                    exact_match_prev, len_prev, hs_50_overlap_prev,\
                    hs_70_overlap_prev, hs_85_overlap_prev =\
                        self.chunk_features(chunk)
                else:
                    kp_overlap_prev = 0.0; hs_overlap_prev = 0; is_subseq_prev = 0
                    exact_match_prev = 0; len_prev = 0; hs_50_overlap_prev = 0
                    hs_70_overlap_prev = 0; hs_85_overlap_prev = 0
                    #syn_overlap_prev = 0.0; hs_syn_overlap_prev = 0
                    #is_subseq_syn_prev = 0; exact_match_syn_prev = 0

                # next chunk
                if i + 1 < len(section['chunks']) - 1:
                    chunk = section['chunks'][i + 1]
                    kp_overlap_next, hs_overlap_next, is_subseq_next,\
                    exact_match_next, len_next, hs_50_overlap_next,\
                    hs_70_overlap_next, hs_85_overlap_next =\
                        self.chunk_features(chunk)
                else:
                    kp_overlap_next = 0.0; hs_overlap_next = 0; is_subseq_next = 0
                    exact_match_next = 0; len_next = 0; hs_50_overlap_next = 0
                    hs_70_overlap_next = 0; hs_85_overlap_next = 0
                    #syn_overlap_next = 0.0; hs_syn_overlap_next = 0
                    #is_subseq_syn_next = 0; exact_match_syn_next = 0

                # current chunk
                chunk = section['chunks'][i]
                kp_overlap, hs_overlap, is_subseq, exact_match, len_curr, hs_50_overlap,\
                hs_70_overlap, hs_85_overlap =\
                    self.chunk_features(chunk)

                X.append([is_subseq, hs_overlap, exact_match, kp_overlap,
                          is_subseq_prev, hs_overlap_prev, exact_match_prev, kp_overlap_prev,
                          is_subseq_next, hs_overlap_next, exact_match_next, kp_overlap_next,
                          len_prev, len_curr, len_next,
                          hs_50_overlap_prev, hs_50_overlap, hs_50_overlap_next,
                          hs_70_overlap_prev, hs_70_overlap, hs_70_overlap_next,
                          hs_85_overlap_prev, hs_85_overlap, hs_85_overlap_next])
                          #is_subseq_syn, hs_syn_overlap, exact_match_syn, syn_overlap,
                          #is_subseq_syn_prev, hs_syn_overlap_prev, exact_match_syn_prev, syn_overlap_prev,
                          #is_subseq_syn_next, hs_syn_overlap_next, exact_match_syn_next, syn_overlap_next])

                # indicates whether there is a relation in this chunk
                relations_found = [ann['ann_rel'] for ann in chunk['ann_relations']]
                relations_found_set = set(relations_found)

                if include_binary and include_unary:
                    y.append(int(len(relations_found) > 0))
                elif include_unary and not include_binary:
                    y.append(int(len(relations_found_set & self.ontology.UNARY_RLNS) > 0))
                elif include_binary and not include_unary:
                    y.append(int(len(relations_found_set & self.ontology.BINARY_RLNS) > 0))
                else:
                    y.append(0)

                # gets the actual type of the rel, type is empty string if no reln in chunk
                if include_binary and include_unary:
                    y_types.append(relations_found_set)
                elif include_unary and not include_binary:
                    y_types.append(relations_found_set & self.ontology.UNARY_RLNS)
                elif include_binary and not include_unary:
                    y_types.append(relations_found_set & self.ontology.BINARY_RLNS)
                else:
                    y_types.append(set([]))

        return X, y, y_types

    def best_match_chunk(self, words, include_binary=False, include_unary=False):
        words = [w.lower() for w in words]
        maxim = 0
        max_relns = set([])

        scores = {}

        for reln in self.ontology.KEYWORDS:
            if (include_binary and reln in self.ontology.BINARY_RLNS) or\
               (include_unary and reln in self.ontology.UNARY_RLNS):

                scores[reln] = 0
                for keyw in self.ontology.KEYWORDS[reln]:
                    overlap = features.overlap_index(words, keyw.split())
                    #print reln, overlap

                    if overlap >= maxim:
                        scores[reln] = overlap
                        #print "HERE", reln
                        maxim = overlap
                        #print "MAX_RELN", max_reln

                if scores[reln] == 0:
                    for syns in self.ontology.BOW[reln]:
                        overlap = features.overlap_index(words, syns.split())
                        if overlap >= maxim:
                            scores[reln] = overlap
                            maxim = overlap

        for reln in self.ontology.KEYWORDS:
            if (include_binary and reln in self.ontology.BINARY_RLNS) or \
               (include_unary and reln in self.ontology.UNARY_RLNS):

                if scores[reln] == maxim and maxim != 0:
                    max_relns.add(reln)

        # if no relation type was found, choose a random one
        if len(max_relns) == 0:
            rand_rel = random.randint(0, len(self.ontology.KEYWORDS) - 1)
            keys = list(self.ontology.KEYWORDS.keys())
            max_relns.add(keys[rand_rel])
        return max_relns


    def best_match_types(self, dataset, test_file, pred_y, include_binary=False,
                         include_unary=False):
        pred_y_types = []; pred_index = 0
        for section in dataset[test_file]['sections']:
            for chunk in section['chunks']:

                if pred_y[pred_index] == 1:
                    reln_type = self.best_match_chunk(chunk['words'], include_binary,
                                                      include_unary)
                    pred_y_types.append(reln_type)
                else:
                    pred_y_types.append(set([]))
                pred_index += 1

        return pred_y_types

    def extract_train(self, dataset, train_files):
        train_X = []; train_y = []
        for n_f, filename in enumerate(train_files):
             X, y, y_types = self.featurize_file(dataset, filename, include_binary=True,
                                                 include_unary=True)
             train_X += X; train_y += y
        return train_X, train_y

    def train(self, dataset, train_files):
        train_X, train_y = self.extract_train(dataset, train_files)
        train_X = np.asarray(train_X)
        train_y = np.asarray(train_y)
        self.clf = svm.SVC(class_weight='balanced', kernel='linear')
        self.clf.fit(train_X, train_y)

    def predict(self, dataset, test_file, include_unary, include_binary):
        test_X, test_y, test_y_types = \
            self.featurize_file(dataset, test_file, include_binary=include_binary,
                                include_unary=include_unary)
        pred_y = self.clf.predict(test_X)
        pred_y_types = self.best_match_types(dataset, test_file, pred_y,
                                             include_binary=include_binary,
                                             include_unary=include_unary)
        return test_y, test_y_types, pred_y, pred_y_types

