import json
import os
from pprint import pprint
import optparse 
from collections import Counter
from sklearn import metrics
import random

UNARY_RLNS = set(['r_sequence_number', 'r_checksum', 'r_port', 'r_packet_type',
                  'r_header_length', 'r_multiple', 'r_mbz', 'r_monotonic_inc', 'r_range'])

BINARY_RLNS = set(['r_contains', 'r_field_present', 'r_significant', 'r_offset',
                   'r_greater', 'r_less'])

KEYWORDS = \
        {'r_sequence_number': ['data octect', 'sequence number', 'acknowledgement number'],
         'r_checksum': ['checksum'],
         'r_port': ['port'],
         'r_packet_type': ['type', 'packet type', 'packet', 'control'],
         'r_header_length': ['length', 'header', 'data offset', 'size'],
         'r_multiple': ['integral number', 'multiple'],
         'r_mbz': ['must be zero', 'reserved', 'zeros', 'zero', "zeroes", '0'],
         'r_monotonic_inc': ['should not be lessened'],
         'r_contains': ['from left to right', 'these parameters'],
         'r_field_present': ['contain', 'carry', 'present', 'following format', 'following parameters', 'packets'],
         'r_significant': ['signficant', 'only interpreted with', '=', 'valid'],
         'r_offset': ['offset', 'indicated in'],
         'r_greater': ['greater than'],
         'r_less': ['must not be greater', 'less'],
         'r_range': ['takes value of', 'range']}

BOW = \
        {'r_sequence_number': ['data', 'octect', 'sequence', 'number', 'acknowledgement', 'number'],
         'r_checksum': ['checksum'],
         'r_port': ['port'],
         'r_packet_type': ['type', 'packet'],
         'r_header_length': ['length', 'header', 'data', 'offset'],
         'r_multiple': ['integral', 'number'],
         'r_mbz': ['zero', 'reserved', 'zeros', "zeroes", '0'],
         'r_monotonic_inc': ['lessened'],
         'r_contains': ['left', 'right', 'parameters'],
         'r_field_present': ['contain', 'carry', 'present'],
         'r_significant': ['signficant', 'interpreted'],
         'r_offset': ['offset', 'indicated'],
         'r_greater': ['greater'],
         'r_less': ['less'],
         'r_range': ['value', 'range']}


def false_positives(test_y, pred_y):
    # calculate the metrics that we care about
    n_fake_chunks = len([1 for (y_p, y_g) in zip(pred_y, test_y) \
                         if y_p == 1 and y_g == 0])
    n_chunks = len(test_y)
    return n_fake_chunks, n_chunks

def relation_metrics(compressed_results, gold_tuples):
    relations_found = Counter([r[0] for r in compressed_results])
    relations_gold = Counter([r[0] for r in gold_tuples])

    relations_type_found = Counter([(r[0], r[1]) for r in compressed_results])
    relations_type_gold = Counter([(r[0], r[1]) for r in gold_tuples])

    '''
    not_found = 0
    for rel in relations_gold:
        not_found += max(0, relations_gold[rel] - relations_found[rel])
    n_reln_identif = len(gold_tuples) - not_found

    not_found = 0
    for rel in relations_type_gold:
        not_found += max(0, relations_type_gold[rel] - relations_type_found[rel])
    n_reln_identif_type = len(gold_tuples) - not_found
    '''
    not_found = 0; total = 0
    for rel in relations_gold:
        if rel not in relations_found:
            not_found += 1
        total += 1
    n_reln_identif = total - not_found

    not_found = 0; total = 0
    for rel in relations_type_gold:
        if rel not in relations_type_found:
            not_found += 1
        total += 1
    n_reln_identif_type = total - not_found

    return n_reln_identif, n_reln_identif_type, total


def get_all_keyphrases(include_binary, include_unary):
    key_phrases = []
    for key in KEYWORDS:
        if (include_binary and key in BINARY_RLNS) or\
            (include_unary and key in UNARY_RLNS):
            for value in KEYWORDS[key]:
                key_phrases.append(value)
    return key_phrases

def longest_common_substring(s1, s2, realid):
   #print s1, "|", realid, s2
   m = [[0] * (1 + len(s2)) for i in xrange(1 + len(s1))]
   longest, x_longest = 0, 0
   for x in xrange(1, 1 + len(s1)):
       for y in xrange(1, 1 + len(s2)):
           if s1[x - 1] == s2[y - 1]:
               m[x][y] = m[x - 1][y - 1] + 1
               if m[x][y] > longest:
                   longest = m[x][y]
                   x_longest = x
           else:
               m[x][y] = 0
   return s1[x_longest - longest: x_longest]

def get_overlap_perc(chunk, key_phrase, realid):
    lcs = longest_common_substring(chunk, key_phrase, realid)
    lcs_size = len(lcs)

    #if lcs_size > 0:
    #    print lcs

    return (1.0 * lcs_size) / len(key_phrase)

def overlap_index(words1, words2):
    num = len(set(words1) & set(words2))
    denom = min(len(set(words1)), len(set(words2)))
    return (1.0 * num) / denom

def best_match_types(dataset, test_file, pred_y, include_binary=False, include_unary=False):
    pred_y_types = []; pred_index = 0
    for section in dataset[test_file]['sections']:
        for chunk in section['chunks']:

            if pred_y[pred_index] == 1:
                reln_type = best_match(chunk['words'], include_binary, include_unary)
                pred_y_types.append(reln_type)
            else:
                pred_y_types.append(set([]))
            pred_index += 1

    return pred_y_types

def get_relations_and_entities(dataset, test_file, pred_y, pred_y_types, test_y):
    sect_predicted_relns = []; sect_entities = []
    pred_index = 0

    for section in dataset[test_file]['sections']:
        for chunk in section['chunks']:
            predicted_relns = []; entities = []

            if pred_y[pred_index] == 1 and test_y[pred_index] == 1:
                chunk['pred_type'] = pred_y_types[pred_index]
                predicted_relns.append(chunk)
            if 'ann_ent_id' in chunk:
                entities.append(chunk)
            pred_index += 1

            sect_entities.append(entities)
            sect_predicted_relns.append(predicted_relns)

    return sect_predicted_relns, sect_entities

def get_gold_tuples(sect_relns, include_binary=False, include_unary=False):
    tuples_single = set([]); tuples_all = set([])
    results = []
    for sect_id in range(len(sect_relns)):
        for reln in sect_relns[sect_id]:
            if reln['ann_rel'] in UNARY_RLNS and include_unary:
                for j, ent in enumerate(reln['ann_to_ent'].split(',')):
                    if ent != '':
                        tuples_single.add((reln['ann_rel'], int(ent)))
                        tuples_all.add((reln['ann_rel_id'], reln['ann_rel'], int(ent)))

            elif reln['ann_rel'] in BINARY_RLNS and include_binary:
                if 'ann_to_ent_A' in reln and 'ann_to_ent_B' in reln:
                    for ent_A in reln['ann_to_ent_A'].split(','):
                        for ent_B in reln['ann_to_ent_B'].split(','):
                            if ent_A != '' and ent_B != '':
                                tuples_single.add((reln['ann_rel'], int(ent_A), int(ent_B)))
                                tuples_all.add((reln['ann_rel_id'], reln['ann_rel'], int(ent_A), int(ent_B)))
                else:
                    tuples_single.add((reln['ann_rel'], None, None))
                    tuples_all.add((reln['ann_rel_id'], reln['ann_rel'], None, None))
    return tuples_single, tuples_all

def get_results(sect_predicted_relns, sect_entities):
    results = []
    for sect_id in range(len(sect_predicted_relns)):
        for reln in sect_predicted_relns[sect_id]:
            #print reln['pred_type']
            if len(reln['pred_type'] & UNARY_RLNS) > 0:
                results += get_entity_def(reln)
            elif len(reln['pred_type'] & BINARY_RLNS) > 0:
                results += get_closest_left_right(reln, sect_entities[sect_id])
    return results

def compress_results(results):
    compressed_single = set([]); compressed_all = set([])
    for r in results:
        if len(r) == 2:
            for _type in r[0]['pred_type']:
                compressed_single.add((_type, r[1]))
                if 'ann_rel_id' in r[0]:
                    compressed_all.add((r[0]['ann_rel_id'], _type, r[1]))
        if len(r) == 3:
            for _type in r[0]['pred_type']:
                compressed_single.add((_type, r[1], r[2]))
                if 'ann_rel_id' in r[0]:
                    compressed_all.add((r[0]['ann_rel_id'], _type, r[1], r[2]))
    return compressed_single, compressed_all

def get_entity_def(reln):
    results = []
    for entdef in reln['entity_defs']:
        matched_entity = entdef
        results.append((reln, matched_entity))
    if len(reln['entity_defs']) == 0:
        results.append((reln, None))
    return results

# Right now we only match one A and B for a binary rel
# How do we deal with the cases where there are multiple?
def get_closest_left_right(reln, sect_entities):
    results = []
    matched_left = None; matched_right = None
    closest_distance_left = float('inf')
    closest_distance_right = float('inf')

    for ent in sect_entities:
        distance = (reln['id'] - ent['id'])
        if distance > 0:
            #left
            if distance < closest_distance_left:
                closest_distance_left = distance
                matched_left = ent
        elif distance < 0:
            #right
            if abs(distance) < closest_distance_right:
                closest_distance_right = abs(distance)
                matched_right = ent
    results.append((reln, matched_left, matched_right))
    return results

def best_match(words, include_binary=False, include_unary=False):
    words = [w.lower() for w in words]
    maxim = 0
    max_relns = set([])

    scores = {}

    for reln in KEYWORDS:
        if (include_binary and reln in BINARY_RLNS) or (include_unary and reln in UNARY_RLNS):
            scores[reln] = 0
            for keyw in KEYWORDS[reln]:
                overlap = overlap_index(words, keyw.split())
                #print reln, overlap

                if overlap >= maxim:
                    scores[reln] = overlap
                    #print "HERE", reln
                    maxim = overlap
                    #print "MAX_RELN", max_reln

            if scores[reln] == 0:
                for syns in BOW[reln]:
                    overlap = overlap_index(words, syns.split())
                    if overlap >= maxim:
                        scores[reln] = overlap
                        maxim = overlap

    for reln in KEYWORDS:
        if (include_binary and reln in BINARY_RLNS) or (include_unary and reln in UNARY_RLNS):
            if scores[reln] == maxim and maxim != 0:
                max_relns.add(reln)

    # if no relation type was found, choose a random one
    if len(max_relns) == 0:
        rand_rel = random.randint(0, len(KEYWORDS) - 1)
        max_relns.add(KEYWORDS.keys()[rand_rel])
    return max_relns


def main():
    parser = optparse.OptionParser()
    parser.add_option('-r', help='relations file', dest='data', type='string')
    parser.add_option('-b', '--binary', help='extract binary relations', dest='include_binary',
                      default=False, action='store_true')
    parser.add_option('-u', '--unary', help='extract unary relations', dest='include_unary',
                      default=False, action='store_true')

    parser.add_option('-o', help='overlap degree', dest='overlap_degree', type='float')
    (opts, args) = parser.parse_args()

    # Making sure all mandatory options appeared
    mandatories = ['data', 'overlap_degree']
    for m in mandatories:
        if not opts.__dict__[m]:
            print "mandatory option is missing\n"
            parser.print_help()
            exit(-1)

    with open(opts.data, 'rb') as f:
        dataset = json.loads(f.read())

    key_phrases = get_all_keyphrases(include_binary=opts.include_binary,
                                     include_unary=opts.include_unary)

    n_reln_ALL = 0; n_reln_identif_ALL = 0;
    n_reln_identif_type_ALL = 0; n_fake_chunks_ALL = 0
    n_reln_mentions_ALL = 0; n_chunks_ALL = 0

    for i in range(len(dataset['files'])):
        print "fold", i, dataset['files'][i]
        test_file = dataset['files'][i]
        pred_y = []; test_y = []; test_y_types = []
        for section in dataset[test_file]['sections']:
            for i in range(len(section['chunks'])):
                chunk = section['chunks'][i]

                current_pred = 0; overlap = 0
                for keyphrase in key_phrases:
                    words = [w.lower() for w in chunk['words']]
                    expression = " ".join(words)
                    curr_overlap = get_overlap_perc(expression, keyphrase, None)
                    if curr_overlap > overlap and curr_overlap >= opts.overlap_degree:
                        current_pred = 1
                        overlap = curr_overlap

                pred_y.append(current_pred)

                # indicates whether there is a relation in this chunk
                if opts.include_binary and opts.include_unary:
                    test_y.append(int('ann_rel' in chunk))
                elif opts.include_unary and not opts.include_binary:
                    test_y.append(int('ann_rel' in chunk and chunk['ann_rel'] in UNARY_RLNS))
                elif opts.include_binary and not opts.include_unary:
                    test_y.append(int('ann_rel' in chunk and chunk['ann_rel'] in BINARY_RLNS))
                else:
                    test_y.append(0)

                # gets the actual type of the rel, type is empty string if no reln in chunk
                if 'ann_rel' in chunk and opts.include_binary and opts.include_unary:
                    test_y_types.append(set([chunk['ann_rel']]))
                elif 'ann_rel' in chunk and opts.include_unary and not opts.include_binary:
                    if chunk['ann_rel'] in UNARY_RLNS:
                        test_y_types.append(set([chunk['ann_rel']]))
                    else:
                        test_y_types.append(set([]))
                elif 'ann_rel' in chunk and opts.include_binary and not opts.include_unary:
                    if chunk['ann_rel'] in BINARY_RLNS:
                        test_y_types.append(set([chunk['ann_rel']]))
                    else:
                        test_y_types.append(set([]))
                else:
                    test_y_types.append(set([]))

        pred_y_types = best_match_types(dataset, test_file, pred_y,
                                include_binary=opts.include_binary,
                                include_unary=opts.include_unary)


        print metrics.classification_report(test_y, pred_y)

        n_fake_chunks, n_chunks = false_positives(test_y, pred_y)

        sect_predicted_relns, sect_entities = \
                get_relations_and_entities(dataset, test_file, pred_y, pred_y_types, test_y)
        sect_gold_relns, sect_entities = \
                get_relations_and_entities(dataset, test_file, test_y, test_y_types, test_y)
        gold_tuples_single, gold_tuples_all = get_gold_tuples(sect_gold_relns,
                                                              include_binary=opts.include_binary,
                                                              include_unary=opts.include_unary)

        results_pred = get_results(sect_predicted_relns, sect_entities)
        results_gold = get_results(sect_gold_relns, sect_entities)

        compressed_pred_single, compressed_pred_all = compress_results(results_pred)
        compressed_gold_single, compressed_gold_all = compress_results(results_gold)

        n_reln_identif, n_reln_identif_type, n_reln_mentions = relation_metrics(compressed_pred_all, gold_tuples_all)

        # aggregate results
        n_reln_identif_ALL += n_reln_identif
        n_reln_identif_type_ALL += n_reln_identif_type
        n_fake_chunks_ALL += n_fake_chunks
        n_chunks_ALL += n_chunks
        n_reln_mentions_ALL += n_reln_mentions

        print "RESULTS"
        if n_reln_mentions == 0:
            print "overlap_recall", "N/A"
            print "overlap_type_recall", "N/A"
        else:
            print "overlap_recall", n_reln_identif / (1.0 * n_reln_mentions)
            print "overlap_type_recall", n_reln_identif_type / (1.0 * n_reln_mentions)
        print "noise_ratio", n_fake_chunks / (1.0 * n_chunks)
        print "n_reln", n_reln_mentions, "n_reln_identif", n_reln_identif, "n_reln_identif_type", n_reln_identif_type


    print
    print "TOTAL RESULTS"
    print "overlap_recall", n_reln_identif_ALL / (1.0 * n_reln_mentions_ALL)
    print "overlap_type_recall", n_reln_identif_type_ALL / (1.0 * n_reln_mentions_ALL)
    print "noise_ratio", n_fake_chunks_ALL / (1.0 * n_chunks_ALL)
    print "n_reln", n_reln_mentions_ALL, "n_reln_identif", n_reln_identif_ALL, "n_reln_identif_type", n_reln_identif_type_ALL

if __name__ == "__main__":
    main()
