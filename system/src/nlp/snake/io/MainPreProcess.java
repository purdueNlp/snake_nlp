package nlp.snake.io;

import java.util.ArrayList;
import java.util.List;

import nlp.snake.config.Config;
import nlp.snake.docprocessors.AnnotationStatistics;
import nlp.snake.docprocessors.DocProcessor;
import nlp.snake.ent.Entity;
import nlp.snake.ent.InputProtocol;
import nlp.snake.entityextractors.EntitiesFromAnnotation;
import nlp.snake.entityextractors.EntitiesFromHeaders;
import nlp.snake.entityextractors.EntityExtractor;

public class MainPreProcess {
	private static final double VERSION = 1.0;
	private static final int COPYRIGHT_YEAR = 2018;

	public static void main(String[] args) {
		List<String> files = new ArrayList<>();
		String outputdir = "";
		boolean annotation_stats = false;

		/* Parse Arguments */
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-d")) {
				Config.debug++;
			} else if (args[i].equals("-v")) {
				version();
			} else if (args[i].equals("-h")) {
				usage();
			} else if (args[i].equals("-s")) {
				annotation_stats = true;
			} else if (args[i].equals("-o")) {
				if (i + 1 >= args.length) {
					System.err.println("-o option requires an argument");
					usage();
				}
				outputdir = args[i + 1];
				i++;
			} else if (args[i].startsWith("-")) {
				usage();
			} else {
				files.add(args[i]);
			}
		}
		if (files.size() == 0) {
			usage();
		}
		
		List<DocProcessor> procs = new ArrayList<>();
		if (annotation_stats) {
			DocProcessor p = new AnnotationStatistics();
			procs.add(p);
		}

		/* Read Inputfile */
		for (String f : files) {
			NewDataReader reader = new NewDataReader();
			InputProtocol p = reader.readProtocolFile(f);
			if (p == null) {
				System.err.println("Failed to Read: " + f);
				continue;
			}

			/* Debug Structure Output */
			if (Config.debug > 1) {
				p.print(System.out, true);
			}
			
			/* Do doc processors */
			for (DocProcessor proc : procs) {
				proc.processDocument(p);
				proc.finish();
			}

			/* Write JSON */
			DataWriter writer = new DataWriter(outputdir, p.getTitle());
			writer.writeJSON(outputdir +  p.getTitle() + ".json",p);
		}
	}

	private static void version() {
		System.err.println("NLP for RFCs version " + VERSION);
		System.err.println("Copyright (C) " + COPYRIGHT_YEAR + " Samuel Jero <sjero@sjero.net>");
		System.exit(0);
	}

	private static void usage() {
		System.err
				.println("Usage: [-v] [-h] [-d] [-s] [-e] [-o outputdir] input_files");
		System.err.println("       -v   Version Information");
		System.err.println("       -h   Help");
		System.err.println("       -d   Debug. Maybe repeated for additional verbosity");
		System.err.println("       -s   Output annotation statistics");
		System.err.println("       -o   Output directory");
		System.exit(0);
	}
}
