package nlp.snake.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import nlp.snake.config.Config;
import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;

public class DataWriter {
	protected static final int TRUTH_ID = 0;
	protected static final int REAL_ID = 1;
	protected static final int FIRST_ID = 2;
	protected static final int IS_MENTION_ID = 5;
	protected static final int FEATURES_START_AT = 10;
	protected static final int POSITIVE_EXAMPLE = 1;
	protected static final int NEGATIVE_EXAMPLE = -1;
	protected static final int UNKNOWN_EXAMPLE = 0;
	protected static final int UNKNOWN_VALUE = 0;

	private String dir;
	private String protoName;
	
	HashMap<String, Integer> relationIds = new HashMap<String, Integer>();
    HashMap<Integer, Object> sectionHierarchy = new HashMap<Integer, Object>();
    HashMap<Integer, String> sectionNaming = new HashMap<Integer, String>();

	int annId = 0;
    int sectId = 0;

	private static final String[] relations = {"r_checksum", "r_header_length",
			  "r_mbz", "r_monotonic_inc", "r_packet_type", "r_multiple",
			  "r_port", "r_sequence_number", "r_contains", "r_field_present",
			  "r_significant", "r_offset", "r_greater", "r_less", "r_range"};

	public DataWriter(String directory, String protoName) {
		this.dir = directory;
		this.protoName = protoName;
	}

	public boolean writeEntityReferenceFeatures(List<Map<Integer, Integer>> features) {
		String outfile = dir + protoName + ".out";

		if (features == null) {
			return false;
		}

		try {
			/* Open file */
			BufferedWriter out = new BufferedWriter(new FileWriter(outfile));

			/* Output feature info */
			for (Map<Integer, Integer> line : features) {
				/* Handle truth */
				if (line.containsKey(TRUTH_ID) || (line.containsKey(IS_MENTION_ID) && Config.eval_rule_based_baseline)) {
					out.write(line.get(TRUTH_ID) + " ");
				} else {
					out.write("0 ");
				}

				boolean first = false;
				if (line.containsKey(FIRST_ID)) {
					first = true;
				}

				/* Output all other features */
				for (Integer k : asSortedList(line.keySet())) {
					if (k < FEATURES_START_AT) {
						continue;
					}
					if (line.get(k) == UNKNOWN_VALUE) {
						continue;
					}
					out.write(k.toString() + ":" + line.get(k).toString() + " ");
				}
				if (first) {
					out.write(" #First");
				}
				out.write("\n");
			}

			/* Close file */
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Failed to write to: " + outfile);
			return false;
		}

		return true;
	}

	public boolean writeEntityReferenceDebug(List<String> data) {
		String outfile = dir + protoName + ".debug";

		if (data == null) {
			return false;
		}

		try {
			/* Open file */
			BufferedWriter out = new BufferedWriter(new FileWriter(outfile));

			/* Output feature info */
			for (String line : data) {
				out.write(line + "\n");
			}

			/* Close file */
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Failed to write to: " + outfile);
			return false;
		}

		return true;
	}

	public boolean writeEntities(List<Entity> entities) {
		String outfile = dir + protoName + ".ent";

		if (entities == null) {
			return false;
		}

		try {
			/* Open file */
			BufferedWriter out = new BufferedWriter(new FileWriter(outfile));

			/* Output feature info */
			for (Entity e : entities) {
				if (e.getType().equals("field")) {
					String width = e.getValue().replace("-bit", " bit").toLowerCase().trim();
					if (width.length() == 0) {
						width = "0";
					}
					out.write(e.getType() + "," + e.getId() + "," + e.getRealId() + ","
							+ e.getName() + "," + width + "\n");
				} else if (e.getType().equals("pkt-type")) {
					String val = e.getValue().toLowerCase().trim();
					if (val.length() == 0) {
						val = "-1";
					}
					out.write(e.getType() + "," + e.getId() + "," + e.getRealId() + ","
							+ e.getName() + "," + val + "\n");
				} else {
					out.write(e.getType() + "," + e.getId() + "," + e.getRealId() + ","
							+ e.getName() + "," + e.getValue() + "\n");
				}
			}

			/* Close file */
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Failed to write to: " + outfile);
			return false;
		}
		return true;
	}
	
	private static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
		List<T> list = new ArrayList<T>(c);
		java.util.Collections.sort(list);
		return list;
	}
	
	public boolean writeJSON(String filename, InputProtocol p) {
		try {
			/* Open file */
			BufferedWriter out = new BufferedWriter(new FileWriter(filename));
			
			/* Write data */
			out.write(p.exportJSON().toJSONString());
			
			/* Close file */
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Failed to write to: " + filename);
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public boolean writeRelations(String filename, List<InputProtocol> lp) {
		JSONObject obj = new JSONObject();
		JSONArray fileAnns = new JSONArray();

		for (InputProtocol p: lp) {
            //if (!p.getTitle().equals("IP.annote.txt.short")) {
            //    continue;
            //}

            System.out.println(p.getTitle());
			fileAnns.add(p.getTitle());
			writeRelation(p,obj);
		}
		obj.put("files", fileAnns);
		
		try {
			/* Open file */
			BufferedWriter out = new BufferedWriter(new FileWriter(filename));
			
			/* Write data */
			out.write(obj.toJSONString());
			
			/* Close file */
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Failed to write to: " + filename);
			return false;
		}
		return true;
	}
	
	private boolean recurseRelationSections(int currSectId, ProtocolSection sect, List<Entity> ents, List<List<HashMap<String, Object>> > sections) {
		System.out.println("##### Process Section: " + Integer.toString(currSectId) + " | " + sect.getTitle());

		sections.add(relationProcessChunks(currSectId, sect, ents));

		for (ProtocolSection s : sect.getSubsections()) {

            List<Integer> temp = (List<Integer>)sectionHierarchy.get(currSectId);
            temp.add(sectId);

            List<Integer> subSections = new ArrayList<Integer>();
            sectionHierarchy.put(sectId, subSections);

			if (s.getTitle() != null) {
                sectionNaming.put(sectId, s.getTitle().toString());
            } else {
                sectionNaming.put(sectId, "null");
            }

            int subSectId = sectId;
            sectId++;

			if(!recurseRelationSections(subSectId, s, ents, sections)) {
				return false;
			}
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private boolean writeRelation(InputProtocol p, JSONObject out) {
		List<List<HashMap<String, Object>> > sections = new ArrayList<List<HashMap<String, Object>>>();

        // clear global containers
		relationIds.clear();
        sectionHierarchy.clear();
		annId = 0; sectId = 0;


        /* Collect Info */
		for (ProtocolSection sect : p.getSections()) {
            List<Integer> subSections = new ArrayList<Integer>();
            sectionHierarchy.put(sectId, subSections);
            sectionNaming.put(sectId, sect.getTitle().toString());
            int currSectId = sectId;
            sectId++;
			recurseRelationSections(currSectId,sect,p.getEntities(),sections);
		}
		
		/* Write JSON */
		JSONObject fileObj = new JSONObject();

        JSONObject hierarchy = new JSONObject();
        for (Integer id: sectionHierarchy.keySet()) {
            JSONArray subsections = new JSONArray();
            List<Integer> temp = (List<Integer>)sectionHierarchy.get(id);
            for (Integer id2: temp) {
                subsections.add(id2);
            }

            hierarchy.put(id, subsections);
        }

        fileObj.put("hierarchy", hierarchy);


		JSONArray sectionAnns = new JSONArray();
		for (List<HashMap<String, Object>> sect: sections) {
			
			JSONObject sectionObj = new JSONObject();
			JSONArray chunkAnns = new JSONArray();
			
			for (HashMap<String, Object> chunk: sect) {
				JSONObject chunkObj = new JSONObject();
				chunkObj.put("id", (Integer)chunk.get("CHUNK_ID"));
				chunkObj.put("sent", (Integer)chunk.get("SENT_ID"));
				chunkObj.put("section", (String)chunk.get("SECTION"));
                chunkObj.put("section_id", (Integer)chunk.get("SECT_ID"));
				chunkObj.put("begin_char", (Integer)chunk.get("CHUNK_BEG_CHAR"));
				chunkObj.put("begin_word", (Integer)chunk.get("CHUNK_BEG_WORD"));
				chunkObj.put("begin_sent", (Integer)chunk.get("CHUNK_BEG_SENT"));
				chunkObj.put("end_char", (Integer)chunk.get("CHUNK_END_CHAR"));
				chunkObj.put("end_word", (Integer)chunk.get("CHUNK_END_WORD"));
				chunkObj.put("end_sent", (Integer)chunk.get("CHUNK_END_SENT"));
				
				if (chunk.get("ANN_RELATIONS") != null)  {
				    JSONArray relations = new JSONArray();
                    for (HashMap<String, Object> ann_rel: (List<HashMap<String, Object>>)chunk.get("ANN_RELATIONS")) {
                        JSONObject relObj = new JSONObject();
                        relObj.put("ann_rel", (String)ann_rel.get("ANN_REL"));
                        relObj.put("ann_rel_id", (Integer)ann_rel.get("ANN_REL_ID"));
                        relObj.put("ann_to_ent", (String)ann_rel.get("ANN_TO_ENT"));
                        relObj.put("ann_rel_str", (String)ann_rel.get("ANN_REL_STR"));
                        relObj.put("ann_expression", (String)ann_rel.get("ANN_EXPRESSION"));

                        if (ann_rel.get("ANN_TO_ENT_A") != null) {
                            relObj.put("ann_to_ent_A", (String)ann_rel.get("ANN_TO_ENT_A"));
                            relObj.put("ann_to_ent_B", (String)ann_rel.get("ANN_TO_ENT_B"));
                        }

                        relations.add(relObj);
                    }

                    chunkObj.put("ann_relations", relations);
				}
								
				if (chunk.get("ANN_ENTITIES") != null) {
				    JSONArray entities = new JSONArray();
                    for (HashMap<String, Object> ann_ent: (List<HashMap<String, Object>>)chunk.get("ANN_ENTITIES")) {
                        JSONObject entObj = new JSONObject();
                        entObj.put("ann_ent", (String)ann_ent.get("ANN_ENT"));
                        entObj.put("ann_ent_id", (String)ann_ent.get("ANN_ENT_ID"));
                        entObj.put("ann_ent_str", (String)ann_ent.get("ANN_ENT_STR"));

                        entities.add(entObj);
                    }

                    chunkObj.put("ann_entities", entities);

				}
				
				JSONArray words = new JSONArray();
				for (String w: (List<String>)chunk.get("WORDS")) {
					words.add(w);
				}
				chunkObj.put("words", words);
				
				JSONArray pos = new JSONArray();
				for (String w: (List<String>)chunk.get("POS")) {
					pos.add(w);
				}
				chunkObj.put("pos", pos);
				
				JSONArray entity_defs = new JSONArray();
				for (Integer w: (List<Integer>)chunk.get("ENTITY_DEF")) {
					entity_defs.add(w);
				}
				chunkObj.put("entity_defs", entity_defs);
				
				chunkAnns.add(chunkObj);
				
			}
			
			sectionObj.put("chunks", chunkAnns);
			sectionAnns.add(sectionObj);
		}
		fileObj.put("sections", sectionAnns);
		out.put(p.getTitle(), fileObj);
		return true;
	}
	
	private List<HashMap<String, Object>> relationProcessChunks(int currSectId, ProtocolSection sect, List<Entity> entitiesDefs) {

        //System.out.println(sect.getTitle());

		List<HashMap<String, Object>> info = new ArrayList<HashMap<String, Object>>();
		
		/* Get ground truth annotations */
		List<Annotation> relnTruth = getGoldRelnAnnotations(sect);
		List<Annotation> relnTruthTitle = getGoldRelnTitleAnnotations(sect);
		
		/* Get Entity References */
		List<Annotation> entities = new ArrayList<>();
		List<Annotation> entitiesTitle = new ArrayList<>();
		if (Config.eval_gold_entities) {
			if (Config.mention_dofields) {
				entities.addAll(sect.getTextAnnotations("fieldref"));
				entitiesTitle.addAll(sect.getTitleAnnotations("fieldref"));
			}
			if (Config.mention_dotypes) {
				entities.addAll(sect.getTextAnnotations("typeref"));
				entitiesTitle.addAll(sect.getTitleAnnotations("typeref"));
			}
            if (Config.mention_donames) {
                entities.addAll(sect.getTextAnnotations("fieldname"));
				entitiesTitle.addAll(sect.getTitleAnnotations("fieldname"));
            }
		} else {
      entities.addAll(sect.getTextAnnotations("int:entityreference"));
			for (Annotation a: entities) {
        boolean found = false;
				for (Entity e: entitiesDefs) {
					Integer i = e.getId();
					if (a.getId().equals(i.toString())) {
						found = true;
						Integer r = e.getRealId();
						a.setId(r.toString());
					}
				}
				if (!found) {
					a.setId("-1");
				}
			}
			entitiesTitle.addAll(sect.getTitleAnnotations("int:entityreference"));
			for (Annotation a: entitiesTitle) {
				boolean found = false;
				for (Entity e: entitiesDefs) {
					Integer i = e.getId();
					if (a.getId().equals(i.toString())) {
						found = true;
						Integer r = e.getRealId();
						a.setId(r.toString());
					}
				}
				if (!found) {
					a.setId("-1");
				}
			}
		}
		
		/* Get Definitions of Entities in this section */
		List<Entity> entityDefs = new ArrayList<Entity>();
		for (Entity e: entitiesDefs) {
			if (sect.getTitle() == null) {
				continue;
			}
			if (trimHeader(sect.getTitle().text()).equals(e.getLocation())) {
				entityDefs.add(e);
			}
		}
		
		int chunknum = 0;
		Sentence sent = sect.getTitle();
		for (Annotation c : sect.getTitleChunks()) {
			/* Build String and list of words */
			List<String> cwords = new ArrayList<>();
			for (int i = c.getBeginWordIndex(); i <= c.getEndWordIndex(); i++) {
				cwords.add(sent.originalText(i));
			}
			
			List<String> cposTags = new ArrayList<>();
			for (int i = c.getBeginWordIndex(); i <= c.getEndWordIndex(); i++) {
				cposTags.add(sent.posTags().get(i));
			}
			
			/* Create new map for this relation, chunk */
			HashMap<String, Object> values = new HashMap<>();
			ArrayList<Object> fieldNames = new ArrayList<>(); 
			values.put("CHUNK_ID", chunknum++);
			values.put("WORDS", cwords);
			values.put("POS", cposTags);
			values.put("CHUNK_BEG_CHAR", c.getBeginCharIndex());
			values.put("CHUNK BEG_SENT", c.getBeginWordIndex());
			values.put("CHUNK_BEG_WORD", c.getBeginWordIndex());
			values.put("CHUNK_END_CHAR", c.getEndCharIndex());
			values.put("CHUNK_END_WORD", c.getEndWordIndex());
			values.put("CHUNK_END_SENT", c.getEndSentIndex());
			values.put("SENT_ID", -1); // indicates title
            values.put("SECT_ID", currSectId);

            ArrayList<Object> relation_anns = new ArrayList();
            ArrayList<Object> entity_anns = new ArrayList();
			
			if (sect.getTitle() != null)
				values.put("SECTION", sect.getTitle().toString());
			else
				values.put("SECTION", "null");
							
			for (Annotation a : relnTruthTitle) {
				if (c.overlapping(a) || a.overlapping(c)) {

                    HashMap<String, Object> rel_ann = new HashMap<>();
					
					// This is the hack to give a unique Id
					// to each annotated relation
					int localId = 0;
					if (relationIds.containsKey(a.toString())) {
						localId = relationIds.get(a.toString());
					} else {
						localId = annId;
						relationIds.put(a.toString(), localId);
						annId++;
					}
					rel_ann.put("ANN_REL", a.getType());
					rel_ann.put("ANN_REL_ID", localId);
					rel_ann.put("ANN_TO_ENT", a.getId());
					rel_ann.put("ANN_REL_STR", a.toString());
					rel_ann.put("ANN_EXPRESSION", a.getExpression());
					List<List<Integer>> lr = a.getRefGroups();
					if (lr.size() > 0) {
						String ent_a = lr.get(0).toString();
						String ent_b = lr.get(1).toString();
						rel_ann.put("ANN_TO_ENT_A", ent_a.substring(1, ent_a.length() - 1));
						rel_ann.put("ANN_TO_ENT_B", ent_b.substring(1, ent_b.length() - 1));
					}

                    relation_anns.add(rel_ann);
					
				}

			}
						
			for (Annotation a : entitiesTitle) {
				if (c.overlapping(a) || a.overlapping(c)) {
                    HashMap<String, Object> ent_ann = new HashMap<>();

					ent_ann.put("ANN_ENT", a.getType());
					ent_ann.put("ANN_ENT_ID", a.getId());
					ent_ann.put("ANN_ENT_STR", a.toString());

                    entity_anns.add(ent_ann);
				}

			}
			
			for (Entity e: entityDefs) {
				fieldNames.add(e.getRealId());
			}
			
			values.put("ENTITY_DEF", fieldNames);
            values.put("ANN_RELATIONS", relation_anns);
            values.put("ANN_ENTITIES", entity_anns);
			info.add(values);
		}

        //System.out.println(sect.getText());
        //System.out.println("-----------------------------------");


		/* For each sentence */
        if (sect.getText() != null) {
            for (int s = 0; s < sect.getText().sentences().size(); s++) {
                sent = sect.getText().sentence(s);

                /* For each chunk */
                chunknum = 0;
                for (Annotation c : sect.getChunks(s)) {
                    /* Build String and list of words */
                    List<String> cwords = new ArrayList<>();
                    for (int i = c.getBeginWordIndex(); i <= c.getEndWordIndex(); i++) {
                        cwords.add(sent.originalText(i));
                    }

                    List<String> cposTags = new ArrayList<>();
                    for (int i = c.getBeginWordIndex(); i <= c.getEndWordIndex(); i++) {
                        cposTags.add(sent.posTags().get(i));
                    }

                    /* Create new map for this relation, chunk */
                    HashMap<String, Object> values = new HashMap<>();
                    ArrayList<Object> fieldNames = new ArrayList<>(); 
                    values.put("CHUNK_ID", chunknum++);
                    values.put("WORDS", cwords);
                    values.put("POS", cposTags);
                    values.put("CHUNK_BEG_CHAR", c.getBeginCharIndex());
                    values.put("CHUNK BEG_SENT", c.getBeginWordIndex());
                    values.put("CHUNK_BEG_WORD", c.getBeginWordIndex());
                    values.put("CHUNK_END_CHAR", c.getEndCharIndex());
                    values.put("CHUNK_END_WORD", c.getEndWordIndex());
                    values.put("CHUNK_END_SENT", c.getEndSentIndex());
                    values.put("SENT_ID", s);
                    values.put("SECT_ID", currSectId);

                    ArrayList<Object> relation_anns = new ArrayList();
                    ArrayList<Object> entity_anns = new ArrayList();

                    if (sect.getTitle() != null)
                        values.put("SECTION", sect.getTitle().toString());
                    else
                        values.put("SECTION", "null");

                    for (Annotation a : relnTruth) {
                        if (c.overlapping(a) || a.overlapping(c)) {

                            HashMap<String, Object> rel_ann = new HashMap<>();

                            // This is the hack to give a unique Id
                            // to each annotated relation
                            int localId = 0;
                            if (relationIds.containsKey(a.toString())) {
                                localId = relationIds.get(a.toString());
                            } else {
                                localId = annId;
                                relationIds.put(a.toString(), localId);
                                annId++;
                            }
                            rel_ann.put("ANN_REL", a.getType());
                            rel_ann.put("ANN_REL_ID", localId);
                            rel_ann.put("ANN_TO_ENT", a.getId());
                            rel_ann.put("ANN_REL_STR", a.toString());
                            rel_ann.put("ANN_EXPRESSION", a.getExpression());
                            List<List<Integer>> lr = a.getRefGroups();
                            if (lr.size() > 0) {
                                String ent_a = lr.get(0).toString();
                                String ent_b = lr.get(1).toString();
                                rel_ann.put("ANN_TO_ENT_A", ent_a.substring(1, ent_a.length() - 1));
                                rel_ann.put("ANN_TO_ENT_B", ent_b.substring(1, ent_b.length() - 1));
                            }

                            relation_anns.add(rel_ann);

                        }
                    }

                    for (Annotation a : entities) {
                        if (c.overlapping(a) || a.overlapping(c)) {
                            //System.out.println(a.toString());
                            HashMap<String, Object> ent_ann = new HashMap<>();

                            ent_ann.put("ANN_ENT", a.getType());
                            ent_ann.put("ANN_ENT_ID", a.getId());
                            ent_ann.put("ANN_ENT_STR", a.toString());

                            entity_anns.add(ent_ann);
                        }
                    }

                    for (Entity e: entityDefs) {
                        fieldNames.add(e.getRealId());
                    }

                    values.put("ENTITY_DEF", fieldNames);
                    values.put("ANN_RELATIONS", relation_anns);
                    values.put("ANN_ENTITIES", entity_anns);

                    info.add(values);
                }
			}
		}
		
        return info;
	}	
	
	private List<Annotation> getGoldRelnAnnotations(ProtocolSection sect) {
		List<Annotation> relnTruth = new ArrayList<Annotation>();
				
		// Get all relations annotated in the section 
		for (String rel: relations) {
			List<Annotation> anns = sect.getTextAnnotations(rel);
			relnTruth.addAll(anns);	
		}
		return relnTruth;

	}
	
	private List<Annotation> getGoldRelnTitleAnnotations(ProtocolSection sect) {
		List<Annotation> relnTruth = new ArrayList<Annotation>();
				
		// Get all relations annotated in the title
		for (String rel: relations) {
			List<Annotation> titleAnns = sect.getTitleAnnotations(rel);
			relnTruth.addAll(titleAnns);

		}
		return relnTruth;

	}
	
	private String trimHeader(String header) {
		if (header.contains(":")) {
			header = header.split(":")[0];
		}
		return header.trim();
	}
}
