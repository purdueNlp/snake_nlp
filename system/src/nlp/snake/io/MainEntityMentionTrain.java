package nlp.snake.io;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nlp.snake.config.Config;
import nlp.snake.docprocessors.AnnotationStatistics;
import nlp.snake.docprocessors.DocProcessor;
import nlp.snake.docprocessors.SplitFields;
import nlp.snake.ent.Entity;
import nlp.snake.ent.InputProtocol;
import nlp.snake.entityextractors.EntitiesFromHeaders;
import nlp.snake.entityextractors.EntityExtractor;
import nlp.snake.featurizers.Featurizer;
import nlp.snake.featurizers.FieldInChunkFeaturizer;
import nlp.snake.learn.Learner;
import nlp.snake.learn.NullLearner;
import nlp.snake.learn.svmLightLearner;

public class MainEntityMentionTrain {
	private static final double VERSION = 1.0;
	private static final int COPYRIGHT_YEAR = 2018;

	public static void main(String[] args) {
		List<String> files = new ArrayList<>();
		List<InputProtocol> protos = new ArrayList<>();
		String svmlight_bin_dir = "";
		String tmpdir = "";
		String svmlight_model = "";
		boolean evaluate = false;

		/* Parse Arguments */
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-d")) {
				Config.debug++;
			} else if (args[i].equals("-v")) {
				version();
			} else if (args[i].equals("-h")) {
				usage();
			} else if (args[i].equals("-e")) {
				evaluate = true;
			} else if (args[i].equals("-g")) {
				Config.eval_gold_entities = true;
			} else if (args[i].equals("-b")) {
				if (i + 1 >= args.length) {
					System.err.println("-b option requires an argument");
					usage();
				}
				svmlight_bin_dir = args[i + 1];
				i++;
			} else if (args[i].equals("-t")) {
				if (i + 1 >= args.length) {
					System.err.println("-t option requires an argument");
					usage();
				}
				tmpdir = args[i + 1];
				i++;
			} else if (args[i].equals("-m")) {
				if (i + 1 >= args.length) {
					System.err.println("-m option requires an argument");
					usage();
				}
				svmlight_model = args[i + 1];
				i++;
			} else if (args[i].startsWith("-")) {
				usage();
			} else {
				files.add(args[i]);
			}
		}
		if (files.size() == 0) {
			usage();
		}
		if (svmlight_model.length()==0 || svmlight_bin_dir.length()==0) {
			usage();
		}

		/* Read Inputfile */
		for (String f : files) {
			DataReader reader = new DataReader();
			InputProtocol p = reader.readJSON(f);
			if (p == null) {
				System.err.println("Failed to Read: " + f);
				continue;
			}

			/* Debug Structure Output */
			if (Config.debug > 1) {
				p.print(System.out, true);
			}

			protos.add(p);
		}

		/* Create Entity Reference Featurizer */
		Featurizer f = null;
		// f = new NullFeaturizer();
		// f = new FieldFinderRuleBased();
		f = new FieldInChunkFeaturizer();

		/* Create Learner */
		Learner l = null;
		l = new svmLightLearner(svmlight_bin_dir, tmpdir, svmlight_model);

		/* Process Documents */
		List<Map<Integer, Integer>> all = new ArrayList<>();
		for (InputProtocol p : protos) {
			List<Map<Integer, Integer>> features = null;
			List<String> debug_output = null;

			features = f.processDocument(p);
			debug_output = f.getDebugOutput();
			
			if (Config.debug > 1) {
				DataWriter writer = new DataWriter(tmpdir, p.getTitle());
				writer.writeEntityReferenceFeatures(features);
				writer.writeEntityReferenceDebug(debug_output);
			}
			all.addAll(features);
		}
		l.train(all);
		
		if (evaluate) {
			l.classify(all);
			l.evaluate(System.out);
		}
		if (!Config.eval_rule_based_baseline) {
			l.clear();
		}
	}

	private static void version() {
		System.err.println("NLP for RFCs version " + VERSION);
		System.err.println("Copyright (C) " + COPYRIGHT_YEAR + " Samuel Jero <sjero@sjero.net>");
		System.exit(0);
	}

	private static void usage() {
		System.err
				.println("Usage: [-v] [-h] [-d] [-g] [-e] [-b svm_light_bin_dir] [-t tmp_dir][-m model] input_files");
		System.err.println("       -v   Version Information");
		System.err.println("       -h   Help");
		System.err.println("       -d   Debug. Maybe repeated for additional verbosity");
		System.err.println("       -g   Use gold entities, instead of extracted ones");
		System.err.println("       -e   Do evaluation");
		System.err.println("       -b   SVMLight bin directory");
		System.err.println("       -t   Temporary directory for SVMLight temp files");
		System.err.println("       -m   SVMLight model");
		System.exit(0);
	}
}
