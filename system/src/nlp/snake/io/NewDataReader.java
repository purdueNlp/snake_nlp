package nlp.snake.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import nlp.snake.config.Config;
import nlp.snake.ent.ASCIIImage;
import nlp.snake.ent.ASCIITable;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;

public class NewDataReader {
	
	private class Paragraph {
		private List<ASCIIImage> images = new LinkedList<>();
		private List<ASCIITable> tables = new LinkedList<>();
		private String text = "";
		private int numlines = 0;
		
		public void addLine(String line) {
			numlines++;
			text += line + "\n";
		}
		
		public String getText() {
			return text;
		}
		
		public int indentation() {
			int lvl = 0;
			while (lvl < text.length() && Character.isWhitespace(text.charAt(lvl))) {
				lvl++;
			}
			return lvl;
		}
		
		public int lines() {
			return numlines;
		}
		
		public boolean empty() {
			return text.length() == 0;
		}
		
		public List<ASCIIImage> getImages() {
			return images;
		}

		public List<ASCIITable> getTables() {
			return tables;
		}
		
		public void addImage(ASCIIImage image) {
			images.add(image);
		}

		public void addTable(ASCIITable table) {
			tables.add(table);
		}
	}
	
	private class Result {
		public ProtocolSection sect;
		public int index;

		public Result(ProtocolSection sect,int index) {
			this.sect=sect;
			this.index=index;
		}
	}
	
	
	public InputProtocol readProtocolFile(String file) {
		InputProtocol protocol = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			ArrayList<String> doctext = new ArrayList<>();
			List<ProtocolSection> sections = new ArrayList<>();
			String tmp;

			/* Read text */
			while ((tmp = br.readLine()) != null) {
				doctext.add(tmp);
			}
			br.close();
			
			for (int i=0; i < doctext.size();i++) {
				Result ret = parseTopSection(doctext,i);
				i=ret.index;
				sections.add(ret.sect);
			}

			if (file.contains("/")) {
				file = file.substring(file.lastIndexOf("/") + 1);
			}

			protocol = new InputProtocol(file, sections);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return protocol;
	}
	
	private Result parseTopSection(ArrayList<String> lines, int i) {
		boolean inImageTag = false;
		boolean inTableTag = false;
		ArrayList<Integer> cur = null;
		ArrayList<Integer> next = null;
		ArrayList<ProtocolSection> subsections = new ArrayList<>();
		ArrayList<String> newtext = new ArrayList<>();
		
		for (; i < lines.size();i++) {
			// start a new section
			if (numberedSectionStart(lines.get(i)) && !inImageTag && !inTableTag) {
				System.out.println(lines.get(i));
				next = parseNumbers(lines.get(i));
				if (cur == null) {
					cur = next;
				} else if (numberSubsection(next,cur)) {
					Result ret = parseTopSection(lines,i);
					i=ret.index;
					subsections.add(ret.sect);
					continue;
				} else {
					/* Hit sibling or super-section */
					i=i-1;
					break;
				}
				newtext.clear();
			}
			if (imageStart(lines.get(i)) && !inImageTag) {
				inImageTag = true;
				newtext.add(lines.get(i));
			} else if (tableStart(lines.get(i)) && !inTableTag) {
				inTableTag = true;
				newtext.add(lines.get(i));
			} else if (imageEnd(lines.get(i)) && inImageTag) {
				inImageTag = false;
				newtext.add(lines.get(i));
			} else if (tableEnd(lines.get(i)) && inTableTag) {
				inTableTag = false;
				newtext.add(lines.get(i));
			} else {
				newtext.add(lines.get(i));
			}
		}
		
		/*Process This Section*/
		ProtocolSection s = parseNumberedSection(newtext);
		for (ProtocolSection sub: subsections) {
			s.addSubsection(sub);
		}
		return new Result(s,i);
	}
	
	private ProtocolSection parseNumberedSection(ArrayList<String> text) {
		boolean inImageTag = false;
		boolean inTableTag = false;
		String imageText = "";
		String tableText = "";
		List<Paragraph> paras = new ArrayList<>();
		Paragraph cur = new Paragraph();
		String tmp;

		/* Remove page headers */
		text = stripPageHeaders(text);

		/* Parse into Paragraphs and tables/images */
		for (String line: text) {
			tmp = line;
			if (imageStart(tmp) && !inImageTag) {
				inImageTag = true;
				if (tmp.length() > 0) {
					imageText = tmp + "\n";
				}
			} else if (tableStart(tmp) && !inTableTag) {
				inTableTag = true;
				if (tmp.length() > 0) {
					tableText = tmp + "\n";
				}
			} else if (imageEnd(tmp) && inImageTag) {
				inImageTag = false;
				if (tmp.length() > 0) {
					imageText += tmp;
				}
				cur.addImage(new ASCIIImage(imageText));
			} else if (tableEnd(tmp) && inTableTag) {
				inTableTag = false;
				if (tmp.length() > 0) {
					tableText += tmp;
				}
				cur.addTable(new ASCIITable(tableText));
			} else if (inTableTag) {
				tableText += tmp + "\n";
			} else if (inImageTag) {
				imageText += tmp + "\n";
			} else {
				/* Default line handler */
				String naline = removeAnnotation(tmp);
				boolean isEmpty = !nonBlank(naline);
				int indent = indentationLevel(tmp);
				
				if (isEmpty) {
					/* Empty Line breaks paragraphs */
					if (!cur.empty()) {
						paras.add(cur);
						cur = new Paragraph();
					}
				} else if (indent != cur.indentation()) {
					/* Change in indent breaks paragraphs */
					if (!cur.empty()) {
						paras.add(cur);
						cur = new Paragraph();
					}
					cur.addLine(tmp + "\n");
				} else {
					cur.addLine(tmp + "\n");
				}
			}
		}
		if (!cur.empty()) {
			paras.add(cur);
		}

		/*for(Paragraph p: paras) {
			System.out.println("########P");
			System.out.println(p.getText());
		}*/

		/* Parse into Sections */
		ProtocolSection s = null;
		int i = 0;
		while (i < paras.size()) {
			Result res = parseSection(paras,i);
			if (s == null) {
				s = res.sect;
			} else {
				s.addSubsection(res.sect);
			}
			i = res.index;
		}
		
		return s;
	}

	private boolean lowerIndent(Paragraph cur, Paragraph next) {
		if (cur == null || next == null) {
			return false;
		}
		return cur.indentation() < next.indentation();
	}

	private boolean higherIndent(int prev, Paragraph cur) {
		if (cur == null) {
			return false;
		}
		return prev > cur.indentation();
	}
	
	private boolean singleThenLower(Paragraph prev, Paragraph cur, Paragraph next) {
		if (cur == null || next == null || prev == null) {
			return false;
		}
		return prev.indentation() == cur.indentation() && cur.lines() == 1 && cur.indentation() < next.indentation();
	}
	
	private boolean allLinesShort(Paragraph cur) {
		if (cur == null) {
			return false;
		}
		if (cur.lines() <= 1) {
			return false;
		}
		if (cur.getText().length() == 0) {
			return false;
		}
		
		String lines[] = cur.getText().split("\n");
		for (String l: lines) {
			if (removeAnnotation(l).length() > 0.7*80) {
				return false;
			}
		}
		return true;
	}

	private Result parseSection(List<Paragraph> paras, int index) {
		String text = "";
		String title = "";
		ProtocolSection sect = new ProtocolSection();
		Paragraph cur = null;
		int indent = -1;
		Paragraph prev = null;
		Paragraph next = null;
		
		/* Setup markers */
		cur = paras.get(index);
		if (index - 1 > 0) {
			prev = paras.get(index - 1);
		}
		if (index + 1 < paras.size()) {
			next = paras.get(index + 1);
		}
		
		
		/* Could this paragraph be a title? */
		if (cur.lines() == 1) {
			/* Process Title */
			title = findTitleInFirstLine(cur.getText());
			sect.setSectionTitle(title);
			text = findTextInFirstLine(cur.getText());
			indent = cur.indentation();
			index++;
			
			/* Is there a body in the next paragraph? */
			if (!text.isEmpty() && !lowerIndent(cur,next)) {
				for (ASCIIImage i: cur.getImages()) {
					sect.addImage(i);
				}
				for (ASCIITable t: cur.getTables()) {
					sect.addTable(t);
				}
				sect.setSectionText(text);
				return new Result(sect,index);
			}
		}
		
		/* Setup markers */
		prev = null;
		if (index < paras.size()) {
			cur = paras.get(index);
		} else {
			cur = null;
		}
		if (indent == -1) {
			indent = cur.indentation();
		}
		if (index + 1 < paras.size()) {
			next = paras.get(index + 1);
		} else {
			next = null;
		}
		
		/* Parse section body */
		while(cur != null && !higherIndent(indent,cur)) {
			
			/* Check for subsection */
			if (lowerIndent(prev,cur) || singleThenLower(cur,cur,next)||looksLikeTitle(cur)) {
				Result r = parseSection(paras,index);
				index = r.index;
				sect.addSubsection(r.sect);
				cur = prev;
			} else if (allLinesShort(cur) && Config.shortLinesAreSection) {
				String lines[] = cur.getText().split("\n");
				for (String l: lines) {
					if (l.isEmpty()) {
						continue;
					}
					ProtocolSection s = new ProtocolSection();
					s.setSectionTitle(l);
					sect.addSubsection(s);
				}
				index++;
				indent = cur.indentation();
			} else {
				for (ASCIIImage i: cur.getImages()) {
					sect.addImage(i);
				}
				for (ASCIITable t: cur.getTables()) {
					sect.addTable(t);
				}
				text += cur.getText();
				index++;
				indent = cur.indentation();
			}
			
			
			/* Setup markers */
			prev = cur;
			if (index < paras.size()) {
				cur = paras.get(index);
			} else {
				cur = null;
			}
			if (index + 1 < paras.size()) {
				next = paras.get(index + 1);
			} else {
				next = null;
			}
		}

		sect.setSectionText(text);
		return new Result(sect,index);
	}

	private String removeAnnotation(String str) {
		while (str.indexOf("<tag") != -1) {
			int indexS = str.indexOf("<tag");
			int indexE = str.indexOf("/>", indexS) + 2;
			String s1 = str.substring(0, indexS);
			String s2 = str.substring(indexE);
			if (s1.length() > 0 && s2.length() > 0
					&& s1.codePointAt(s1.length() - 1) != ' '
					&& s2.codePointAt(0) != ' ' && s2.codePointAt(0) != ','
					&& s2.codePointAt(0) != '.') {
				str = str.substring(0, indexS) + " " + str.substring(indexE);
			} else {
				str = str.substring(0, indexS) + str.substring(indexE);
			}
		}
		return str;
	}

	private ArrayList<String> stripPageHeaders(ArrayList<String> text) {
		Pattern p = Pattern.compile("[^\\[]*\\[Page [0-9]+\\].*");
		int skiplines = 0;
		boolean inheader = false;
		ArrayList<String> out = new ArrayList<>();

		for (String line : text) {
			if (p.matcher(line).matches()) {
				skiplines = 2;
				inheader = true;
				continue;
			}
			if (skiplines > 0) {
				skiplines--;
				continue;
			}
			if (inheader && !line.isEmpty()) {
				continue;
			}
			inheader = false;
			out.add(line);
			skiplines = 0;
		}

		return out;
	}

	private boolean tableEnd(String tmp) {
		if (tmp.contains("asciiTable") && tmp.contains("value=\"end\""))
			return true;
		return false;
	}

	private boolean imageEnd(String tmp) {
		if (tmp.contains("asciiImage") && tmp.contains("value=\"end\""))
			return true;
		return false;
	}

	private boolean tableStart(String tmp) {
		if (tmp.contains("asciiTable") && tmp.contains("value=\"start\""))
			return true;
		return false;
	}

	private boolean imageStart(String tmp) {
		if (tmp.contains("asciiImage") && tmp.contains("value=\"start\""))
			return true;
		return false;
	}

	private int indentationLevel(String str) {
		int lvl = 0;
		while (lvl < str.length() && Character.isWhitespace(str.charAt(lvl))) {
			lvl++;
		}
		return lvl;
	}

	private boolean looksLikeTitle(Paragraph cur) {
		if (cur == null) {
			return false;
		}
		if (cur.lines() != 1) {
			return false;
		}
		String ln_no_annotation = removeAnnotation(cur.getText());
		String ln_trimed = ln_no_annotation.trim();

		if (hasLongWhitespace(ln_trimed)) {
			return true;
		}
		return false;
	}

	private String findTitleInFirstLine(String ln) {
		ln = ln.trim();
		int whitespace = 0;

		for (int i = 0; i < ln.length(); i++) {
			if (Character.isWhitespace(ln.charAt(i))) {
				whitespace++;
			} else {
				whitespace = 0;
			}
			if (whitespace > 3) {
				ln = ln.substring(0, i);
				ln = ln.trim();
				return ln;
			}
		}

		return ln;
	}

	private String findTextInFirstLine(String ln) {
		ln = ln.trim();
		int whitespace = 0;

		for (int i = 0; i < ln.length(); i++) {
			if (Character.isWhitespace(ln.charAt(i))) {
				whitespace++;
			} else {
				whitespace = 0;
			}
			if (whitespace > 3) {
				ln = ln.substring(i);
				ln = ln.trim();
				return ln;
			}
		}

		return "";
	}

	private boolean hasLongWhitespace(String ln) {
		ln = ln.trim();
		int whitespace = 0;

		for (int i = 0; i < ln.length(); i++) {
			if (Character.isWhitespace(ln.charAt(i))) {
				whitespace++;
			} else {
				whitespace = 0;
			}
			if (whitespace > 3) {
				return true;
			}
		}

		return false;
	}

	private boolean nonBlank(String ln) {
		ln = ln.trim();
		if (!ln.isEmpty()) {
			return true;
		}
		return false;
	}
	
	private ArrayList<Integer> parseNumbers(String str) {
		ArrayList<Integer> out = new ArrayList<>();
		
		int i=0;
		str = removeAnnotation(str);
		String digits = "";
		while (i < str.toCharArray().length && str.toCharArray()[i] != ' ') {
			if (Character.isDigit(str.toCharArray()[i])) {
				digits+=str.toCharArray()[i];
			}else if (str.toCharArray()[i] == '.') {
				try {
					out.add(Integer.parseInt(digits));
				}catch(Exception e) {
					System.err.print(e.toString());
				}
				digits = "";
			}
			i++;
		}
		if (!digits.isEmpty()) {
			try {
				out.add(Integer.parseInt(digits));
			}catch(Exception e) {
				System.err.print(e.toString());
			}
			digits = "";
		}
		return out;
	}
	
	private boolean numberSubsection(ArrayList<Integer> a, ArrayList<Integer> b) {
		int num = (a.size()<b.size())? a.size() : b.size();
		for (int i=0; i < num;i++) {
			if (!a.get(i).equals(b.get(i))) {
				return false;
			}
		}
		return true;
	}
	
	private boolean numberedSectionStart(String str) {
		int i = 0;
		int digit = 0;
		int dot = 0;
		str = removeAnnotation(str);
		while (i < str.toCharArray().length && str.toCharArray()[i] != ' ') {
			if (Character.isDigit(str.toCharArray()[i])) {
				i++;
				digit++;
			} else if (str.toCharArray()[i] == '.') {
				i++;
				dot++;
			} else {
				break;
			}
		}

		if (i < str.toCharArray().length && digit > 0 && dot > 0) {
			return true;
		}

		return false;
	}
	
	public InputProtocol readJSON(String filename) {
		JSONParser parser = new JSONParser();
		InputProtocol p = new InputProtocol();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			Object obj = parser.parse(br);
			
            JSONObject jsonObject = (JSONObject) obj;
            if (!p.importJSON(jsonObject)) {
            	br.close();
            	return null;
            }
            
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return p;
	}

}
