package nlp.snake.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import nlp.snake.ent.ASCIIImage;
import nlp.snake.ent.ASCIITable;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;

public class DataReader {
	public InputProtocol readProtocolFile(String file) {
		InputProtocol protocol = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String tmp;

			List<ProtocolSection> sections = new ArrayList<>();
			List<ASCIIImage> images = new ArrayList<>();
			List<ASCIITable> tables = new ArrayList<>();
			boolean inImageTag = false;
			boolean inTableTag = false;
			String imageText = "";
			String tableText = "";
			String text = "";
			String title = "";

			while ((tmp = br.readLine()) != null) {
				// start a new section
				if (sectionStart(tmp) && !inImageTag && !inTableTag) {
					if (text.length() > 0 || images.size() > 0
							|| tables.size() > 0) {
						sections.add(parseSection(title, text, tables, images));
					}
					title = tmp;
					text = "";
					images.clear();
					tables.clear();
				}
				// start image/table tag
				else if (imageStart(tmp) && !inImageTag) {
					inImageTag = true;
					tmp = removeAnnotation(tmp);
					if (tmp.length() > 0) {
						imageText = tmp + "\n";
					}
				} else if (tableStart(tmp) && !inTableTag) {
					inTableTag = true;
					tmp = removeAnnotation(tmp);
					if (tmp.length() > 0) {
						tableText = tmp + "\n";
					}
				} else if (imageEnd(tmp) && inImageTag) {
					inImageTag = false;
					tmp = removeAnnotation(tmp);
					if (tmp.length() > 0) {
						imageText += tmp;
					}
					images.add(new ASCIIImage(imageText));
				} else if (tableEnd(tmp) && inTableTag) {
					inTableTag = false;
					tmp = removeAnnotation(tmp);
					if (tmp.length() > 0) {
						tableText += tmp;
					}
					tables.add(new ASCIITable(tableText));
				} else {
					if (inTableTag) {
						tableText += tmp + "\n";
					} else if (inImageTag) {
						imageText += tmp + "\n";
					} else {
						text += tmp + "\n";
					}
				}
			}
			br.close();
			if (text.length() > 0 || images.size() > 0 || tables.size() > 0) {
				sections.add(parseSection(title, text, tables, images));
			}
			if (file.contains("/")) {
				file = file.substring(file.lastIndexOf("/") + 1);
			}

			protocol = new InputProtocol(file, sections);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return protocol;
	}

	private ProtocolSection parseSection(String title, String text,
			List<ASCIITable> tables, List<ASCIIImage> images) {
		String myText = "";
		String subText = "";
		String subTitle = "";
		boolean inSubsection = false;
		ProtocolSection sect = new ProtocolSection();

		/* Process title */
		String textFromTitle = findTextInFirstLine(title);
		if (!textFromTitle.isEmpty()) {
			myText = textFromTitle + "\n";
		}
		title = findTitleInFirstLine(title);
		sect.setSectionTitle(title);

		/* Remove page headers */
		text = stripPageHeaders(text);

		/* Loop over all lines in section */
		int indent = 0;
		int lastindent = 0;
		int nextindent = 0;
		String lines[] = text.split("\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];

			/* Current indentation level */
			indent = indentationLevel(line);

			/* Indentation level of last non-blank line */
			int j = i - 1;
			while (j >= 0 && !nonBlank(lines[j])) {
				j--;
			}
			if (j >= 0) {
				lastindent = indentationLevel(lines[j]);
			} else {
				lastindent = -1;
			}

			/* Indentation level of next non-blank line */
			j = i + 1;
			while (j < lines.length && !nonBlank(lines[j])) {
				j++;
			}
			if (j < lines.length) {
				nextindent = indentationLevel(lines[j]);
			} else {
				nextindent = -1;
			}

			/* Is end of subsection, based on indentation */
			if (nonBlank(line) && lastindent != -1 && lastindent > indent) {
				if (inSubsection) {
					sect.addSubsection(parseSection(subTitle, subText,
							new ArrayList<ASCIITable>(),
							new ArrayList<ASCIIImage>()));
				}
				subTitle = "";
				subText = "";
				inSubsection = false;
			}

			/* Is new subsection, based on indentation */
			if ((nonBlank(line) && nextindent != -1 && nextindent > indent
					&& i - 1 >= 0 && lines[i - 1].isEmpty())
					|| (nonBlank(line) && hasLongWhitespace(line))) {
				if (inSubsection) {
					sect.addSubsection(parseSection(subTitle, subText,
							new ArrayList<ASCIITable>(),
							new ArrayList<ASCIIImage>()));
				}
				if (looksLikeTitle(line)) {
					subTitle = line.trim();
					subText = "";
				} else {
					subTitle = "";
					subText = line.trim() + "\n";
				}
				inSubsection = true;
			} else {
				if (inSubsection) {
					/* Subsection */
					subText += line + "\n";
				} else {
					/* My Text Section */
					line = line.trim();
					if (line.isEmpty()) {
						continue;
					}
					if (line.endsWith("-")) {
						myText += line;
					} else {
						myText += line + "\n";
					}
				}
			}
		}

		/* Add any remaining subsection */
		if (inSubsection) {
			sect.addSubsection(parseSection(subTitle, subText,
					new ArrayList<ASCIITable>(), new ArrayList<ASCIIImage>()));
			inSubsection = false;
		}

		/* Setup text section */
		sect.setSectionText(myText);

		for (ASCIITable t : tables) {
			sect.addTable(t);
		}

		for (ASCIIImage i : images) {
			sect.addImage(i);
		}

		return sect;
	}

	private String removeAnnotation(String str) {
		while (str.indexOf("<tag") != -1) {
			int indexS = str.indexOf("<tag");
			int indexE = str.indexOf("/>", indexS) + 2;
			String s1 = str.substring(0, indexS);
			String s2 = str.substring(indexE);
			if (s1.length() > 0 && s2.length() > 0
					&& s1.codePointAt(s1.length() - 1) != ' '
					&& s2.codePointAt(0) != ' ' && s2.codePointAt(0) != ','
					&& s2.codePointAt(0) != '.') {
				str = str.substring(0, indexS) + " " + str.substring(indexE);
			} else {
				str = str.substring(0, indexS) + str.substring(indexE);
			}
			str = str.trim();
		}
		return str;
	}

	private String stripPageHeaders(String text) {
		Pattern p = Pattern.compile("[^\\[]*\\[Page [0-9]+\\].*");
		String out = "";
		int skiplines = 0;
		boolean inheader = false;

		for (String line : text.split("\n")) {
			if (p.matcher(line).matches()) {
				skiplines = 2;
				inheader = true;
				continue;
			}
			if (skiplines > 0) {
				skiplines--;
				continue;
			}
			if (inheader && !line.isEmpty()) {
				continue;
			}
			inheader = false;
			out += line + "\n";
			skiplines = 0;
		}

		return out;
	}

	private boolean tableEnd(String tmp) {
		if (tmp.contains("asciiTable") && tmp.contains("value=\"end\""))
			return true;
		return false;
	}

	private boolean imageEnd(String tmp) {
		if (tmp.contains("asciiImage") && tmp.contains("value=\"end\""))
			return true;
		return false;
	}

	private boolean tableStart(String tmp) {
		if (tmp.contains("asciiTable") && tmp.contains("value=\"start\""))
			return true;
		return false;
	}

	private boolean imageStart(String tmp) {
		if (tmp.contains("asciiImage") && tmp.contains("value=\"start\""))
			return true;
		return false;
	}

	private boolean sectionStart(String str) {
		int i = 0;
		int digit = 0;
		int dot = 0;
		while (i < str.toCharArray().length && str.toCharArray()[i] != ' ') {
			if (Character.isDigit(str.toCharArray()[i])) {
				i++;
				digit++;
			} else if (str.toCharArray()[i] == '.') {
				i++;
				dot++;
			} else {
				break;
			}
		}

		if (i < str.toCharArray().length && digit > 0 && dot > 0) {
			return true;
		}

		return false;
	}

	private int indentationLevel(String str) {
		int lvl = 0;
		while (lvl < str.length() && Character.isWhitespace(str.charAt(lvl))) {
			lvl++;
		}
		return lvl;
	}

	private boolean looksLikeTitle(String ln) {
		String ln_no_annotation = removeAnnotation(ln);
		String ln_trimed = ln_no_annotation.trim();
		int upper = 0;

		for (int i = 0; i < ln_trimed.length(); i++) {
			if (Character.isUpperCase(ln_trimed.charAt(i))) {
				upper++;
			}
		}

		if (hasLongWhitespace(ln_trimed)) {
			return true;
		}

		if (upper > 5) {
			return true;
		}

		if (ln_no_annotation.length() > (0.7 * 80)) {
			return false;
		}

		if (ln_trimed.length() < (0.6 * 80)) {
			return true;
		}

		if (upper > 2) {
			return true;
		}

		if (ln_trimed.endsWith(".") || ln_trimed.contains(",")) {
			return false;
		}

		if (ln_trimed.length() < 3) {
			return false;
		}

		return false;
	}

	private String findTitleInFirstLine(String ln) {
		ln = ln.trim();
		int whitespace = 0;

		for (int i = 0; i < ln.length(); i++) {
			if (Character.isWhitespace(ln.charAt(i))) {
				whitespace++;
			} else {
				whitespace = 0;
			}
			if (whitespace > 3) {
				ln = ln.substring(0, i);
				ln = ln.trim();
				return ln;
			}
		}

		return ln;
	}

	private String findTextInFirstLine(String ln) {
		ln = ln.trim();
		int whitespace = 0;

		for (int i = 0; i < ln.length(); i++) {
			if (Character.isWhitespace(ln.charAt(i))) {
				whitespace++;
			} else {
				whitespace = 0;
			}
			if (whitespace > 3) {
				ln = ln.substring(i);
				ln = ln.trim();
				return ln;
			}
		}

		return "";
	}

	private boolean hasLongWhitespace(String ln) {
		ln = ln.trim();
		int whitespace = 0;

		for (int i = 0; i < ln.length(); i++) {
			if (Character.isWhitespace(ln.charAt(i))) {
				whitespace++;
			} else {
				whitespace = 0;
			}
			if (whitespace > 3) {
				return true;
			}
		}

		return false;
	}

	private boolean nonBlank(String ln) {
		ln = ln.trim();
		if (!ln.isEmpty()) {
			return true;
		}
		return false;
	}
	
	public InputProtocol readJSON(String filename) {
		JSONParser parser = new JSONParser();
		InputProtocol p = new InputProtocol();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			Object obj = parser.parse(br);
			
            JSONObject jsonObject = (JSONObject) obj;
            if (!p.importJSON(jsonObject)) {
            	br.close();
            	return null;
            }
            
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return p;
	}

}
