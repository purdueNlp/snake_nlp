package nlp.snake.io;

import java.util.ArrayList;
import java.util.List;

import nlp.snake.config.Config;
import nlp.snake.ent.InputProtocol;

public class MainRelationExtract {
	private static final double VERSION = 1.0;
	private static final int COPYRIGHT_YEAR = 2018;

	public static void main(String[] args) {
		List<String> files = new ArrayList<>();
		List<InputProtocol> protos = new ArrayList<>();
		String outputdir = "";

		/* Parse Arguments */
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-d")) {
				Config.debug++;
			} else if (args[i].equals("-v")) {
				version();
			} else if (args[i].equals("-h")) {
				usage();
			} else if (args[i].equals("-g")) {
				Config.eval_gold_entities = true;
			} else if (args[i].equals("-o")) {
				if (i + 1 >= args.length) {
					System.err.println("-o option requires an argument");
					usage();
				}
				outputdir = args[i + 1];
				i++;
			} else if (args[i].startsWith("-")) {
				usage();
			} else {
				files.add(args[i]);
			}
		}
		if (files.size() == 0) {
			usage();
		}

		/* Read Inputfile */
		for (String f : files) {
			DataReader reader = new DataReader();
			InputProtocol p = reader.readJSON(f);
			if (p == null) {
				System.err.println("Failed to Read: " + f);
				continue;
			}

			/* Debug Structure Output */
			if (Config.debug > 1) {
				p.print(System.out, true);
			}

			protos.add(p);
		}

		/* Process Documents */
		DataWriter writer = new DataWriter(outputdir, "");
		writer.writeRelations(outputdir + "relations.json", protos);
	}

	private static void version() {
		System.err.println("NLP for RFCs version " + VERSION);
		System.err.println("Copyright (C) " + COPYRIGHT_YEAR + " Samuel Jero <sjero@sjero.net>");
		System.exit(0);
	}

	private static void usage() {
		System.err
				.println("Usage: [-v] [-h] [-d] [-g] [-o outputdir] input_files");
		System.err.println("       -v   Version Information");
		System.err.println("       -h   Help");
		System.err.println("       -d   Debug. Maybe repeated for additional verbosity");
		System.err.println("       -g   Use gold entities, instead of extracted ones");
		System.err.println("       -o   Output directory");
		System.exit(0);
	}
}
