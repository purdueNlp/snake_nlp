package nlp.snake.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import nlp.snake.config.Config;

public class svmLightWrapper {
	protected static final int TRUTH_ID = 0;
	protected static final int FEATURES_START_AT = 10;
	protected static final int UNKNOWN_VALUE = 0;
	private String bindir;
	private String tmpdir;
	private String model;
	private String training_parameters;

	public svmLightWrapper(String bindir, String tmpdir, String model) {
		this.bindir = bindir;
		this.tmpdir = tmpdir;
		this.model = model;
		this.training_parameters = "-c 10";
	}
	
	public void train(List<Map<Integer, Integer>> features) {
		if (features == null) {
			return;
		}

		if (!writeExampleFile(features)) {
			return;
		}
		
		try {
			if (Config.debug > 0) {
				System.err.print("Running svmLight for classify...");
			}
			String examples = tmpdir + "examples";
			String cmd = bindir + "svm_learn -t 0 -m 200 -v 0 " +  training_parameters + " " + examples + " " + model;
			Process p = Runtime.getRuntime().exec(cmd);

			if (Config.debug > 1) {
				String s;
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(
						p.getInputStream()));
				BufferedReader stdError = new BufferedReader(new InputStreamReader(
						p.getErrorStream()));
				// read the output from the command
				while ((s = stdInput.readLine()) != null) {
					System.err.println(s);
				}
				// read any errors from the attempted command
				while ((s = stdError.readLine()) != null) {
					System.err.println(s);
				}
			}

			p.waitFor();
			if (Config.debug > 0) {
				System.err.println("done");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error: Couldn't run svmLight!");
		}
		return;
	}

	public List<Boolean> classify(List<Map<Integer, Integer>> features) {
		List<Boolean> res;

		if (features == null) {
			return null;
		}

		if (!writeExampleFile(features)) {
			return null;
		}

		try {
			if (Config.debug > 0) {
				System.err.print("Running svmLight for classify...");
			}
			String examples = tmpdir + "examples";
			String output = tmpdir + "out";
			String cmd = bindir + "svm_classify " + examples + " " + model + " " + output;
			Process p = Runtime.getRuntime().exec(cmd);

			if (Config.debug > 1) {
				String s;
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(
						p.getInputStream()));
				BufferedReader stdError = new BufferedReader(new InputStreamReader(
						p.getErrorStream()));
				// read the output from the command
				while ((s = stdInput.readLine()) != null) {
					System.err.println(s);
				}
				// read any errors from the attempted command
				while ((s = stdError.readLine()) != null) {
					System.err.println(s);
				}
			}

			p.waitFor();
			if (Config.debug > 0) {
				System.err.println("done");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error: Couldn't run svmLight!");
		}

		res = readOutputFile();
		return res;

	}

	public void cleanup() {
		try {
			Files.delete(Paths.get(tmpdir + "examples"));
			Files.delete(Paths.get(tmpdir + "out"));
		} catch (Exception e) {
			if (Config.debug > 0) {
				e.printStackTrace();
				System.out.println("Warning: Couldn't delete temporary files");
			}
		}
	}

	private boolean writeExampleFile(List<Map<Integer, Integer>> features) {
		String outfile = tmpdir + "examples";
		try {
			/* Open file */
			BufferedWriter out = new BufferedWriter(new FileWriter(outfile));

			/* Output feature info */
			for (Map<Integer, Integer> line : features) {
				/* Handle truth */
				if (line.containsKey(TRUTH_ID)) {
					out.write(line.get(TRUTH_ID) + " ");
				} else {
					out.write("0 ");
				}

				/* Output all other features */
				for (Integer k : asSortedList(line.keySet())) {
					if (k < FEATURES_START_AT) {
						continue;
					}
					if (line.get(k) == UNKNOWN_VALUE) {
						continue;
					}
					out.write(k.toString() + ":" + line.get(k).toString() + " ");
				}
				out.write("\n");
			}

			/* Close file */
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error: Can't write temp file for svmLight!");
			return false;
		}
		return true;
	}

	private List<Boolean> readOutputFile() {
		String infile = tmpdir + "out";
		List<Boolean> out = new LinkedList<>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(infile));
			String tmp;

			while ((tmp = br.readLine()) != null) {
				Double f = 0.0;
				try {
					f = Double.valueOf(tmp);
				} catch (Exception e) {
					f = 0.0;
				}

				if (f > 0) {
					out.add(true);
				} else {
					out.add(false);
				}
			}
			br.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error: Can't read temp file from svmLight!");
			return null;
		}
		return out;
	}

	private static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
		List<T> list = new ArrayList<T>(c);
		java.util.Collections.sort(list);
		return list;
	}
}
