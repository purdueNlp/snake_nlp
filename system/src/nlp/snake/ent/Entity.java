package nlp.snake.ent;

import org.json.simple.JSONObject;

public class Entity {
	private String name = "";
	private String location = "";
	private String type = "";
	private int id = -1;
	private int realid = -1;
	private String value = "";
	
	public Entity() {
		this.type = "null";
	}

	public Entity(int id, String name, String location, String type, String value, int realid) {
		this.name = new String(name);
		this.location = new String(location);
		this.type = new String(type);
		this.value = new String(value);
		this.id = id;
		this.realid = realid;
	}

	public Entity(int id, String name, String location, String type, String value) {
		this.name = new String(name);
		this.location = new String(location);
		this.type = new String(type);
		this.value = new String(value);
		this.id = id;
		this.realid = -1;
	}

	public Entity(int id, String name, String location, String type, int realid) {
		this.name = new String(name);
		this.location = new String(location);
		this.type = new String(type);
		this.id = id;
		this.realid = realid;
	}

	public Entity(int id, String name, String location, String type) {
		this.name = new String(name);
		this.location = new String(location);
		this.type = new String(type);
		this.id = id;
		this.realid = -1;
	}

	public Entity(int id) {
		this.name = "";
		this.location = "";
		this.type = "";
		this.id = id;
		this.realid = -1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean validId() {
		return id != -1;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getRealId() {
		return realid;
	}

	public void setRealId(int realid) {
		this.realid = realid;
	}

	public boolean equals(Object o) {
		if (!(o instanceof Entity)) {
			return false;
		}
		Entity other = (Entity) o;

		if (!this.type.equals(other.type)) {
			return false;
		}

		if (this.id == other.id) {
			return true;
		}

		if (this.realid != -1 && other.realid != -1 && this.realid == other.realid) {
			return true;
		}

		if ((this.realid == -1 || other.realid == -1) && this.name.equals(other.name)) {
			return true;
		}

		return false;
	}

	public String toString() {
		return "<" + id + "," + name + "," + type + "," + realid + "," + value + ">";
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject exportJSON() {
		JSONObject obj = new JSONObject();
		obj.put("name", this.name);
		obj.put("location", this.location);
		obj.put("type", this.type);
		obj.put("id", this.id);
		obj.put("realid", this.realid);
		obj.put("value", this.value);
		return obj;
	}
	
	public boolean importJSON(JSONObject obj) {
		if (!obj.containsKey("name")) {
			return false;
		}
		this.name = (String) obj.get("name");
		
		if (!obj.containsKey("location")) {
			return false;
		}
		this.location = (String) obj.get("location");
		
		if (!obj.containsKey("type")) {
			return false;
		}
		this.type = (String) obj.get("type");
		
		if (!obj.containsKey("id")) {
			return false;
		}
		this.id = ((Long)obj.get("id")).intValue();
		
		if (!obj.containsKey("realid")) {
			return false;
		}
		this.realid = ((Long)obj.get("realid")).intValue();
		
		if (!obj.containsKey("value")) {
			return false;
		}
		this.value = (String) obj.get("value");
		
		return true;
	}
}
