package nlp.snake.ent;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ASCIITable extends Element {
	private String rawText;
	private List<Annotation> annotations = new ArrayList<>();
	private List<List<String>> table = new ArrayList<>();
	private List<String> othertext = new ArrayList<>();
	
	public ASCIITable() {
	}

	public ASCIITable(String rawText) {
		this.rawText = extractAnnotation(rawText,annotations);
		parseTable();
	}
	
	public List<Annotation> getAnnotations() {
		return annotations;
	}
	
	public List<Annotation> getAnnotations(String type) {
		return filterAnnotations(-1, type, annotations);
	}
	
	public List<List<String>> getTable() {
		return table;
	}
	
	public List<String> getOtherText() {
		return othertext;
	}

	public String toString() {
		return rawText;
	}

	public void print(PrintStream out) {
		print(out,0);
	}
	
	public void print(PrintStream out, int indent) {
		String ind = "";
		for (int i = 0; i < indent; i++) {
			ind += "   ";
		}
		for (List<String> r: table) {
			out.print(ind);
			for (String c: r) {
				out.print(c + ", \t");
			}
			out.println();
		}
		for (String s: othertext) {
			out.println(ind+s);
		}
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject exportJSON() {
		JSONObject obj = new JSONObject();
		obj.put("text", this.rawText);
		
		JSONArray JbodyAnnotations = new JSONArray();
		for (Annotation a: this.annotations) {
			JbodyAnnotations.add(a.exportJSON());
		}
		obj.put("annotations", JbodyAnnotations);
		
		JSONArray JSONtable1 = new JSONArray();
		for (List<String> r: this.table) {
			JSONArray JSONtable2 = new JSONArray();
			for (String s: r) {
				JSONtable2.add(s);
			}
			JSONtable1.add(JSONtable2);
		}
		obj.put("table", JSONtable1);
		
		JSONArray JSONother = new JSONArray();
		for (String s: this.othertext) {
			JSONother.add(s);
		}
		obj.put("othertext", JSONother);
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	public boolean importJSON(JSONObject obj) {
		if (!obj.containsKey("text")) {
			return false;
		}
		this.rawText = (String) obj.get("text");
		
		if (!obj.containsKey("annotations")) {
			return false;
		}
		JSONArray array = (JSONArray) obj.get("annotations");
		Iterator<JSONObject> it1 = array.iterator();
		while (it1.hasNext()) {
			Annotation a = new Annotation();
			if (!a.importJSON(it1.next())) {
				return false;
			}
			this.annotations.add(a);
		}
		
		if (!obj.containsKey("table")) {
			return false;
		}
		table.clear();
		JSONArray table1 = (JSONArray) obj.get("table");
		Iterator<JSONArray> it2 = table1.iterator();
		while (it2.hasNext()) {
			JSONArray table2 =  it2.next();
			Iterator<String> it3 = table2.iterator();
			List<String> tbl = new ArrayList<>();
			while (it3.hasNext()) {
				tbl.add(it3.next());
			}
			table.add(tbl);
		}
		
		if (!obj.containsKey("othertext")) {
			return false;
		}
		othertext.clear();
		JSONArray Jot = (JSONArray) obj.get("othertext");
		Iterator<String> it4 = Jot.iterator();
		while (it4.hasNext()) {
			othertext.add(it4.next());
		}
		return true;
	}
	
	private void parseTable() {
		int blank = 0;
		int pipe = 0;
		int tab = 0;
		for(String line: rawText.split("\n")) {
			if (line.trim().contains("  ")) {
				blank++;
			}
			if (line.contains("|")) {
				pipe++;
			}
			if (line.trim().contains("\t")) {
				tab++;
			}
		}
		
		for(String line: rawText.split("\n")) {
			List<String> parts = new ArrayList<>();
			if (blank > 0 && blank > pipe && blank > tab) {
				for (String p: line.split("   ")) {
					if (!p.trim().isEmpty()) {
						parts.add(p.trim());
					}
				}
			}
			if (pipe > 0 && pipe > blank && pipe > tab) {
				for (String p: line.split("|")) {
					if (!p.trim().isEmpty()) {
						parts.add(p.trim());
					}
				}
			}
			if (tab > 0 && tab > pipe && tab > blank) {
				for (String p: line.split("\t")) {
					if (!p.trim().isEmpty()) {
						parts.add(p.trim());
					}
				}
			}
			
			if (parts.size() > 1) {
				table.add(parts);
			} else if (line.trim().length() > 0) {
				othertext.add(line.trim());
			}
		}
	}
}
