package nlp.snake.ent;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import nlp.snake.config.Config;

public class InputProtocol {

	/**
	 * A document is a list of sections. Each section has a title, content =
	 * {text,ascii images,tables}, and nested sections.
	 */

	private String title = "";
	private List<ProtocolSection> sections = new ArrayList<>();
	private List<Entity> entities = new ArrayList<>();
	private List<Entity> goldEntities = new ArrayList<>();
	
	public InputProtocol() {
		
	}

	public InputProtocol(String title, List<ProtocolSection> sections) {
		this.sections = sections;
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public List<ProtocolSection> getSections() {
		return sections;
	}

	public void addEntities(List<Entity> entities, boolean gold) {
		if (!gold) {
			this.entities = new ArrayList<>(entities);
		} else {
			this.goldEntities = new ArrayList<>(entities);
		}
	}

	public List<Entity> getEntities() {
		if (!Config.eval_gold_entities) {
			return entities;
		} else {
			return goldEntities;
		}
		
	}
	
	public List<Entity> getDiscoveredEntities() {
		return entities;
	}
	
	public List<Entity> getGoldEntities() {
		return goldEntities;
	}

	public void print(PrintStream out) {
		print(out, false, 0);
	}

	public void print(PrintStream out, boolean annotation) {
		print(out, annotation, 0);
	}

	public void print(PrintStream out, boolean annotation, int indent) {
		out.println("Document Title: " + title);
		for (ProtocolSection s : sections) {
			out.println("#######################");
			s.print(out, annotation, indent);
			out.println("\n#######################");
		}
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject exportJSON() {
		JSONObject obj = new JSONObject();
		
		obj.put("title", this.title);
		
		JSONArray Jsections = new JSONArray();
		for (ProtocolSection p: this.sections) {
			Jsections.add(p.exportJSON());
		}
		obj.put("sections", Jsections);
		
		JSONArray Jentities = new JSONArray();
		for (Entity e: this.entities) {
			Jentities.add(e.exportJSON());
		}
		obj.put("entities", Jentities);
		
		JSONArray Jgentities = new JSONArray();
		for (Entity e: this.goldEntities) {
			Jgentities.add(e.exportJSON());
		}
		obj.put("goldentities", Jgentities);
		
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	public boolean importJSON(JSONObject obj) {
		if (!obj.containsKey("title")) {
			return false;
		}
		this.title = (String)obj.get("title");
		
		if (!obj.containsKey("sections")) {
			return false;
		}
		JSONArray array = (JSONArray) obj.get("sections");
		Iterator<JSONObject> it1 = array.iterator();
		while (it1.hasNext()) {
			ProtocolSection p = new ProtocolSection();
			if (!p.importJSON(it1.next())) {
				return false;
			}
			this.sections.add(p);
		}
		
		if (!obj.containsKey("entities")) {
			return false;
		}
		array = (JSONArray) obj.get("entities");
		it1 = array.iterator();
		while (it1.hasNext()) {
			Entity e = new Entity();
			if (!e.importJSON(it1.next())) {
				return false;
			}
			this.entities.add(e);
		}
		
		if (!obj.containsKey("goldentities")) {
			return false;
		}
		array = (JSONArray) obj.get("goldentities");
		it1 = array.iterator();
		while (it1.hasNext()) {
			Entity e = new Entity();
			if (!e.importJSON(it1.next())) {
				return false;
			}
			this.goldEntities.add(e);
		}
		
		return true;
	}
}
