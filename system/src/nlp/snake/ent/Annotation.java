package nlp.snake.ent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Annotation {
	private int beginCharIndex = -1;
	private int endCharIndex = -1;
	private int beginSentIndex = -1;
	private int endSentIndex = -1;
	private int beginWordIndex = -1;
	private int endWordIndex = -1;
	private String expression = "";
	private String id = ""; // usable internally and raw string of id field
	private String type = "";
	private List<Integer> refs = new ArrayList<>(); // id field
	private List<List<Integer>> refGroups = new ArrayList<>(); // Groups for
																// ida, idb,
																// idc, etc

	public Annotation() {

	}

	public Annotation(Annotation c) {
		this.beginCharIndex = c.beginCharIndex;
		this.endCharIndex = c.endCharIndex;
		this.beginSentIndex = c.beginSentIndex;
		this.beginWordIndex = c.beginWordIndex;
		this.endSentIndex = c.endSentIndex;
		this.endWordIndex = c.endWordIndex;
		this.expression = new String(c.expression);
		this.id = new String(c.id);
		this.type = new String(c.type);
	}

	public int getBeginCharIndex() {
		return beginCharIndex;
	}

	public void setBeginCharIndex(int beginIndex) {
		this.beginCharIndex = beginIndex;
	}

	public int getEndCharIndex() {
		return endCharIndex;
	}

	public void setEndCharIndex(int endIndex) {
		this.endCharIndex = endIndex;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getBeginSentIndex() {
		return beginSentIndex;
	}

	public void setBeginSentIndex(int beginSentIndex) {
		this.beginSentIndex = beginSentIndex;
	}

	public int getEndSentIndex() {
		return endSentIndex;
	}

	public void setEndSentIndex(int endSentIndex) {
		this.endSentIndex = endSentIndex;
	}

	public int getBeginWordIndex() {
		return beginWordIndex;
	}

	public void setBeginWordIndex(int beginWordIndex) {
		this.beginWordIndex = beginWordIndex;
	}

	public int getEndWordIndex() {
		return endWordIndex;
	}

	public void setEndWordIndex(int endWordIndex) {
		this.endWordIndex = endWordIndex;
	}

	public void addRef(int r) {
		this.refs.add(r);
	}

	public List<Integer> getRefs() {
		return this.refs;
	}

	public void clearRefs() {
		this.refs.clear();
	}

	public void addRefGroup(List<Integer> g) {
		this.refGroups.add(new ArrayList<>(g));
	}

	public List<List<Integer>> getRefGroups() {
		return this.refGroups;
	}

	public void clearRefGroups() {
		this.refGroups.clear();
	}

	public String toString() {
		String b = "(" + beginCharIndex + "," + beginSentIndex + "," + beginWordIndex + ") ";
		String e = "(" + endCharIndex + "," + endSentIndex + "," + endWordIndex + ") ";
		String details = "(" + type + "," + id + ")";
		String parsed = "(" + refs.toString() + refGroups.toString() + ")";
		return "[" + b + details + parsed + expression + e + "]";
	}

	public boolean overlapping(Annotation o) {
		if (this.beginCharIndex >= 0 && this.endCharIndex >= 0 && o.beginCharIndex >= 0
				&& o.endCharIndex >= 0) {
			/* O surrounds this */
			if (this.beginCharIndex >= o.beginCharIndex && this.endCharIndex <= o.endCharIndex) {
				return true;
			}

			/* this surrounds O */
			if (this.beginCharIndex <= o.beginCharIndex && this.endCharIndex >= o.endCharIndex) {
				return true;
			}

			/* O overlaps on left with this */
			if (this.beginCharIndex >= o.beginCharIndex && this.beginCharIndex <= o.endCharIndex) {
				return true;
			}

			/* O overlaps on right with this */
			if (this.endCharIndex <= o.endCharIndex && this.endCharIndex >= o.beginCharIndex) {
				return true;
			}
		} else if (this.beginSentIndex >= 0 && this.endSentIndex >= 0 && this.beginWordIndex >= 0
				&& this.endWordIndex >= 0 && o.beginSentIndex >= 0 && o.endSentIndex >= 0
				&& o.beginWordIndex >= 0 && o.endWordIndex >= 0) {
			/* O surrounds this */
			if (this.beginSentIndex >= o.beginSentIndex && this.beginWordIndex >= o.beginWordIndex
					&& this.endSentIndex <= o.endSentIndex && this.endWordIndex <= o.endWordIndex) {
				return true;
			}

			/* this surrounds O */
			if (this.beginSentIndex <= o.beginSentIndex && this.beginWordIndex <= o.beginWordIndex
					&& this.endSentIndex >= o.endSentIndex && this.endWordIndex >= o.endWordIndex) {
				return true;
			}

			/* O overlaps on left with this */
			if (this.beginSentIndex >= o.beginSentIndex && this.beginWordIndex >= o.beginWordIndex
					&& this.beginSentIndex <= o.endSentIndex
					&& this.beginWordIndex <= o.endWordIndex) {
				return true;
			}

			/* O overlaps on right with this */
			if (this.endSentIndex <= o.endSentIndex && this.endWordIndex <= o.endWordIndex
					&& this.endSentIndex >= o.beginSentIndex
					&& this.endWordIndex >= o.beginWordIndex) {
				return true;
			}
		}
		return false;
	}

	public boolean contains(Annotation o) {
		/* this surrounds O */
		if (this.beginCharIndex >= 0 && this.endCharIndex >= 0 && o.beginCharIndex >= 0
				&& o.endCharIndex >= 0) {
			if (this.beginCharIndex <= o.beginCharIndex && this.endCharIndex >= o.endCharIndex) {
				return true;
			}
		} else if (this.beginSentIndex >= 0 && this.endSentIndex >= 0 && this.beginWordIndex >= 0
				&& this.endWordIndex >= 0 && o.beginSentIndex >= 0 && o.endSentIndex >= 0
				&& o.beginWordIndex >= 0 && o.endWordIndex >= 0) {
			if (this.beginSentIndex <= o.beginSentIndex && this.beginWordIndex <= o.beginWordIndex
					&& this.endSentIndex >= o.endSentIndex && this.endWordIndex >= o.endWordIndex) {
				return true;
			}

		}

		return false;
	}

	public boolean equals(Object o) {
		if (!(o instanceof Annotation)) {
			return false;
		}
		Annotation other = (Annotation) o;

		if (this.beginCharIndex >= 0 && this.endCharIndex >= 0 && other.beginCharIndex >= 0
				&& this.endCharIndex >= 0 && this.beginCharIndex == other.beginCharIndex
				&& this.endCharIndex == other.endCharIndex && this.type.equals(other.type)
				&& this.id.equals(other.id) && this.expression.equals(other.expression)) {
			return true;
		}

		if (this.beginSentIndex >= 0 && this.endSentIndex >= 0 && this.beginWordIndex >= 0
				&& this.endWordIndex >= 0 && other.beginSentIndex >= 0 && other.endSentIndex >= 0
				&& other.beginWordIndex >= 0 && other.endWordIndex >= 0
				&& this.beginSentIndex == other.beginSentIndex
				&& this.endSentIndex == other.endSentIndex
				&& this.beginWordIndex == other.beginWordIndex
				&& this.endWordIndex == other.endWordIndex && this.type.equals(other.type)
				&& this.id.equals(other.id) && this.expression.equals(other.expression)) {
			return true;
		}

		return false;

	}
	
	@SuppressWarnings("unchecked")
	public JSONObject exportJSON() {
		JSONObject obj = new JSONObject();
		obj.put("expression", this.expression);
		obj.put("type", this.type);
		obj.put("id", this.id);
		JSONArray Jrefs = new JSONArray();
		for (Integer i: refs) {
			Jrefs.add(i);
		}
		obj.put("refs", Jrefs);
		JSONArray JrefsG = new JSONArray();
		for (List<Integer> l : this.refGroups) {
			JSONArray j = new JSONArray();
			for (Integer i: l) {
				j.add(i);
			}
			JrefsG.add(j);
		}
		obj.put("refGroups", JrefsG);
		obj.put("bg-s", this.beginSentIndex);
		obj.put("bg-w", this.beginWordIndex);
		obj.put("ed-s", this.endSentIndex);
		obj.put("ed-w", this.endWordIndex);
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	public boolean importJSON(JSONObject obj) {
		if (!obj.containsKey("expression")) {
			return false;
		}
		this.expression = (String) obj.get("expression");
		
		if (!obj.containsKey("type")) {
			return false;
		}
		this.type = (String) obj.get("type");
		
		if (!obj.containsKey("id")) {
			return false;
		}
		this.id = (String) obj.get("id");
		
		if (!obj.containsKey("refs")) {
			return false;
		}
		JSONArray arr = (JSONArray) obj.get("refs");
		Iterator<Long> it = arr.iterator();
		while (it.hasNext()) {
			this.refs.add(((Long)it.next()).intValue());
		}
		
		if (!obj.containsKey("refGroups")) {
			return false;
		}
		JSONArray array = (JSONArray) obj.get("refGroups");
		Iterator<JSONArray> it1 = array.iterator();
		while (it1.hasNext()) {
			List<Integer> tmp  = new ArrayList<>();
			Iterator<Long> it2 = it1.next().iterator();
			while(it2.hasNext()) {
				tmp.add(((Long)it2.next()).intValue());
			}
			this.refGroups.add(tmp);
		}
		
		if (!obj.containsKey("bg-s")) {
			return false;
		}
		this.beginSentIndex = ((Long)obj.get("bg-s")).intValue();
		
		if (!obj.containsKey("bg-w")) {
			return false;
		}
		this.beginWordIndex = ((Long)obj.get("bg-w")).intValue();
		
		if (!obj.containsKey("ed-s")) {
			return false;
		}
		this.endSentIndex = ((Long)obj.get("ed-s")).intValue();
		
		if (!obj.containsKey("ed-w")) {
			return false;
		}
		this.endWordIndex = ((Long)obj.get("ed-w")).intValue();
		
		return true;
	}
}
