package nlp.snake.ent;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import nlp.snake.featurizers.CoreNLPCoRefWrapper;
import edu.stanford.nlp.hcoref.CorefCoreAnnotations;
import edu.stanford.nlp.hcoref.data.CorefChain;
import edu.stanford.nlp.hcoref.data.CorefChain.CorefMention;
import edu.stanford.nlp.ie.util.RelationTriple;
import edu.stanford.nlp.ling.CoreAnnotations.BeginIndexAnnotation;
import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

public class ProtocolSection extends Element {
	private String origtitle = "";
	private String rawtitle = "";
	private Sentence title = null;
	private String origtext = "";
	private String rawtext = "";
	private Document text = null;
	private List<ASCIIImage> images = new LinkedList<>();
	private List<ASCIITable> tables = new LinkedList<>();
	private List<ProtocolSection> subsections = new LinkedList<>();
	private List<Annotation> textAnnotations = new LinkedList<>();
	private List<Annotation> titleAnnotations = new LinkedList<>();
	private Map<String,String> metadata = new HashMap<>();
	private boolean chunked = false;
	private boolean corefed = false;

	public String getRawTitle() {
		return rawtitle;
	}

	public Sentence getTitle() {
		return title;
	}

	public String getRawText() {
		return rawtext;
	}

	public Document getText() {
		return text;
	}

	public List<ASCIIImage> getImages() {
		return images;
	}

	public List<ASCIITable> getTables() {
		return tables;
	}

	public List<ProtocolSection> getSubsections() {
		return subsections;
	}

	public void setSectionTitle(String title) {
		this.titleAnnotations.clear();
		this.origtitle = title;

		if (title.length() > 0) {
			this.rawtitle = extractAnnotation(title, titleAnnotations);
			this.title = new Sentence(this.rawtitle);
			processNewTitleAnnotations();
		} else {
			this.title = null;
			this.rawtitle = "";
		}
	}

	public void addImage(ASCIIImage image) {
		images.add(image);
	}

	public void addTable(ASCIITable table) {
		tables.add(table);
	}

	public void addSubsection(ProtocolSection sect) {
		subsections.add(sect);
	}

	public void setSectionText(String currentText) {
		this.textAnnotations.clear();
		this.chunked = false;
		this.corefed = false;

		this.origtext = currentText;

		if (currentText.length() > 0) {
			this.rawtext = extractAnnotation(currentText, textAnnotations);
			this.rawtext = handleRFC2119(this.rawtext, textAnnotations);
			this.text = new Document(this.rawtext);
			processNewTextAnnotations();
		} else {
			this.text = null;
			this.rawtext = "";
		}
	}

	public void addTextAnnotation(Annotation a) {
		if (!this.textAnnotations.contains(a)) {
			this.textAnnotations.add(a);
			processNewTextAnnotations();
		}
	}

	public void addTextAnnotations(List<Annotation> textAnnotations) {
		for (Annotation a : textAnnotations) {
			if (!this.textAnnotations.contains(a)) {
				this.textAnnotations.add(a);
				processNewTextAnnotations();
			}
		}
		processNewTextAnnotations();
	}

	public List<Annotation> getTextAnnotations() {
		return textAnnotations;
	}

	public List<Annotation> getTextAnnotations(String type) {
		return filterAnnotations(-1, type, textAnnotations);
	}

	public List<Annotation> getTextAnnotations(int sentence) {
		return filterAnnotations(sentence, null, textAnnotations);
	}

	public List<Annotation> getTextAnnotations(int sentence, String type) {
		return filterAnnotations(sentence, type, textAnnotations);
	}

	public List<Annotation> getTitleAnnotations() {
		return titleAnnotations;
	}

	public List<Annotation> getTitleAnnotations(String type) {
		return filterAnnotations(-1, type, titleAnnotations);
	}

	public void addTitleAnnotation(Annotation a) {
		if (!this.titleAnnotations.contains(a)) {
			this.titleAnnotations.add(a);
			processNewTitleAnnotations();
		}
	}

	public void addTitleAnnotations(List<Annotation> titleAnnotations) {
		for (Annotation a : titleAnnotations) {
			if (!this.titleAnnotations.contains(a)) {
				this.titleAnnotations.add(a);
			}
		}
		processNewTitleAnnotations();
	}

	public void print(PrintStream out) {
		print(out, false, 0);
	}

	public void print(PrintStream out, boolean annotation, int indent) {
		String ind = "";
		for (int i = 0; i < indent; i++) {
			ind += "   ";
		}

		if (annotation) {
			out.print(ind + "Section Title: ");
			printAnnotatedTitle(out);
			out.println("");
			out.println(ind + "Text:");
			out.print(ind);
			printAnnotatedText(out);
		} else {
			out.println(ind + "Section Title: " + rawtitle);
			out.println(ind + "Text:");
			out.print(ind + rawtext);
		}
		if (tables.size() > 0) {
			for (ASCIITable t : tables) {
				out.println(ind + "\n*****");
				out.println(ind + "Table:");
				t.print(out);
			}
		}
		if (images.size() > 0) {

			for (ASCIIImage t : images) {
				out.println(ind + "\n*****");
				out.println(ind + "Image:");
				t.print(out);
			}
		}
		if (subsections.size() > 0) {
			for (ProtocolSection s : subsections) {
				out.println(ind + "\n*****");
				out.println(ind + "Subsection:");
				s.print(out, annotation, indent + 1);
			}
		}
	}

	private void printAnnotatedText(PrintStream out) {
		if (text == null | rawtext.length() == 0) {
			return;
		}

		List<List<String>> wordarray = new ArrayList<>();

		for (Sentence s : text.sentences()) {
			List<String> words = new ArrayList<>();
			for (String w : s.originalTexts()) {
				words.add(w);
			}
			wordarray.add(words);
		}

		for (Annotation a : textAnnotations) {
			if (a.getBeginSentIndex() >= wordarray.size() || a.getBeginSentIndex() < 0) {
				continue;
			}
			if (a.getEndSentIndex() >= wordarray.size() || a.getEndSentIndex() < 0) {
				continue;
			}
			List<String> s = wordarray.get(a.getBeginSentIndex());
			if (a.getBeginWordIndex() >= s.size() || a.getBeginWordIndex() < 0) {
				continue;
			}
			String w = s.get(a.getBeginWordIndex());
			w = "[(" + a.getType() + ")" + w;
			s.set(a.getBeginWordIndex(), w);

			s = wordarray.get(a.getEndSentIndex());
			if (a.getEndWordIndex() >= s.size() || a.getEndWordIndex() < 0) {
				continue;
			}
			w = s.get(a.getEndWordIndex());
			w = w + "]";
			s.set(a.getEndWordIndex(), w);
		}

		String d = "";
		for (List<String> s : wordarray) {
			for (String w : s) {
				d += w + " ";
			}
		}

		out.print(d);
	}

	private void printAnnotatedTitle(PrintStream out) {
		if (title == null | rawtitle.length() == 0) {
			out.print("(No Title)");
			return;
		}

		List<String> text = new ArrayList<>();
		for (String w : title.originalTexts()) {
			text.add(w);
		}

		for (Annotation a : titleAnnotations) {
			if (a.getBeginWordIndex() >= text.size() || a.getBeginWordIndex() < 0) {
				continue;
			}
			String w = text.get(a.getBeginWordIndex());
			w = "[(" + a.getType() + ")" + w;
			text.set(a.getBeginWordIndex(), w);

			if (a.getEndWordIndex() >= text.size() || a.getEndWordIndex() < 0) {
				continue;
			}
			w = text.get(a.getEndWordIndex());
			w = w + "]";
			text.set(a.getEndWordIndex(), w);
		}

		String d = "";
		for (String w : text) {
			d += w + " ";
		}

		out.print(d);
	}

	public List<Sentence> sentences() {
		return text.sentences();
	}

	public Sentence sentence(int i) {
		return text.sentence(i);
	}

	private String LCAndAnnotate(String text, List<Annotation> annts, String target, String type) {
		int loc = 0;
		Annotation a;

		loc = text.indexOf(target);
		while (loc >= 0) {
			a = new Annotation();
			a.setBeginCharIndex(loc);
			a.setEndCharIndex(loc + target.length());
			a.setExpression(target);
			a.setType(type);
			a.setId(target);
			annts.add(a);
			text = text.replaceFirst(target, target.toLowerCase());
			loc = text.indexOf(target);
		}

		return text;
	}

	private String handleRFC2119(String text, List<Annotation> annts) {

		text = LCAndAnnotate(text, annts, "MUST NOT", "int:key");
		text = LCAndAnnotate(text, annts, "MUST", "int:key");
		text = LCAndAnnotate(text, annts, "SHALL NOT", "int:key");
		text = LCAndAnnotate(text, annts, "SHALL", "int:key");
		text = LCAndAnnotate(text, annts, "SHOULD NOT", "int:key");
		text = LCAndAnnotate(text, annts, "SHOULD", "int:key");
		text = LCAndAnnotate(text, annts, "REQUIRED", "int:key");
		text = LCAndAnnotate(text, annts, "RECOMMENDED", "int:key");
		text = LCAndAnnotate(text, annts, "MAY", "int:key");
		text = LCAndAnnotate(text, annts, "OPTIONAL", "int:key");
		text = LCAndAnnotate(text, annts, "NOT", "int:key");
		return text;
	}

	private void processNewTextAnnotations() {
		if (text == null | rawtext.length() == 0 | textAnnotations.size() == 0) {
			return;
		}

		// System.out.println(text);
		for (Annotation a : textAnnotations) {
			/* Annotation without Sentence/Word indicies */
			if (a.getBeginSentIndex() < 0 || a.getBeginWordIndex() < 0) {
				updateSentenceWordIndexes(a, text);
			}
			if (a.getBeginCharIndex() < 0) {
				updateCharIndicies(a, text);
			}
		}
	}

	private void processNewTitleAnnotations() {
		if (title == null | rawtitle.length() == 0 | titleAnnotations.size() == 0) {
			return;
		}

		for (Annotation a : titleAnnotations) {
			/* Annotation without Word indicies */
			if (a.getBeginSentIndex() < 0 || a.getBeginWordIndex() < 0) {
				updateSentenceWordIndexes(a, title.document);
			}
			if (a.getBeginCharIndex() < 0) {
				updateCharIndicies(a, title.document);
			}
		}
	}

	private List<Integer> sentenceStartCharIndexes(Document d) {
		List<Integer> sentenceStartCharIndexes = new ArrayList<>();
		int start = 0;
		int end = 0;
		for (Sentence s : d.sentences()) {
			List<String> words = s.words();
			end = s.characterOffsetEnd(words.size() - 1);
			sentenceStartCharIndexes.add(start);
			start = end + 1;

		}
		return sentenceStartCharIndexes;
	}

	private void updateSentenceWordIndexes(Annotation a, Document d) {
		List<Integer> sentenceStartCharIndexes = sentenceStartCharIndexes(d);
		int startSent = 0;
		int endSent = 0;
		for (int i = 0; i < sentenceStartCharIndexes.size(); i++) {
			if (sentenceStartCharIndexes.get(i) <= a.getBeginCharIndex()) {
				startSent = i;
			}
			if (sentenceStartCharIndexes.get(i) <= a.getEndCharIndex()) {
				endSent = i;
			}
		}

		// System.out.println("as:" + a.getBeginCharIndex());
		// System.out.println("ae:" + a.getEndCharIndex());

		a.setBeginSentIndex(startSent);
		a.setEndSentIndex(endSent);
		// System.out.println("ss:" + startSent);
		// System.out.println("se:" + endSent);

		/* Compute Start word index */
		int sentOff = a.getBeginCharIndex();
		int wordStart = 0;
		List<Integer> begins = d.sentence(startSent).characterOffsetBegin();
		for (int i = 0; i < begins.size(); i++) {
			if (sentOff >= begins.get(i)) {
				wordStart = i;
			}
		}
		a.setBeginWordIndex(wordStart);
		// System.out.println("ws:" + wordStart);

		/* Compute End word index */
		sentOff = a.getEndCharIndex();
		int wordEnd = 0;
		List<Integer> ends = d.sentence(endSent).characterOffsetEnd();
		for (int i = 0; i < ends.size(); i++) {
			if (sentOff >= ends.get(i)) {
				wordEnd = i;
			}
		}
		a.setEndWordIndex(wordEnd);
		// System.out.println("we:" + wordEnd);
	}

	private void updateCharIndicies(Annotation a, Document d) {
		int sentence = a.getBeginSentIndex();
		int word = a.getBeginWordIndex();
		if (sentence < d.sentences().size() && sentence >= 0
				&& word < d.sentence(sentence).words().size() && word >= 0) {
			a.setBeginCharIndex(d.sentence(sentence).characterOffsetBegin(word));
		}

		sentence = a.getEndSentIndex();
		word = a.getEndWordIndex();
		if (sentence < d.sentences().size() && sentence >= 0
				&& word < d.sentence(sentence).words().size() && word >= 0) {
			a.setEndCharIndex(d.sentence(sentence).characterOffsetEnd(word));
		}
	}

	public void NER() {
		System.out.println("#NER:");
		System.out.println(title.text().replace("\n", " "));
		System.out.println("Entities:");
		for (String t : title.mentions()) {
			System.out.println(t);
		}
		System.out.println("****");

		for (Sentence s : text.sentences()) {
			System.out.println(s.text().replace("\n", " "));
			System.out.println("Entities:");
			for (String t : s.mentions()) {
				System.out.println(t);
			}
			System.out.println("****");
		}
	}

	public void OIE() {
		System.out.println("#OIE:");
		System.out.println(title.text().replace("\n", " "));
		System.out.println("Relations:");
		for (RelationTriple r : title.openieTriples()) {
			System.out.println(r.toString());
		}
		System.out.println("****");

		for (Sentence s : text.sentences()) {
			System.out.println(s.text().replace("\n", " "));
			for (RelationTriple r : s.openieTriples()) {
				System.out.println(r.toString());
			}
			System.out.println("****");
		}
	}

	public List<Annotation> coreferenceResolution() {
		List<Annotation> coRefs = new LinkedList<>();
		Annotation cur = null;

		if (corefed) {
			return filterAnnotations(-1, "int:CoRef", textAnnotations);
		}

		/* Do correference resolution */
		edu.stanford.nlp.pipeline.Annotation doc = new edu.stanford.nlp.pipeline.Annotation(
				text.text());
		CoreNLPCoRefWrapper.get().pipeline().annotate(doc);
		Map<Integer, edu.stanford.nlp.hcoref.data.CorefChain> refs = doc
				.get(CorefCoreAnnotations.CorefChainAnnotation.class);

		/* Convert to annotations */
		for (CorefChain c : refs.values()) {
			for (CorefMention m : c.getMentionsInTextualOrder()) {
				cur = new Annotation();
				cur.setId(c.getRepresentativeMention().mentionSpan);
				cur.setType("int:CoRef");
				cur.setBeginSentIndex(m.sentNum - 1);
				cur.setBeginWordIndex(m.startIndex - 1);
				cur.setEndSentIndex(m.sentNum - 1);
				cur.setEndWordIndex(m.endIndex - 2);
				cur.setExpression(m.mentionSpan);
				coRefs.add(cur);
			}
		}

		addTextAnnotations(coRefs);
		corefed = true;
		return coRefs;
	}

	private void doChunk() {
		if (text != null) {
			for (Sentence s : text.sentences()) {
				List<Annotation> chunks = chunkSentence(s);
				addTextAnnotations(chunks);
			}
		}
		if (title != null) {
			List<Annotation> chunks = chunkSentence(title);
			addTitleAnnotations(chunks);
		}
		chunked = true;
	}

	private List<Annotation> chunkSentence(Sentence s) {
		List<Annotation> chunks = new LinkedList<>();
		Annotation cur = null;
		String expr = "";

		/* Get chunk labels for each word */
		List<String> labels = chunkWords(s);

		/* Loop over all words */
		for (int i = 0; i < s.words().size(); i++) {
			if (cur != null && !labels.get(i).equals(cur.getId())) {
				cur.setEndSentIndex(s.sentenceIndex());
				cur.setEndWordIndex(i - 1);
				cur.setExpression(expr);
				chunks.add(cur);
				cur = null;
			}
			if (cur == null && !labels.get(i).equals("X")) {
				cur = new Annotation();
				cur.setId(labels.get(i));
				cur.setType("int:chunk");
				cur.setBeginSentIndex(s.sentenceIndex());
				cur.setBeginWordIndex(i);
				expr = s.originalText(i);
				continue;
			}

			if (cur != null) {
				expr = expr + " " + s.originalText(i);
			}
		}

		if (cur != null) {
			cur.setEndSentIndex(s.sentenceIndex());
			cur.setEndWordIndex(s.words().size() - 1);
			cur.setExpression(expr);
			chunks.add(cur);
			cur = null;
		}

		return chunks;
	}

	private List<String> chunkWords(Sentence s) {
		List<String> tags = new ArrayList<String>();
		for (int i = 0; i < s.words().size(); i++) {
			tags.add("X");
		}

		Tree parse = s.parse();
		parse.indexSpans();
		for (Tree l : parse.getLeaves()) {
			Tree p2 = l.ancestor(2, parse);
			if (p2 != null) {
				if ("S".equals(p2.label().value())) {
					continue;
				}

				CoreMap c = (CoreMap) l.label();
				int begin = c.get(BeginIndexAnnotation.class);
				if (l.value().equals(",")) {
					tags.set(begin, "X");
				} else {
					tags.set(begin, p2.label().value());
				}
			}
		}

		return tags;
	}

	public List<Annotation> getChunks(int sentence) {
		if (!chunked) {
			doChunk();
		}
		return filterAnnotations(sentence, "int:chunk", textAnnotations);
	}

	public List<Annotation> getChunks() {
		return getChunks(-1);
	}
	
	public List<Annotation> getTitleChunks() {
		if (!chunked) {
			doChunk();
		}
		return filterAnnotations(-1,"int:chunk",titleAnnotations);
	}

	@SuppressWarnings("unchecked")
	public JSONObject exportJSON() {
		JSONObject obj = new JSONObject();

		JSONArray Jimages = new JSONArray();
		for (ASCIIImage i: this.images) {
			Jimages.add(i.exportJSON());
		}
		obj.put("images", Jimages);

		JSONArray Jtables = new JSONArray();
		for (ASCIITable t: this.tables) {
			Jtables.add(t.exportJSON());
		}
		obj.put("tables", Jtables);

		if (this.title == null) {
			obj.put("rawtitle", "");
		} else {
			obj.put("rawtitle", this.rawtitle);
		}
		JSONArray Jtitle = new JSONArray();
		for (Annotation a: this.getTitleChunks()) {
			JSONObject c = new JSONObject();
			JSONArray w = new JSONArray();
			JSONArray p = new JSONArray();
			for (int i=a.getBeginWordIndex(); i <= a.getEndWordIndex();i++) {
				w.add(this.title.word(i));
				p.add(this.title.posTag(i));
			}
			c.put("words", w);
			c.put("pos", p);
			c.put("type",a.getId());
			Jtitle.add(c);
		}
		obj.put("title", Jtitle);

		if (this.text == null) {
			obj.put("rawbody", "");
		} else {
			obj.put("rawbody", this.rawtext);
		}
		JSONArray Jbody = new JSONArray();
		if (this.text != null) {
			for (int s = 0; s < this.text.sentences().size();s++) {
				JSONArray st = new JSONArray();
				for (Annotation a: this.getChunks(s)) {
					JSONObject c = new JSONObject();
					JSONArray w = new JSONArray();
					JSONArray p = new JSONArray();
					for (int i=a.getBeginWordIndex(); i <= a.getEndWordIndex();i++) {
						w.add(this.sentence(s).word(i));
						p.add(this.sentence(s).posTag(i));
					}
					c.put("words", w);
					c.put("pos", p);
					c.put("type",a.getId());
					st.add(c);
				}
				Jbody.add(st);
			}
		}
		obj.put("body", Jbody);

		JSONArray JtitleAnnotations = new JSONArray();
		for (Annotation a: this.titleAnnotations) {
			JtitleAnnotations.add(a.exportJSON());
		}
		obj.put("titleannotations", JtitleAnnotations);

		JSONArray JbodyAnnotations = new JSONArray();
		for (Annotation a: this.textAnnotations) {
			JbodyAnnotations.add(a.exportJSON());
		}
		obj.put("bodyannotations", JbodyAnnotations);

		JSONArray Jsubsections = new JSONArray();
		for (ProtocolSection p: this.subsections) {
			Jsubsections.add(p.exportJSON());
		}
		obj.put("subsections", Jsubsections);
	
		return obj;
	}

	@SuppressWarnings("unchecked")
	public boolean importJSON(JSONObject obj) {
		if (!obj.containsKey("images")) {
			return false;
		}
		JSONArray array = (JSONArray) obj.get("images");
		Iterator<JSONObject> it1 = array.iterator();
		while (it1.hasNext()) {
			ASCIIImage tmp1 = new ASCIIImage();
			if (!tmp1.importJSON(it1.next())) {
				return false;
			}
			this.images.add(tmp1);
		}

		if (!obj.containsKey("tables")) {
			return false;
		}
		array = (JSONArray) obj.get("tables");
		it1 = array.iterator();
		while (it1.hasNext()) {
			ASCIITable tmp1 = new ASCIITable();
			if (!tmp1.importJSON(it1.next())) {
				return false;
			}
			this.tables.add(tmp1);
		}

		if (!obj.containsKey("rawtitle")) {
			return false;
		}
		this.rawtitle = (String) obj.get("rawtitle");
		this.origtitle = this.rawtitle;
		if (this.rawtitle.length() > 0) {
			this.title = new Sentence(this.rawtitle);
		} else {
			this.title = null;
		}

		if (!obj.containsKey("rawbody")) {
			return false;
		}
		this.rawtext = (String)obj.get("rawbody");
		this.origtext = this.rawtext;
		if (this.rawtext.length() > 0) {
			this.text = new Document(this.rawtext);
		} else {
			this.text = null;
		}

		if (!obj.containsKey("titleannotations")) {
			return false;
		}
		array = (JSONArray) obj.get("titleannotations");
		it1 = array.iterator();
		while (it1.hasNext()) {
			Annotation a = new Annotation();
			if (!a.importJSON(it1.next())) {
				return false;
			}
			this.titleAnnotations.add(a);
		}
		processNewTitleAnnotations();


		if (!obj.containsKey("bodyannotations")) {
			return false;
		}
		array = (JSONArray) obj.get("bodyannotations");
		it1 = array.iterator();
		while (it1.hasNext()) {
			Annotation a = new Annotation();
			if (!a.importJSON(it1.next())) {
				return false;
			}
			if (a.getType().equals("int:chunk")) {
				this.chunked = true;
			}
			if (a.getType().equals("int:CoRef")) {
				this.corefed = true;
			}	
			this.textAnnotations.add(a);
		}
		processNewTextAnnotations();

		if (!obj.containsKey("subsections")) {
			return false;
		}
		array = (JSONArray) obj.get("subsections");
		it1 = array.iterator();
		while (it1.hasNext()) {
			ProtocolSection p = new ProtocolSection();
			if (!p.importJSON(it1.next())) {
				return false;
			}
			this.subsections.add(p);
		}
		return true;
	}
	
	public void setMeta(String key, String value) {
		metadata.put(key, value);
	}
	
	public String getMeta(String key) {
		return metadata.get(key);
	}
}
