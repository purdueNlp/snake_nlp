package nlp.snake.ent;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ASCIIImage extends Element{

	private String rawText;
	private List<Annotation> annotations = new ArrayList<>();
	
	public ASCIIImage() {
	}

	public ASCIIImage(String rawText) {
		this.rawText = extractAnnotation(rawText,annotations);
	}
	
	public List<Annotation> getAnnotations() {
		return annotations;
	}
	
	public List<Annotation> getAnnotations(String type) {
		return filterAnnotations(-1, type, annotations);
	}

	public String toString() {
		return rawText;
	}

	public void print(PrintStream out) {
		out.print(rawText);
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject exportJSON() {
		JSONObject obj = new JSONObject();
		obj.put("text", this.rawText);
		
		JSONArray JbodyAnnotations = new JSONArray();
		for (Annotation a: this.annotations) {
			JbodyAnnotations.add(a.exportJSON());
		}
		obj.put("annotations", JbodyAnnotations);
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	public boolean importJSON(JSONObject obj) {
		if (!obj.containsKey("text")) {
			return false;
		}
		this.rawText = (String) obj.get("text");
		
		if (!obj.containsKey("annotations")) {
			return false;
		}
		JSONArray array = (JSONArray) obj.get("annotations");
		Iterator<JSONObject> it1 = array.iterator();
		while (it1.hasNext()) {
			Annotation a = new Annotation();
			if (!a.importJSON(it1.next())) {
				return false;
			}
			this.annotations.add(a);
		}
		return true;
	}
}
