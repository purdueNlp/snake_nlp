package nlp.snake.ent;

import java.util.ArrayList;
import java.util.List;

public abstract class Element {
	protected String extractAnnotation(String str, List<Annotation> annt) {
		while (str.indexOf("value=\"start\"/>") != -1) {
			Annotation annotation = new Annotation();
			int openIndexS = str.indexOf("<tag");
			int openIndexE = str.indexOf("/>", openIndexS) + 2;
			if (openIndexS < 0 || openIndexE < 0) {
				str = removeAnnotation(str);
				break;
			}

			int startIndex = str.indexOf("value=\"start\"/>", openIndexS);
			int endIndex = str.indexOf("value=\"end\"/>", openIndexS);
			if (endIndex < startIndex) {
				str = str.substring(0, openIndexS) + str.substring(openIndexE);
				continue;
			}

			String openTag = str.substring(openIndexS, openIndexE);
			String typeStr = openTag.substring(openTag.indexOf("name=") + 6,
					openTag.indexOf("\"", openTag.indexOf("name=") + 6));

			int idIndex = openTag.indexOf("id=");
			String idStr = "";
			if (idIndex >= 0) {
				idStr = openTag.substring(openTag.indexOf("id=") + 4,
						openTag.indexOf("\"", openTag.indexOf("id=") + 4));
			}
			idIndex = openTag.indexOf("ida=");
			String idAStr = "";
			if (idIndex >= 0) {
				idAStr = openTag.substring(openTag.indexOf("ida=") + 5,
						openTag.indexOf("\"", openTag.indexOf("ida=") + 5));
			}
			idIndex = openTag.indexOf("idb=");
			String idBStr = "";
			if (idIndex >= 0) {
				idBStr = openTag.substring(openTag.indexOf("idb=") + 5,
						openTag.indexOf("\"", openTag.indexOf("idb=") + 5));
			}
			idIndex = openTag.indexOf("idc=");
			String idCStr = "";
			if (idIndex >= 0) {
				idCStr = openTag.substring(openTag.indexOf("idc=") + 5,
						openTag.indexOf("\"", openTag.indexOf("idc=") + 5));
			}

			int closeIndexS = str.indexOf("<tag name=\"" + typeStr, openIndexE);
			int closeIndexE = str.indexOf("/>", closeIndexS) + 2;
			if (closeIndexS < 0 || closeIndexE < 0) {
				str = removeAnnotation(str);
				break;
			}
			// String closeTag = str.substring(closeIndexS, closeIndexE);

			String expression = str.substring(openIndexE, closeIndexS);
			expression = removeAnnotation(expression);

			String s1 = str.substring(0, openIndexS);
			String s2 = str.substring(openIndexE);
			if (s1.length() > 0 && s2.length() > 0 && s1.codePointAt(s1.length() - 1) != ' '
					&& s2.codePointAt(0) != ' ' && s2.codePointAt(0) != ','
					&& s2.codePointAt(0) != '.' && s1.codePointAt(s1.length() - 1) != '<'
					&& s1.codePointAt(s1.length() - 1) != '>' && s2.codePointAt(0) != '<'
					&& s2.codePointAt(0) != '>') {
				str = str.substring(0, openIndexS) + " " + str.substring(openIndexE);
				openIndexS++;
			} else {
				str = str.substring(0, openIndexS) + str.substring(openIndexE);
			}

			closeIndexS = str.indexOf("<tag name=\"" + typeStr, 0);
			closeIndexE = str.indexOf("/>", closeIndexS) + 2;
			if (closeIndexS < 0 || closeIndexE < 0) {
				str = removeAnnotation(str);
				break;
			}
			int eventualEnd = removeAnnotation(str.substring(0, closeIndexS)).length();

			s1 = str.substring(0, closeIndexS);
			s2 = str.substring(closeIndexE);
			if (s1.length() > 0 && s2.length() > 0 && s1.codePointAt(s1.length() - 1) != ' '
					&& s2.codePointAt(0) != ' ' && s2.codePointAt(0) != ','
					&& s2.codePointAt(0) != '.' && s1.codePointAt(s1.length() - 1) != '<'
					&& s1.codePointAt(s1.length() - 1) != '>' && s2.codePointAt(0) != '<'
					&& s2.codePointAt(0) != '>') {
				str = str.substring(0, closeIndexS) + " " + str.substring(closeIndexE);
			} else {
				str = str.substring(0, closeIndexS) + str.substring(closeIndexE);
			}

			int expressionIndexStart = openIndexS;
			int expressionIndexEnd = eventualEnd;

			annotation.setBeginCharIndex(expressionIndexStart);
			annotation.setEndCharIndex(expressionIndexEnd);
			annotation.setExpression(expression);
			annotation.setType(typeStr);
			annotation.setId(idStr);
			for (String s : idStr.split(",")) {
				try {
					Integer i = Integer.parseInt(s);
					annotation.addRef(i);
				} catch (Exception e) {
					continue;
				}
			}
			ParseIdGroup(idAStr, annotation);
			ParseIdGroup(idBStr, annotation);
			ParseIdGroup(idCStr, annotation);
			annt.add(annotation);
		}
		return str;
	}
	
	private String removeAnnotation(String str) {
		while (str.indexOf("<tag") != -1) {
			int indexS = str.indexOf("<tag");
			int indexE = str.indexOf("/>", indexS) + 2;
			String s1 = str.substring(0, indexS);
			String s2 = str.substring(indexE);
			if (s1.length() > 0 && s2.length() > 0 && s1.codePointAt(s1.length() - 1) != ' '
					&& s2.codePointAt(0) != ' ' && s2.codePointAt(0) != ','
					&& s2.codePointAt(0) != '.') {
				str = str.substring(0, indexS) + " " + str.substring(indexE);
			} else {
				str = str.substring(0, indexS) + str.substring(indexE);
			}
			str = str.trim();
		}
		return str;
	}
	
	private void ParseIdGroup(String id, Annotation a) {
		if (id.length() == 0) {
			return;
		}

		ArrayList<Integer> idGroup = new ArrayList<>();
		for (String s : id.split(",")) {
			try {
				Integer i = Integer.parseInt(s);
				idGroup.add(i);
			} catch (Exception e) {
				continue;
			}
		}
		if (idGroup.size() > 0) {
			a.addRefGroup(idGroup);
		}
		return;
	}
	
	protected List<Annotation> filterAnnotations(int sentence, String type,
			List<Annotation> annotations) {
		List<Annotation> tags = new ArrayList<>();

		for (Annotation a : annotations) {
			if (sentence == -1
					|| (a.getBeginSentIndex() >= sentence && a.getEndSentIndex() <= sentence)) {
				if (type == null || type.equals(a.getType())) {
					tags.add(a);
				}
			}
		}

		return tags;
	}
}
