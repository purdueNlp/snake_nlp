package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class ChunkTextAcronym extends Feature {

	public ChunkTextAcronym(int num) {
		this.num = num;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0) {
			return;
		}

		for (String word : cwords) {
			if (wordInList(word, rfcWords) || wordInList(word, functionWords)
					|| wordInList(word, commonWords)) {
				continue;
			}
			/*
			 * Check for initial capitial and total number of capitals in word
			 */
			int upper = 0;
			for (int c = 0; c < word.length(); c++) {
				if (Character.isUpperCase(word.charAt(c))) {
					upper++;
				}
			}

			/* Acronym */
			if (upper == word.length()) {
				features.put(num, POSITIVE_VALUE);
				break;
			}
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-ChunkTextAcronym]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkTextAcronym)) {
			return false;
		}

		return true;
	}

}
