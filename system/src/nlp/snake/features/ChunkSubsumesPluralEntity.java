package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class ChunkSubsumesPluralEntity extends Feature {
	private boolean caseSensitive = false;

	public ChunkSubsumesPluralEntity(int num, boolean caseSensitive) {
		this.num = num;
		this.caseSensitive = caseSensitive;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		String ent = stripParens(entity.getName());
		Sentence s = new Sentence(ent);
		if (s.words().size() == 1) {
			return;
		}

		String plural = "";
		if (ent.endsWith("es")) {
			return;
		} else if (ent.endsWith("s")) {
			plural = ent + "es";
		} else {
			plural = ent + "s";
		}

		if (compare(plural, cwords)) {
			features.put(num, POSITIVE_VALUE);
		}
		return;
	}

	public boolean compare(String ent, List<String> cwords) {
		if (ent.length() == 0) {
			return false;
		}

		Sentence s = new Sentence(ent);
		int i = 0;
		for (int c = 0; c < cwords.size(); c++) {
			if (i < s.words().size()) {
				String tmp1 = s.word(i);
				String tmp2 = cwords.get(c);
				if (!caseSensitive) {
					tmp1 = tmp1.toLowerCase();
					tmp2 = tmp2.toLowerCase();
				}
				if (tmp1.equals(tmp2)) {
					i++;
					continue;
				}
			}
		}

		if (i < s.words().size()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = "CaseSensitive:";
		}
		return "[Feat-ChunkSubsumesPluralEntity:" + casestr + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkSubsumesPluralEntity)) {
			return false;
		}
		ChunkSubsumesPluralEntity other = (ChunkSubsumesPluralEntity) o;
		return true;
	}
}
