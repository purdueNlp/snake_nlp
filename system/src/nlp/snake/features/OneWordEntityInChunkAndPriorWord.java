package nlp.snake.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class OneWordEntityInChunkAndPriorWord extends Feature {
	private boolean caseSensitive = false;
	private boolean stemming = false;
	private List<String> words = new ArrayList<>();

	public OneWordEntityInChunkAndPriorWord(int num, boolean caseSensitive, boolean stemming,
			List<String> words) {
		this.num = num;
		this.caseSensitive = caseSensitive;
		this.stemming = stemming;
		this.words = new ArrayList<>(words);
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0 || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() == 1) {
			return;
		}

		Sentence s = new Sentence(stripParens(entity.getName()));
		if (s.words().size() != 1) {
			return;
		}

		Sentence ch = new Sentence(chunk.getExpression());

		int state = 0;
		int i = 0;
		for (int c = 0; c < ch.words().size(); c++) {
			String tmp1 = "";
			if (i < s.words().size()) {
				if (stemming) {
					tmp1 = s.lemmas().get(i).trim();
				} else {
					tmp1 = s.words().get(i).trim();
				}
			}

			String tmp2 = "";
			if (stemming) {
				tmp2 = ch.lemmas().get(c).trim();
			} else {
				tmp2 = cwords.get(c).trim();
			}

			if (!caseSensitive) {
				tmp1 = tmp1.toLowerCase();
				tmp2 = tmp2.toLowerCase();
			}

			switch (state) {
			case 0:
				if (tmp1.equals(tmp2)) {
					i++;
					state = 1;

					if (c - 1 < 0) {
						return;
					}
					boolean found = false;
					for (String w : words) {
						String tmp3 = ch.words().get(c - 1).trim();
						if (!caseSensitive) {
							tmp3 = tmp3.toLowerCase();
							w = w.toLowerCase().trim();
						}
						if (tmp3.equals(w)) {
							found = true;
						}
					}
					if (!found) {
						return;
					}
				}
				break;
			case 1:
				if (tmp1.equals(tmp2)) {
					i++;
					break;
				} else {
					state = 2;
				}
			case 2:
				break;
			}
		}

		if (i < s.words().size()) {
			return;
		} else {
			features.put(num, POSITIVE_VALUE);
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		String stemmingstr = "";
		if (caseSensitive) {
			casestr = "CaseSensitive:";
		}
		if (stemming) {
			stemmingstr = "Stemming:";
		}
		return "[Feat-OneWordEntityInChunkAndPriorWord:" + casestr + stemmingstr + words.toString()
				+ "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof OneWordEntityInChunkAndPriorWord)) {
			return false;
		}
		OneWordEntityInChunkAndPriorWord other = (OneWordEntityInChunkAndPriorWord) o;
		if (this.words.equals(other.words) && stemming == other.stemming
				&& caseSensitive == other.caseSensitive) {
			return true;
		}
		return false;
	}
}
