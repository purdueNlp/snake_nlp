package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class ParensHeaderMatchChunk extends Feature {

	public ParensHeaderMatchChunk(int num) {
		this.num = num;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0 || sect.getTitle() == null  || entity.getName().length() == 0) {
			return;
		}

		if (!trimHeader(sect.getTitle().text()).equals(entity.getLocation())) {
			return;
		}

		if (!entity.getName().contains("(")) {
			return;
		}

		String en = entity.getName();
		String paren = en.substring(en.indexOf("(") + 1);
		if (!paren.contains(")")) {
			return;
		}
		paren = paren.substring(0, paren.indexOf(")"));
		if (paren.length() <= 0) {
			return;
		}

		for (int c = 0; c < cwords.size(); c++) {
			if (cwords.get(c).equals(paren)) {
				features.put(num, POSITIVE_VALUE);
				break;
			}
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-ParensHeaderMatchChunk]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ParensHeaderMatchChunk)) {
			return false;
		}
		return true;
	}
}
