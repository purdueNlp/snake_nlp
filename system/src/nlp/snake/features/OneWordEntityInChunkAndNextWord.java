package nlp.snake.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class OneWordEntityInChunkAndNextWord extends Feature {
	private boolean caseSensitive = false;
	private boolean stemming = false;
	private List<String> words = new ArrayList<>();

	public OneWordEntityInChunkAndNextWord(int num, boolean caseSensitive, boolean stemming,
			List<String> words) {
		this.num = num;
		this.caseSensitive = caseSensitive;
		this.stemming = stemming;
		this.words = new ArrayList<>(words);
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() == 1) {
			return;
		}

		Sentence s = new Sentence(stripParens(entity.getName()));
		if (s.words().size() != 1) {
			return;
		}

		Sentence ch = new Sentence(chunk.getExpression());

		int state = 0;
		int i = 0;
		for (int c = 0; c < ch.words().size(); c++) {
			String tmp1 = "";
			if (i < s.words().size()) {
				if (stemming) {
					tmp1 = s.lemmas().get(i).trim();
				} else {
					tmp1 = s.words().get(i).trim();
				}
			}

			String tmp2 = "";
			if (stemming) {
				tmp2 = ch.lemmas().get(c).trim();
			} else {
				tmp2 = ch.words().get(c).trim();
			}

			if (!caseSensitive) {
				tmp1 = tmp1.toLowerCase();
				tmp2 = tmp2.toLowerCase();
			}

			switch (state) {
			case 0:
				if (tmp1.equals(tmp2)) {
					i++;
					state = 1;
				}
				break;
			case 1:
				if (tmp1.equals(tmp2)) {
					i++;
					break;
				} else {
					state = 2;
				}
			case 2:
				tmp2 = ch.words().get(c).trim();
				boolean found = false;
				for (String w : words) {
					if (!caseSensitive) {
						w = w.toLowerCase().trim();
						tmp2 = tmp2.toLowerCase();
					}

					if (tmp2.equals(w)) {
						found = true;
						break;
					}
				}
				if (!found) {
					return;
				}
				state = 3;
				break;
			case 3:
				break;
			}
		}

		if (i < s.words().size() || state != 3) {
			return;
		} else {
			features.put(num, POSITIVE_VALUE);
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		String stemmingstr = "";
		if (caseSensitive) {
			casestr = "CaseSensitive:";
		}
		if (stemming) {
			stemmingstr = "Stemming:";
		}
		return "[Feat-OneWordEntityInChunkAndNextWord:" + casestr + stemmingstr + words.toString()
				+ "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof OneWordEntityInChunkAndNextWord)) {
			return false;
		}
		OneWordEntityInChunkAndNextWord other = (OneWordEntityInChunkAndNextWord) o;
		if (this.words.equals(other.words) && stemming == other.stemming
				&& caseSensitive == other.caseSensitive) {
			return true;
		}
		return false;
	}
}
