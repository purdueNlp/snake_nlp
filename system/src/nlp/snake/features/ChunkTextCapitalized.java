package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class ChunkTextCapitalized extends Feature {
	private int numCapitials;

	public ChunkTextCapitalized(int num, int numCapitials) {
		this.num = num;
		this.numCapitials = numCapitials;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0) {
			return;
		}

		int matches = 0;
		for (String word : cwords) {
			if (wordInList(word, rfcWords) || wordInList(word, functionWords)
					|| wordInList(word, commonWords)) {
				continue;
			}
			/*
			 * Check for initial capitial and total number of capitals in word
			 */
			int upper = 0;
			for (int c = 0; c < word.length(); c++) {
				if (Character.isUpperCase(word.charAt(c))) {
					upper++;
				}
			}

			/* Just capital letters */
			if (Character.isUpperCase(word.charAt(0)) && upper == 1
					&& chunk.getBeginWordIndex() > 0) {
				matches++;
			}
			if (matches >= numCapitials) {
				features.put(num, POSITIVE_VALUE);
				break;
			}
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-ChunkTextCapitalized:Num-" + numCapitials + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkTextCapitalized)) {
			return false;
		}
		ChunkTextCapitalized other = (ChunkTextCapitalized) o;
		if (other.numCapitials == this.numCapitials) {
			return true;
		}
		return false;
	}

}
