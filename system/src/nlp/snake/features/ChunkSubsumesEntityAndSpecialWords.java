package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class ChunkSubsumesEntityAndSpecialWords extends Feature {
	boolean caseSensitive = false;

	public ChunkSubsumesEntityAndSpecialWords(int num, boolean caseSensitive) {
		this.num = num;
		this.caseSensitive = caseSensitive;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() == 1) {
			return;
		}

		String ent = stripParens(entity.getName());
		if (ent == null || ent.length() == 0) {
			return;
		}

		Sentence s = new Sentence(ent);
		int i = 0;
		// System.out.println(cwords);
		for (int c = 0; c < cwords.size(); c++) {
			if (i < s.words().size()) {
				String tmp1 = s.word(i);
				String tmp2 = cwords.get(c);
				if (!caseSensitive) {
					tmp1 = tmp1.toLowerCase();
					tmp2 = tmp2.toLowerCase();
				}
				if (tmp1.equals(tmp2)) {
					i++;
					continue;
				}
			}

			if (wordInList(cwords.get(c).toLowerCase(), functionWords)
					|| wordInList(cwords.get(c).toLowerCase(), entityExtraWords)) {
				continue;
			} else {
				return;
			}
		}

		if (i < s.words().size()) {
			return;
		} else {
			features.put(num, POSITIVE_VALUE);
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = ":CaseSensitive";
		}
		return "[Feat-ChunkSubsumesEntityAndSpecialWords" + casestr + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkSubsumesEntityAndSpecialWords)) {
			return false;
		}
		return true;
	}
}
