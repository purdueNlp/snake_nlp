package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class ChunkCoRefMatchEntity extends Feature {
	boolean caseSensitive = false;

	public ChunkCoRefMatchEntity(int num, boolean caseSensitive) {
		this.num = num;
		this.caseSensitive = caseSensitive;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		List<Annotation> coRefs = sect.coreferenceResolution();
		for (Annotation cr : coRefs) {
			if (cr.getBeginSentIndex() != chunk.getBeginSentIndex()) {
				continue;
			}
			if (!chunk.contains(cr)) {
				continue;
			}

			if (entityMatchCR(entity, cr)) {
				features.put(num, POSITIVE_VALUE);
				break;
			}
		}
		return;
	}

	private boolean entityMatchCR(Entity entity, Annotation cr) {
		System.out.println(cr.getId() + " #### " + entity.toString());
		Sentence cw = new Sentence(cr.getId());
		Sentence s = new Sentence(entity.getName());
		int i = 0;

		for (int c = 0; c < cw.words().size(); c++) {
			if (i < s.words().size()) {
				String tmp1 = s.word(i);
				String tmp2 = cw.words().get(c);
				if (!caseSensitive) {
					tmp1 = tmp1.toLowerCase();
					tmp2 = tmp2.toLowerCase();
				}
				if (tmp1.equals(tmp2)) {
					i++;
					continue;
				}
			}

			if (wordInList(cw.words().get(c).toLowerCase(), functionWords)
					|| wordInList(cw.words().get(c).toLowerCase(), entityExtraWords)) {
				continue;
			} else {
				return false;
			}
		}

		if (i < s.words().size()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = ":CaseSensitive";
		}
		return "[Feat-ChunkCoRefMatchEntity" + casestr + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkCoRefMatchEntity)) {
			return false;
		}
		return true;
	}
}
