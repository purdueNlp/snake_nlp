package nlp.snake.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class ChunkSubsumesEntityAndHasSamePOS extends Feature {
	boolean caseSensitive = false;

	public ChunkSubsumesEntityAndHasSamePOS(int num, boolean caseSensitive) {
		this.num = num;
		this.caseSensitive = caseSensitive;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() == 1) {
			return;
		}

		List<String> Chunkpostags = new ArrayList<>();
		for (int i = chunk.getBeginWordIndex(); i <= chunk.getEndWordIndex(); i++) {
			Chunkpostags.add(sect.sentence(chunk.getBeginSentIndex()).posTag(i));
		}

		Sentence s = new Sentence(entity.getName());
		List<String> Entitypostags = s.posTags();

		int i = 0;
		for (int c = 0; c < cwords.size(); c++) {
			if (i < s.words().size()) {
				String tmp1 = s.word(i).trim();
				String tmp2 = cwords.get(c).trim();
				if (!caseSensitive) {
					tmp1 = tmp1.toLowerCase();
					tmp2 = tmp2.toLowerCase();
				}
				if (tmp1.equals(tmp2) && Chunkpostags.get(c).equals(Entitypostags.get(i))) {
					i++;
				}
			}
		}

		if (i < s.words().size()) {
			return;
		} else {
			features.put(num, POSITIVE_VALUE);
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = ":CaseSensitive";
		}
		return "[Feat-ChunkSubsumesEntityAndHasSamePOS" + casestr + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkSubsumesEntityAndHasSamePOS)) {
			return false;
		}
		return true;
	}
}
