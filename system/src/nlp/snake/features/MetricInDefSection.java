package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class MetricInDefSection extends Feature {

	public MetricInDefSection(int num) {
		this.num = num;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {

		if (sect.getTitle() == null  || entity.getName().length() == 0) {
			return;
		}

		if (trimHeader(sect.getTitle().text()).equals(entity.getLocation())) {
			features.put(num, POSITIVE_VALUE);
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-MetricInDefSection]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof MetricInDefSection)) {
			return false;
		}
		return true;
	}
}
