package nlp.snake.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class ChunkSubsumesEntityAndPriorWord extends Feature {
	private boolean caseSensitive = false;
	private List<String> words = new ArrayList<>();

	public ChunkSubsumesEntityAndPriorWord(int num, boolean caseSensitive, List<String> words) {
		this.num = num;
		this.caseSensitive = caseSensitive;
		this.words = new ArrayList<>(words);
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() == 1) {
			return;
		}

		Sentence s = new Sentence(stripParens(entity.getName()));

		int state = 0;
		int i = 0;
		for (int c = 0; c < cwords.size(); c++) {
			String tmp1 = "";
			if (i < s.words().size()) {
				tmp1 = s.word(i).trim();
			}

			String tmp2 = cwords.get(c).trim();
			if (!caseSensitive) {
				tmp1 = tmp1.toLowerCase();
				tmp2 = tmp2.toLowerCase();
			}

			switch (state) {
			case 0:
				if (tmp1.equals(tmp2)) {
					i++;
					state = 1;

					if (c - 1 < 0) {
						return;
					}
					boolean found = false;
					for (String w : words) {
						String tmp3 = cwords.get(c - 1).trim();
						if (!caseSensitive) {
							tmp3 = tmp3.toLowerCase();
							w = w.toLowerCase().trim();
						}
						if (tmp3.equals(w)) {
							found = true;
						}
					}
					if (!found) {
						return;
					}
				}
				break;
			case 1:
				if (tmp1.equals(tmp2)) {
					i++;
					break;
				} else {
					state = 2;
				}
			case 2:
				break;
			}
		}

		if (i < s.words().size()) {
			return;
		} else {
			features.put(num, POSITIVE_VALUE);
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = "CaseSensitive:";
		}
		return "[Feat-ChunkSubsumesEntityAndPriorWord:" + casestr + words.toString() + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkSubsumesEntityAndPriorWord)) {
			return false;
		}
		ChunkSubsumesEntityAndPriorWord other = (ChunkSubsumesEntityAndPriorWord) o;
		if (this.words.equals(other.words)) {
			return true;
		}
		return false;
	}
}
