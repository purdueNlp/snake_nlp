package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class OneWordEntityMatchesOneWordChunk extends Feature {

	public OneWordEntityMatchesOneWordChunk(int num) {
		this.num = num;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() != 1) {
			return;
		}

		Sentence s = new Sentence(entity.getName());
		if (s.words().size() != 1) {
			return;
		}

		if (!cwords.get(0).equals(s.word(0))) {
			return;
		}

		boolean leading_capital = false;
		boolean more_capitals = false;
		String word = s.word(0);
		for (int i = 0; i < word.length(); i++) {
			if (Character.isUpperCase(word.charAt(i))) {
				if (i == 0) {
					leading_capital = true;
				} else {
					more_capitals = true;
				}
			}
		}

		if (leading_capital && !more_capitals && chunk.getBeginWordIndex() == 0) {
			// Simply the start of a sentence
			return;
		}

		features.put(num, POSITIVE_VALUE);
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-OneWordEntityMatchesOneWordChunk]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof OneWordEntityMatchesOneWordChunk)) {
			return false;
		}
		return true;
	}
}
