package nlp.snake.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.util.Characters;

public class PartialEntityMatchesChunk extends Feature {
	private List<Entity> entities = null; // this is a reference to the entity
											// list!
	private Map<Entity, List<String>> unique = new HashMap<>();
	private boolean caseSensitive = false;

	public PartialEntityMatchesChunk(int num, boolean caseSensitive, List<Entity> entities) {
		this.num = num;
		this.caseSensitive = caseSensitive;
		this.entities = entities;
		this.unique.clear();
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() == 1) {
			return;
		}

		String ent = stripParens(entity.getName());
		if (ent == null || ent.length() == 0) {
			return;
		}

		if (unique.isEmpty()) {
			processEntities();
		}

		for (String w : unique.get(entity)) {
			for (int c = 0; c < cwords.size(); c++) {
				String tmp1 = cwords.get(c);
				String tmp2 = w;
				if (!caseSensitive) {
					tmp1 = tmp1.toLowerCase();
					tmp2 = tmp2.toLowerCase();
				}
				if (tmp1.equals(tmp2)
						&& (c == cwords.size() - 1 || !containsNums(cwords.get(c + 1)))) {
					features.put(num, POSITIVE_VALUE);
					break;
				}
			}
		}

		return;
	}

	@Override
	public void newDoc() {
		unique.clear();
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = ":CaseSensitive";
		}
		return "[Feat-PartialEntityMatchesChunk" + casestr + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof PartialEntityMatchesChunk)) {
			return false;
		}
		return true;
	}

	private void processEntities() {
		System.out.println(entities);

		for (Entity e : entities) {
			/* Grab words */
			String ent = stripParens(e.getName());
			if (ent == null || ent.length() == 0) {
				unique.put(e, new ArrayList<String>());
				continue;
			}
			Sentence s = new Sentence(ent);

			List<String> uwords = new ArrayList<String>();
			for (int i = 0; i < s.words().size(); i++) {
				String w = s.originalText(i);
				boolean found = false;
				if (containsNums(w) || containsPunct(w)) {
					found = true;
				}
				if (wordInList(w.toLowerCase(), functionWords)
						|| wordInList(w.toLowerCase(), commonWords)
						|| wordInList(w.toLowerCase(), rfcWords)
						|| wordInList(w.toLowerCase(), entityExtraWords)
						|| wordInList(w.toLowerCase(), difficultWords)) {
					found = true;
				}
				for (Entity o : entities) {
					if (o.equals(e)) {
						continue;
					}

					if (o.getName().contains(w)
							|| o.getName().toLowerCase().contains(s.lemma(i).toLowerCase())) {
						found = true;
						break;
					}
				}
				if (!found) {
					uwords.add(w);
				}
			}

			unique.put(e, uwords);
		}

		System.out.println(unique.toString());
	}

	private boolean containsNums(String tmp) {
		for (int i = 0; i < tmp.length(); i++) {
			if (Character.isDigit(tmp.charAt(i))) {
				return true;
			}
		}
		return false;
	}

	private boolean containsPunct(String tmp) {
		for (int i = 0; i < tmp.length(); i++) {
			if (Characters.isPunctuation(tmp.charAt(i))) {
				return true;
			}
		}
		return false;
	}
}
