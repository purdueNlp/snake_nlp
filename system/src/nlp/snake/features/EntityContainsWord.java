package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class EntityContainsWord extends Feature {
	String word;

	public EntityContainsWord(int num, String word) {
		this.num = num;
		this.word = word;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (entity.getName().contains(word)) {
			features.put(num, POSITIVE_VALUE);
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-EntityContainsWord:" + word + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof EntityContainsWord)) {
			return false;
		}

		EntityContainsWord other = (EntityContainsWord) o;
		if (this.word.equals(other.word)) {
			return true;
		}
		return false;
	}

}
