package nlp.snake.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.util.Characters;

public class ChunkEntityAndExtraWords extends Feature {
	private boolean caseSensitive = false;
	private List<Entity> entities = null; // this is a reference to the entity

	// list!

	public ChunkEntityAndExtraWords(int num, boolean caseSensitive, List<Entity> entities) {
		this.num = num;
		this.caseSensitive = caseSensitive;
		this.entities = entities;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() == 1) {
			return;
		}

		String ent = stripParens(entity.getName());
		if (ent == null || ent.length() == 0) {
			return;
		}
		if (ent.toLowerCase().equals("options") || ent.toLowerCase().equals("option")
				|| ent.toLowerCase().equals("type")) {
			return;
		}

		if (containsNums(chunk.getExpression())) {
			return;
		}

		if (foundEntity(ent, cwords)) {
			for (Entity e : entities) {
				if (entity.equals(e)) {
					continue;
				}
				if (foundEntity(stripParens(e.getName()), cwords)) {
					return;
				}
			}

			features.put(num, POSITIVE_VALUE);
		}
		return;
	}

	private boolean foundEntity(String entity, List<String> cwords) {
		Sentence s = new Sentence(entity);
		List<String> ewords = s.words();
		List<Boolean> matched = new ArrayList<>();
		for (int i = 0; i < ewords.size(); i++) {
			matched.add(false);
		}

		/* Search for all words of entity */
		for (int c = 0; c < cwords.size(); c++) {
			for (int w = 0; w < ewords.size(); w++) {
				String tmp1 = cwords.get(c);
				String tmp2 = ewords.get(w);
				if (!caseSensitive) {
					tmp1 = tmp1.toLowerCase();
					tmp2 = tmp2.toLowerCase();
				}
				if (tmp1.equals(tmp2)) {
					matched.set(w, true);
				}
			}
		}
		boolean found = true;
		for (int i = 0; i < matched.size(); i++) {
			if (matched.get(i) == false) {
				found = false;
				break;
			}
		}

		if (found) {
			return true;
		}
		return false;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = ":CaseSensitive";
		}
		return "[Feat-ChunkEntityAndExtraWords" + casestr + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkEntityAndExtraWords)) {
			return false;
		}
		return true;
	}

	private boolean containsNums(String tmp) {
		for (int i = 0; i < tmp.length(); i++) {
			if (Character.isDigit(tmp.charAt(i))) {
				return true;
			}
		}
		return false;
	}

	private boolean containsPunct(String tmp) {
		for (int i = 0; i < tmp.length(); i++) {
			if (Characters.isPunctuation(tmp.charAt(i))) {
				return true;
			}
		}
		return false;
	}
}
