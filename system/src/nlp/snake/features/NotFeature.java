package nlp.snake.features;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class NotFeature extends Feature {
	private Feature f1 = null;

	public NotFeature(int num, Feature f1) {
		this.num = num;
		this.f1 = f1;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		Map<Integer, Integer> feat1 = new HashMap<>();

		if (f1 == null) {
			return;
		}

		f1.extract(entity, chunk, cwords, sect, feat1);
		if (feat1.size() == 0) {
			features.put(num, POSITIVE_VALUE);
			return;
		}
	}

	@Override
	public void newDoc() {
		if (f1 == null) {
			return;
		}
		f1.newDoc();
		return;
	}

	@Override
	public String toString() {
		String s1 = "";
		if (f1 != null) {
			s1 = f1.toString();
		}
		return "[Feat-Not:" + s1 + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof NotFeature)) {
			return false;
		}
		NotFeature other = (NotFeature) o;

		if (this.f1 == null || other.f1 == null) {
			return true;
		}

		if (!this.f1.equals(other.f1)) {
			return false;
		}

		return true;
	}

}
