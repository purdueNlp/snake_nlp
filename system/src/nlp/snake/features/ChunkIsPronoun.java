package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class ChunkIsPronoun extends Feature {

	public ChunkIsPronoun(int num) {
		this.num = num;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0 || sect.getTitle() == null) {
			return;
		}

		if (!trimHeader(sect.getTitle().text()).equals(entity.getLocation())) {
			return;
		}

		if (chunk.getExpression().toLowerCase().contains("this field")) {
			features.put(num, POSITIVE_VALUE);
			return;
		}

		if (chunk.getExpression().contains("They")) {
			features.put(num, POSITIVE_VALUE);
			return;
		}

		if (chunk.getExpression().toLowerCase().contains("their")) {
			features.put(num, POSITIVE_VALUE);
			return;
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-ChunkIsPronoun]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkIsPronoun)) {
			return false;
		}
		return false;
	}

}
