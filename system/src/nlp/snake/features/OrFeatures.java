package nlp.snake.features;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class OrFeatures extends Feature {
	private Feature f1 = null;
	private Feature f2 = null;

	public OrFeatures(int num, Feature f1, Feature f2) {
		this.num = num;
		this.f1 = f1;
		this.f2 = f2;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		Map<Integer, Integer> feat1 = new HashMap<>();
		Map<Integer, Integer> feat2 = new HashMap<>();

		if (f1 == null || f2 == null) {
			return;
		}

		f1.extract(entity, chunk, cwords, sect, feat1);
		if (feat1.size() > 0) {
			features.put(num, POSITIVE_VALUE);
			return;
		}

		f2.extract(entity, chunk, cwords, sect, feat2);
		if (feat2.size() > 0) {
			features.put(num, POSITIVE_VALUE);
			return;
		}
	}

	@Override
	public void newDoc() {
		if (f1 == null || f2 == null) {
			return;
		}
		f1.newDoc();
		f2.newDoc();
		return;
	}

	@Override
	public String toString() {
		String s1 = "";
		String s2 = "";
		if (f1 != null) {
			s1 = f1.toString();
		}
		if (f2 != null) {
			s2 = f2.toString();
		}
		return "[Feat-Or:" + s1 + s2 + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof OrFeatures)) {
			return false;
		}
		OrFeatures other = (OrFeatures) o;

		if (this.f1 == null || this.f2 == null || other.f1 == null || other.f2 == null) {
			return true;
		}

		if (!this.f1.equals(other.f1) && !this.f1.equals(other.f2)) {
			return false;
		}

		if (!this.f2.equals(other.f1) && !this.f2.equals(other.f2)) {
			return false;
		}

		return true;
	}

}
