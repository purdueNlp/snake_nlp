package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.ie.machinereading.structure.Span;
import edu.stanford.nlp.simple.Sentence;

public class ChunkHeadWordIsPartOfEntity extends Feature {
	boolean caseSensitive = false;

	public ChunkHeadWordIsPartOfEntity(int num, boolean caseSensitive) {
		this.num = num;
		this.caseSensitive = caseSensitive;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() == 1) {
			return;
		}

		int head = sect.sentence(chunk.getBeginSentIndex()).algorithms()
				.headOfSpan(new Span(chunk.getBeginWordIndex(), chunk.getEndWordIndex() + 1));

		Sentence s = new Sentence(entity.getName());
		int i = 0;
		boolean matched = false;
		for (int c = 0; c < cwords.size(); c++) {
			if (i < s.words().size()) {
				String tmp1 = s.word(i);
				String tmp2 = cwords.get(c);
				if (!caseSensitive) {
					tmp1 = tmp1.toLowerCase();
					tmp2 = tmp2.toLowerCase();
				}
				if (tmp1.equals(tmp2)) {
					if (i == head - chunk.getBeginWordIndex()) {
						matched = true;
					}
					i++;
				}
			}
		}

		if (i < s.words().size() || !matched) {
			return;
		} else {
			System.out.println(cwords);
			features.put(num, POSITIVE_VALUE);
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = ":CaseSensitive";
		}
		return "[Feat-ChunkHeadWordIsPartOFEntity" + casestr + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkHeadWordIsPartOfEntity)) {
			return false;
		}
		return true;
	}
}
