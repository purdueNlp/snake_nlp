package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class ChunkContainsWord extends Feature {
	String word;

	public ChunkContainsWord(int num, String word) {
		this.word = word;
		this.num = num;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0) {
			return;
		}

		for (String w : cwords) {
			if (w.contains(word)) {
				features.put(num, 1);
				break;
			}
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-ChunkContainsWord:" + word + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkContainsWord)) {
			return false;
		}
		ChunkContainsWord other = (ChunkContainsWord) o;

		if (this.word.equals(other.word)) {
			return true;
		}

		return false;
	}

}
