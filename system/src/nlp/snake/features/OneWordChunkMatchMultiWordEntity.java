package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class OneWordChunkMatchMultiWordEntity extends Feature {
	boolean caseSensitive = false;

	public OneWordChunkMatchMultiWordEntity(int num, boolean caseSensitive) {
		this.num = num;
		this.caseSensitive = caseSensitive;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() != 1) {
			return;
		}
		if (wordInList(cwords.get(0), functionWords)) {
			return;
		}

		String ent = stripParens(entity.getName());
		if (ent == null || ent.length() == 0) {
			return;
		}

		Sentence s = new Sentence(ent);
		if (s.words().size() <= 1) {
			return;
		}

		String tmp2 = cwords.get(0).trim();
		for (int i = 0; i < s.words().size(); i++) {
			String tmp1 = s.word(i);
			if (!caseSensitive) {
				tmp1 = tmp1.toLowerCase();
				tmp2 = tmp2.toLowerCase();
			}
			if (tmp1.equals(tmp2)) {
				features.put(num, POSITIVE_VALUE);
			}
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = ":CaseSensitive";
		}
		return "[Feat-OneWordChunkMatchMultiWordEntity" + casestr + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof OneWordChunkMatchMultiWordEntity)) {
			return false;
		}
		return true;
	}
}
