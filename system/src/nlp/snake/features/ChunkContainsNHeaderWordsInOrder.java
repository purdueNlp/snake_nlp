package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class ChunkContainsNHeaderWordsInOrder extends Feature {
	private int matchlen;

	public ChunkContainsNHeaderWordsInOrder(int num, int matchlen) {
		this.num = num;
		this.matchlen = matchlen;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0 || sect.getTitle() == null  || entity.getName().length() == 0) {
			return;
		}

		if (!trimHeader(sect.getTitle().text()).equals(entity.getLocation())) {
			return;
		}

		/* Find non-Function Words in entity */
		Sentence s = new Sentence(trimHeader(sect.getTitle().text()));
		for (int w = 0; w < s.words().size(); w++) {
			for (int c = 0; c < cwords.size(); c++) {
				int i = 0;
				while (c + i < cwords.size() && w + i < s.words().size()
						&& cwords.get(c + i).equals(s.word(w + i))) {
					i++;
					if (i >= matchlen) {
						features.put(num, POSITIVE_VALUE);
						break;
					}
				}
			}
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-ChunkContainsNHeaderWordsInOrder:Len-" + matchlen + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkContainsNHeaderWordsInOrder)) {
			return false;
		}
		ChunkContainsNHeaderWordsInOrder other = (ChunkContainsNHeaderWordsInOrder) o;
		if (this.matchlen == other.matchlen) {
			return true;
		}
		return false;
	}
}
