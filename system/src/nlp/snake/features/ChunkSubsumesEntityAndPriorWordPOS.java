package nlp.snake.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class ChunkSubsumesEntityAndPriorWordPOS extends Feature {
	private boolean caseSensitive = false;
	private String priorPOS = "";

	public ChunkSubsumesEntityAndPriorWordPOS(int num, boolean caseSensitive, String priorPOS) {
		this.num = num;
		this.caseSensitive = caseSensitive;
		this.priorPOS = priorPOS;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0  || entity.getName().length() == 0) {
			return;
		}

		if (cwords.size() == 1) {
			return;
		}

		List<String> Chunkpostags = new ArrayList<>();
		for (int i = chunk.getBeginWordIndex(); i <= chunk.getEndWordIndex(); i++) {
			Chunkpostags.add(sect.sentence(chunk.getBeginSentIndex()).posTag(i));
		}
		Chunkpostags = simplifyPOStag(Chunkpostags);

		Sentence s = new Sentence(stripParens(entity.getName()));

		int state = 0;
		int i = 0;
		for (int c = 0; c < cwords.size(); c++) {
			String tmp1 = "";
			if (i < s.words().size()) {
				tmp1 = s.word(i).trim();
			}

			String tmp2 = cwords.get(c).trim();
			if (!caseSensitive) {
				tmp1 = tmp1.toLowerCase();
				tmp2 = tmp2.toLowerCase();
			}

			switch (state) {
			case 0:
				if (tmp1.equals(tmp2)) {
					i++;
					state = 1;
					if (c - 1 < 0 || !Chunkpostags.get(c - 1).equals(priorPOS)) {
						return;
					}
				}
				break;
			case 1:
				if (tmp1.equals(tmp2)) {
					i++;
					break;
				} else {
					state = 2;
				}
			case 2:
				break;
			}
		}

		if (i < s.words().size()) {
			return;
		} else {
			features.put(num, POSITIVE_VALUE);
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (caseSensitive) {
			casestr = "CaseSensitive:";
		}
		return "[Feat-ChunkSubsumesEntityAndPriorWordPOS:" + casestr + priorPOS + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkSubsumesEntityAndPriorWordPOS)) {
			return false;
		}
		ChunkSubsumesEntityAndPriorWordPOS other = (ChunkSubsumesEntityAndPriorWordPOS) o;
		if (this.priorPOS.equals(other.priorPOS)) {
			return true;
		}
		return false;
	}
}
