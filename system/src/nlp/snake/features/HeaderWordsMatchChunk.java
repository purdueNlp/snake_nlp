package nlp.snake.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.util.Characters;

public class HeaderWordsMatchChunk extends Feature {
	private boolean matchCase;
	private int numMatches;

	public HeaderWordsMatchChunk(int num, boolean caseSensitive, int numMatches) {
		this.num = num;
		this.matchCase = caseSensitive;
		this.numMatches = numMatches;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0 || sect.getTitle() == null  || entity.getName().length() == 0) {
			return;
		}

		if (!trimHeader(sect.getTitle().text()).equals(entity.getLocation())) {
			return;
		}

		/* Find non-Function Words in entity */
		Sentence s = new Sentence(sect.getTitle().text());
		int matches = 0;
		for (String w : nonFunctionWords(s)) {
			for (String c : cwords) {
				if (!matchCase) {
					c = c.toLowerCase();
				}
				if (w.equals(c)) {
					matches++;
				}
				if (matches >= numMatches) {
					features.put(num, POSITIVE_VALUE);
					break;
				}
			}
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		String casestr = "";
		if (matchCase) {
			casestr = ":CaseSensitive";
		}
		return "[Feat-HeaderWordsMatchChunk" + casestr + ":Num-" + numMatches + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof HeaderWordsMatchChunk)) {
			return false;
		}
		HeaderWordsMatchChunk other = (HeaderWordsMatchChunk) o;
		if (this.matchCase == other.matchCase && this.numMatches == other.numMatches) {
			return true;
		}
		return false;
	}

	private List<String> nonFunctionWords(Sentence s) {
		List<String> nfw = new ArrayList<>();

		if (s == null) {
			return nfw;
		}

		for (String w : s.originalTexts()) {
			boolean found = false;
			String tmp = w.toLowerCase();

			/* Preset list of words */
			if (wordInList(tmp, functionWords) || wordInList(tmp, commonWords)
					|| wordInList(tmp, rfcWords)) {
				found = true;
			}

			/* Contains punctuation or numbers */
			for (int i = 0; i < tmp.length(); i++) {
				if (Character.isDigit(tmp.charAt(i))) {
					found = true;
					break;
				}
			}

			if (tmp.length() == 1 && Characters.isPunctuation(tmp.charAt(0))) {
				found = true;
			}

			if (!found) {
				if (matchCase) {
					nfw.add(w);
				} else {
					nfw.add(w.toLowerCase());
				}
			}
		}
		return nfw;
	}
}
