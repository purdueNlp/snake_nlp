package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class Bias extends Feature {

	public Bias(int num) {
		this.num = num;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		features.put(num, POSITIVE_VALUE);
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-Bias]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Bias)) {
			return false;
		}
		return true;
	}

}
