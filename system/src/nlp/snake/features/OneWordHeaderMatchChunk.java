package nlp.snake.features;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class OneWordHeaderMatchChunk extends Feature {

	public OneWordHeaderMatchChunk(int num) {
		this.num = num;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0 || sect.getTitle() == null  || entity.getName().length() == 0) {
			return;
		}

		if (!trimHeader(sect.getTitle().text()).equals(entity.getLocation())) {
			return;
		}

		Sentence s = new Sentence(entity.getName());
		if (s.words().size() != 1) {
			return;
		}

		if (wordInList(s.word(0).toLowerCase(), functionWords)) {
			return;
		}

		for (int c = 0; c < cwords.size(); c++) {
			if (cwords.get(c).equals(s.word(0))) {
				Optional<String> opt = sect.getText().sentence(chunk.getBeginSentIndex())
						.incomingDependencyLabel(chunk.getBeginWordIndex() + c);
				if (opt.isPresent() && !opt.get().equals("compound")) {
					features.put(num, POSITIVE_VALUE);
					break;
				}
			}
		}
		return;
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-OneWordMatchChunk]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof OneWordHeaderMatchChunk)) {
			return false;
		}
		return true;
	}
}
