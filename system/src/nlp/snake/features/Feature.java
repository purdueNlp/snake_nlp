package nlp.snake.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public abstract class Feature {
	protected static final String functionWords[] = { "of", "at", "in", "without", "between", "he",
			"she", "they", "it", "the", "a", "an", "that", "my", "more", "much", "neither",
			"either", "and", "that", "which", "when", "while", "although", "or", "be", "is", "am",
			"are", "have", "got", "do", "no", "not", "nor", "as", };
	protected static final String commonWords[] = { "to", "from", "each", "'s" };
	protected static final String rfcWords[] = { "section", "sections", "bits", "bit", "byte",
			"bytes", "packet", "packets", "header" };
	protected static final String entityExtraWords[] = { "bits", "bit", "byte", "bytes", "packet",
			"packets", "'s", "header", "value", "values", "field", "fields", "port", "ports" };
	protected static final String difficultWords[] = { "options", "reserved", "code" };
	protected static final int POSITIVE_VALUE = 1;
	protected static final int NEGATIVE_VALUE = -1;
	protected static final int UNKNOWN_VALUE = 0;
	protected int num;

	public abstract void extract(Entity entity, Annotation chunk, List<String> cwords,
			ProtocolSection sect, Map<Integer, Integer> features);

	public abstract void newDoc();

	public abstract String toString();

	public abstract boolean equals(Object o);

	public int getNum() {
		return num;
	}

	protected boolean wordInList(String word, String list[]) {
		String w = word.toLowerCase();
		for (int i = 0; i < list.length; i++) {
			if (w.equals(list[i])) {
				return true;
			}
		}
		return false;
	}

	protected String trimHeader(String header) {
		if (header.contains(":")) {
			header = header.split(":")[0];
		}
		return header.trim();
	}

	protected String stripParens(String entity) {
		if (entity.contains("(") && entity.contains(")")) {
			entity = entity.substring(0, entity.indexOf("("))
					+ entity.substring(entity.indexOf(")") + 1);
		}
		return entity.trim();
	}

	protected List<String> simplifyPOStag(List<String> tags) {
		List<String> lst = new ArrayList<>();
		for (String t : tags) {
			lst.add(simplifyPOStag(t));
		}
		return lst;
	}

	protected String simplifyPOStag(String tag) {
		if (tag.equals("CC")) {
			// Coordinating Conjunction
			return "CC";
		} else if (tag.equals("CD")) {
			// Cardinal number
			return "CD";
		} else if (tag.equals("DT")) {
			// Determiner
			return "DT";
		} else if (tag.equals("EX")) {
			// Existential there
			return "U";
		} else if (tag.equals("FW")) {
			// Foreign word
			return "U";
		} else if (tag.equals("IN")) {
			// Preposition or subordinating conjunction
			return "PP";
		} else if (tag.equals("JJ")) {
			// Adjective
			return "JJ";
		} else if (tag.equals("JJR")) {
			// Adjective, comparative
			return "JJ";
		} else if (tag.equals("JJS")) {
			// Adjective, superlative
			return "JJ";
		} else if (tag.equals("LS")) {
			// List item marker
			return "U";
		} else if (tag.equals("MD")) {
			// Modal
			return "U";
		} else if (tag.equals("NN")) {
			// Noun, singular or mass
			return "NN";
		} else if (tag.equals("NNS")) {
			// Noun, plural
			return "NN";
		} else if (tag.equals("NNP")) {
			// Proper noun, singular
			return "NN";
		} else if (tag.equals("NNPS")) {
			// Proper noun, plural
			return "NN";
		} else if (tag.equals("PDT")) {
			// Predeterminer
			return "U";
		} else if (tag.equals("POS")) {
			// Possessive ending
			return "U";
		} else if (tag.equals("PRP")) {
			// Personal pronoun
			return "PR";
		} else if (tag.equals("PRP$")) {
			// Possessive pronoun
			return "PR";
		} else if (tag.equals("RB")) {
			// Adverb
			return "RB";
		} else if (tag.equals("RBR")) {
			// Adverb, comparative
			return "RB";
		} else if (tag.equals("RBS")) {
			// Adverb, superlative
			return "RB";
		} else if (tag.equals("RP")) {
			// Particle
			return "U";
		} else if (tag.equals("SYM")) {
			// Symbol
			return "PU";
		} else if (tag.equals("TO")) {
			// to
			return "U";
		} else if (tag.equals("UH")) {
			// Interjection
			return "U";
		} else if (tag.equals("VB")) {
			// Verb, base form
			return "VB";
		} else if (tag.equals("VBD")) {
			// Verb, past tense
			return "VB";
		} else if (tag.equals("VBG")) {
			// Verb, gerund or present participle
			return "VB";
		} else if (tag.equals("VBN")) {
			// Verb, past participle
			return "VB";
		} else if (tag.equals("VBP")) {
			// Verb, non-3rd person singular present
			return "VB";
		} else if (tag.equals("VBZ")) {
			// Verb, 3rd person singular present
			return "VB";
		} else if (tag.equals("WDT")) {
			// Wh-determiner
			return "U";
		} else if (tag.equals("WP")) {
			// Wh-pronoun
			return "PR";
		} else if (tag.equals("WP$")) {
			// Possessive wh-pronoun
			return "PR";
		} else if (tag.equals("WRB")) {
			// Wh-adverb
			return "RB";
		} else if (tag.equals(".")) {
			return "PU";
		} else if (tag.equals(":")) {
			return "PU";
		} else if (tag.equals("`")) {
			return "PU";
		} else if (tag.equals("``")) {
			return "PU";
		} else if (tag.equals("''")) {
			return "PU";
		} else if (tag.equals("-LRB-")) {
			return "PU";
		} else if (tag.equals("-RRB-")) {
			return "PU";
		} else {
			System.out.println("Unknown POS tag: " + tag);
		}
		return tag;
	}
}
