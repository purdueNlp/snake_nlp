package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class EntityIsMultiWord extends Feature {

	public EntityIsMultiWord(int num) {
		this.num = num;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (cwords == null || cwords.size() == 0 || sect.getTitle() == null || entity.getName().length() == 0) {
			return;
		}

		Sentence s = new Sentence(entity.getName());

		if (s.words().size() > 1) {
			features.put(num, POSITIVE_VALUE);
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-EntityIsMultiWord]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof EntityIsMultiWord)) {
			return false;
		}
		return false;
	}

}
