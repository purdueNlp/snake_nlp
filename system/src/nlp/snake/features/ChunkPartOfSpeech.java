package nlp.snake.features;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.ProtocolSection;

public class ChunkPartOfSpeech extends Feature {
	private String pos;
	public static final String NOUN_PHRASE = "NP";
	public static final String VERB_PHRASE = "VP";
	public static final String PREPOSITIONAL_PHRASE = "PP";
	public static final String ADJACTIVE_PHRASE = "ADVP";

	public ChunkPartOfSpeech(int num, String pos) {
		this.num = num;
		this.pos = pos;
	}

	@Override
	public void extract(Entity entity, Annotation chunk, List<String> cwords, ProtocolSection sect,
			Map<Integer, Integer> features) {
		if (chunk.getId().equals(pos)) {
			features.put(num, POSITIVE_VALUE);
		}
	}

	@Override
	public void newDoc() {
		return;
	}

	@Override
	public String toString() {
		return "[Feat-ChunkPartOfSpeech:" + pos + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ChunkPartOfSpeech)) {
			return false;
		}
		ChunkPartOfSpeech other = (ChunkPartOfSpeech) o;
		if (this.pos.equals(other.pos)) {
			return true;
		}
		return false;
	}

}
