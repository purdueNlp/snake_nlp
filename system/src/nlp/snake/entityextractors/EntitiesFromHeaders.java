package nlp.snake.entityextractors;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nlp.snake.config.Config;
import nlp.snake.ent.ASCIITable;
import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class EntitiesFromHeaders extends EntityExtractor {
	private static final String customFunctionWords[] = { "at", "without", "between", "he", "she",
			"they", "it", "a", "an", "that", "my", "much", "neither", "either",
			"that", "which", "when", "while", "although", "be", "is", "am", "are", "have", "got",
			"do", "not", "nor", "as", };
	private static final String pktTypeDefs[] = { "definition", "definitions", "format", "formats",
			"extension", "extensions"};
	private static final String genericTypes[] = {"generic", "common", "field", "fields"};
	private List<Entity> entities = new ArrayList<>();
	private Pattern bitwidth1 = Pattern.compile("(([0-9]+ or )?[0-9]+[ -]bits?)");
	private Pattern bitwidth2 = Pattern.compile("(bits?[ \t\n\r\f]+[0-9-]+)");
	private Pattern bitwidth3 = Pattern.compile("([0-9]+ octets?)");
	private Pattern topsection = Pattern.compile("^([0-9]+)\\.");
	private Pattern typevaluematch = Pattern.compile("\\( *([0-9]+) *\\)");
	private Pattern numbers = Pattern.compile(" ([0-9]+) ");
	private String sizehint = null;
	private boolean allHeadersNumbered = true;


	public EntitiesFromHeaders() {
	}

	@Override
	public List<Entity> processDocument(InputProtocol proto) {
		sizehint = null;
		allHeadersNumbered = true;
		entities.clear();

		System.out.println("##### FindEntities Doc: " + proto.getTitle());
		for (ProtocolSection s : proto.getSections()) {
			scanHeaders(s);
		}
		for (ProtocolSection s : proto.getSections()) {
			lookForEntities(s,null);
		}
		for (ProtocolSection s: proto.getSections()) {
			fillEntityValues(s);
		}

		checkEntities();

		if (Config.debug > 1) {
			for (Entity e : entities) {
				System.err.println("Entity: " + e.toString());
			}
		}
		if (Config.debug > 0) {
			System.err.println("Header Entities: " + entities.size());
		}
		
		//Add null entity
		entities.add(new Entity());

		proto.addEntities(entities,false);
		return entities;
	}

	private void checkEntities() {
		List<Entity> ne = new ArrayList<>();
		int types=0;
		
		/* Count number of pkt types */
		for(Entity e: entities) {
			if (e.getType().equals("pkt-type")) {
				types++;
			}
		}
		
		for (Entity e : entities) {
			if (e.getType().equals("field")) {
				if (e.getValue().length() > 0) {
					ne.add(e);
				}
			} else if (e.getType().equals("pkt-type") && types > 1) {
				if (!Config.only_types_with_values || e.getValue().length() > 0) {
					ne.add(e);
				}
			}
		}

		entities = ne;
	}
	
	private void scanHeaders(ProtocolSection sect) {
		Sentence title = sect.getTitle();
		if (title != null) {
			if (!Character.isDigit(title.text().codePointAt(0))) {
				allHeadersNumbered = false;
			}
		}

		for (ProtocolSection s : sect.getSubsections()) {
			scanHeaders(s);
		}
		return;
	}

	private void lookForEntities(ProtocolSection sect, ProtocolSection top) {
		System.out.println("##### FindEntities Section: " + sect.getTitle());
		Sentence title = sect.getTitle();
		String sizeh = null;

		if (title != null) {
			if (Config.entity_dofields) {
				lookForFields(sect);
			}
			if (Config.entity_dotypes) {
				lookForTypes(sect,top);
			}
			sizeh = sizehint;
		}

		for (ProtocolSection s : sect.getSubsections()) {
			sizehint = sizeh;
			lookForEntities(s,sect);
		}
		sizehint = null;
		return;
	}
	
	private void fillEntityValues(ProtocolSection sect) {
		if (Config.entity_dotypes) {
			findPktTypeValues(sect);
		}
		for (ProtocolSection s : sect.getSubsections()) {
			fillEntityValues(s);
		}
	}

	private void lookForFields(ProtocolSection sect) {
		String title = sect.getTitle().text();

		/* Determine if this is a field */
		if (!isField(title)) {
			return;
		}

		/* Split between name and spec */
		String namePart = removeSectionNumbers(title).trim();
		String specPart = "";
		if (removeSectionNumbers(title).split(":").length > 1) {
			namePart = removeSectionNumbers(title).split(":")[0];
			specPart = removeSectionNumbers(title).split(":")[1];
		}
		if (allHeadersNumbered && specPart.length() == 0 && namePart.split("\\(").length > 1) {
			namePart = removeSectionNumbers(title).split("\\(")[0];
			specPart = removeSectionNumbers(title).split("\\(")[1];
		}

		/* Process Spec Part */
		specPart = processSpec(specPart, sect);

		/* Handle multiple fields in same line */
		List<Entity> ents = new ArrayList<>();
		if (namePart.contains(",")) {
			for (String p : namePart.split(",")) {
				if (p.contains("and ")) {
					int andloc = p.toLowerCase().indexOf("and ");
					if (andloc > 0) {
						p = p.substring(0, andloc) + p.substring(andloc + 4);
					} else {
						p = p.substring(andloc + 4);
					}
				}
				ents.add(new Entity(entitynum++, p.trim(), processTitle(title), "field", specPart));
			}
		} else if (namePart.toLowerCase().contains(" and ")) {
			int andloc = namePart.toLowerCase().indexOf(" and ");
			ents.add(new Entity(entitynum++, namePart.substring(0, andloc).trim(),
					processTitle(title), "field", specPart));
			ents.add(new Entity(entitynum++, namePart.substring(andloc + 5).trim(),
					processTitle(title), "field", specPart));
		} else {
			ents.add(new Entity(entitynum++, namePart.trim(), processTitle(title), "field",
					specPart));
		}

		/* Pull Any Annotation on this line */
		List<Integer> AnnotationRefs = new ArrayList<>();
		List<Annotation> truth = sect.getTitleAnnotations("fieldname");
		for (Annotation a : truth) {
			for (Integer i : a.getRefs()) {
				AnnotationRefs.add(i);
			}
		}

		/* Combine Annotation with Entities and Insert */
		Entity e = null;
		for (int i = 0; i < ents.size(); i++) {
			e = ents.get(i);
			if (i < AnnotationRefs.size()) {
				e.setRealId(AnnotationRefs.get(i));
			}

			if (!entities.contains(e)) {
				entities.add(e);
			}
		}
	}
	
	private void lookForTypes(ProtocolSection sect, ProtocolSection top) {
		String title = sect.getTitle().text();
		
		/* Determine if this is a packet type */
		if (!isType(sect,top)) {
			return;
		}

		title = removeSectionNumbers(title).trim();
		
		/* Handle multiple fields in same line */
		List<Entity> ents = new ArrayList<>();
		if (title.contains(",")) {
			for (String p : title.split(",")) {
				if (p.contains("and ")) {
					int andloc = p.toLowerCase().indexOf("and ");
					if (andloc > 0) {
						p = p.substring(0, andloc) + p.substring(andloc + 4);
					} else {
						p = p.substring(andloc + 4);
					}
				}
				ents.add(new Entity(entitynum++, typeName(p.trim()), processTitle(title), "pkt-type", typeValue(p)));
			}
		} else if (title.toLowerCase().contains(" and ")) {
			int andloc = title.toLowerCase().indexOf(" and ");
			ents.add(new Entity(entitynum++, typeName(title.substring(0, andloc).trim()),
					processTitle(title), "pkt-type", typeValue(title.substring(0, andloc).trim())));
			ents.add(new Entity(entitynum++, typeName(title.substring(andloc + 5).trim()),
					processTitle(title), "pkt-type", typeValue(title.substring(andloc + 5).trim())));
		} else {
			ents.add(new Entity(entitynum++, typeName(title.trim()), processTitle(title), "pkt-type",typeValue(title.trim())));
		}
		
		/* Look for value in first line */
		for (Entity e: ents) {
			if (ents.size() == 1 && e.getValue().length() == 0 && sect.getText() != null) {
				Matcher m = numbers.matcher(sect.getText().text());
				if (m.find()) {
					e.setValue(m.group(1));
				}
			}
		}
		
		/* Pull Any Annotation on this line */
		List<Integer> AnnotationRefs = new ArrayList<>();
		List<Annotation> truth = sect.getTitleAnnotations("pkt_type");
		for (Annotation a : truth) {
			for (Integer i : a.getRefs()) {
				AnnotationRefs.add(i);
			}
		}
		
		/* Combine Annotation with Entities and Insert */
		Entity e = null;
		for (int i = 0; i < ents.size(); i++) {
			e = ents.get(i);
			if (i < AnnotationRefs.size()) {
				e.setRealId(AnnotationRefs.get(i));
			}

			if (!entities.contains(e)) {
				entities.add(e);
			}
		}
		
	}
	
	private void findPktTypeValues(ProtocolSection sect) {
		boolean found;
		int num = 0;
		
		for (ASCIITable tbl: sect.getTables()) {
			found = false;
			for(String ln: tbl.getOtherText()) {
				if (ln.toLowerCase().contains("packet types")) {
					found = true;
					break;
				}
			}
			
			if (found) {
				int val = -1;
				String name = "";
				for (List<String> row: tbl.getTable()) {
					if (row.size() < 1 || row.size() > 2) {
						break;
					}
					for (String c: row) {
						try {
							val = Integer.parseInt(c);
						} catch (Exception e) {
							name = c;
						}
					}
					
					if (val < 0) {
						continue;
					}
					
					for (Entity e: entities) {
						if (!e.getType().equals("pkt-type")) {
							continue;
						}
						if (e.getName().equals(name)) {
							if (num > 3 || e.getValue().isEmpty()) {
								e.setValue(Integer.toString(val));
								num++;
							}
						}
					}
					
				}
			}
		}
	}

	private boolean isField(String header) {
		header = header.trim();

		if (header.length() > 3) {
			if (Character.isDigit(header.codePointAt(0)) && !allHeadersNumbered) {
				return false;
			}
			if (!Character.isUpperCase(header.codePointAt(0)) && !allHeadersNumbered) {
				return false;
			}

			Sentence s = new Sentence(header);
			for (String w : s.words()) {
				if (wordInList(w.toLowerCase(), customFunctionWords)) {
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean isType(ProtocolSection me, ProtocolSection top) {
		String header = me.getTitle().text().trim();
		
		if (header.length() < 3) {
			return false;
		}
		
		Matcher m = topsection.matcher(header);
		if (!m.find()) {
			return false;
		}
		
		Sentence s = new Sentence(header);
		for (String w : s.words()) {
			if (wordInList(w.toLowerCase(), pktTypeDefs)) {
				me.setMeta("int:types-top", "found");
				return false;
			}
		}
		
		
		if (top != null && top.getMeta("int:types-top") != null) {	
			for (String w : s.words()) {
				if (wordInList(w.toLowerCase(), customFunctionWords)) {
					return false;
				}
				if (wordInList(w.toLowerCase(), genericTypes)) {
					return false;
				}
			}
			return true;
		}
		
		return false;
	}
	
	private String removeSectionNumbers(String str) {
		int i=0;
		while (i < str.toCharArray().length && str.toCharArray()[i] != ' ' &&
				(Character.isDigit(str.toCharArray()[i]) || str.toCharArray()[i]=='.')) {
			i++;
		}
		return str.substring(i);
	}
	
	private String typeName(String str) {
		str = typevaluematch.matcher(str).replaceAll("");
		str = str.replace("packets", "");
		str = str.replace("Packets", "");
		str = str.replace("packet", "");
		str = str.replace("Packet", "");
		return str.trim();
	}

	private String typeValue(String str) {
		Matcher m = typevaluematch.matcher(str);
		if (m.find()) {
			return m.group(1).trim();
		}
		return "";
	}
	
	private boolean wordInList(String word, String list[]) {
		String w = word.toLowerCase();
		for (int i = 0; i < list.length; i++) {
			if (w.equals(list[i])) {
				return true;
			}
		}
		return false;
	}

	private String processSpec(String spec, ProtocolSection sect) {
		if (spec.length() == 0 && sect.getText() != null) {
			spec = sect.sentence(0).text();
		}
		if (spec.length() == 0) {
			return "";
		}
		
		if (spec.contains("from left to right")) {
			sizehint = "1 bit";
		}

		if (spec.toLowerCase().contains("variable")) {
			return "variable";
		}

		Matcher m = bitwidth1.matcher(spec.toLowerCase());
		if (m.find()) {
			return m.group();
		}

		m = bitwidth2.matcher(spec.toLowerCase());
		if (m.find()) {
			return m.group();
		}
		
		m = bitwidth3.matcher(spec.toLowerCase());
		if (m.find()) {
			return m.group();
		}
		
		if (sizehint != null) {
			return sizehint;
		}
		return "";
	}

	private String processTitle(String entity) {
		if (entity.contains(":")) {
			entity = entity.split(":")[0];
		}
		return entity.trim();
	}

}
