package nlp.snake.entityextractors;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import nlp.snake.config.Config;
import nlp.snake.ent.ASCIIImage;
import nlp.snake.ent.ASCIITable;
import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;

public abstract class EntityExtractor {
	protected int entitynum = 0;

	public abstract List<Entity> processDocument(InputProtocol proto);

	public void evaluate(InputProtocol proto, PrintStream out) {
		List<Entity> discovered = proto.getDiscoveredEntities();
		List<Annotation> fields = new ArrayList<>();
		List<Annotation> widths = new ArrayList<>();
		List<Annotation> types = new ArrayList<>();
		List<Annotation> typevals = new ArrayList<>();
		List<Entity> truth = new ArrayList<>();

		for (ProtocolSection s : proto.getSections()) {
			collectAnnotation(s, fields, "fieldname",true);
			collectAnnotation(s, widths, "fieldwidth",true);
			collectAnnotation(s, types, "pkt_type",true);
			collectAnnotation(s, typevals, "typeval",true);
		}

		/* Construct Truth Entities */
		for (Annotation f : fields) {
			for (Integer fid : f.getRefs()) {
				Entity e = new Entity(entitynum++);
				e.setType("field");
				e.setName(f.getExpression());
				e.setRealId(fid);

				boolean found = false;
				for (Annotation w : widths) {
					for (Integer wid : w.getRefs()) {
						if (fid.equals(wid)) {
							found = true;
							e.setValue(w.getExpression());
							break;
						}
					}
					if (found) {
						break;
					}
				}

				if (Config.entity_dofields) {
					truth.add(e);
				}
			}

		}
		for (Annotation f : types) {
			for (Integer fid : f.getRefs()) {
				Entity e = new Entity(entitynum++);
				e.setType("pkt-type");
				e.setName(f.getExpression());
				e.setRealId(fid);

				boolean found = false;
				for (Annotation w : typevals) {
					for (Integer wid : w.getRefs()) {
						if (fid.equals(wid)) {
							found = true;
							e.setValue(w.getExpression());
							break;
						}
					}
					if (found) {
						break;
					}
				}

				if (Config.entity_dotypes) {
					truth.add(e);
				}
			}

		}

		int TruePositives = 0;
		int FalsePositives = 0;
		int FalseNegatives = 0;
		boolean found = false;

		/* Calculate True/False Positives/Negatives */
		for (Entity e : discovered) {
			if (e.getType().equals("null")) {
				continue;
			}
			found = false;
			for (Entity t : truth) {
				if (Config.eval_match_values) {
					if (e.getRealId() == t.getRealId() && e.getValue().equals(t.getValue())) {
						TruePositives++;
						found = true;
						break;
					}
				} else {
					if (e.getRealId() == t.getRealId()) {
						TruePositives++;
						found = true;
						break;
					}
				}
			}

			if (!found) {
				FalsePositives++;
				if (Config.debug > 0) {
					out.println("FP: " + e.toString());
				}
			}
		}
		for (Entity t : truth) {
			found = false;
			for (Entity e : discovered) {
				if (Config.eval_match_values) {
					if (e.getRealId() == t.getRealId() && e.getValue().equals(t.getValue())) {
						found = true;
						break;
					}
				} else {
					if (e.getRealId() == t.getRealId()) {
						found = true;
						break;
					}
				}
			}

			if (!found) {
				FalseNegatives++;
				if (Config.debug > 0) {
					out.println("FN: " + t.toString());
				}
			}
		}

		/* Output */
		out.println("########################");
		out.println(proto.getTitle());
		out.println("Entity Extraction Evaluation");
		out.println("------------------------");
		out.println("True Positives: " + TruePositives);
		out.println("False Positives: " + FalsePositives);
		out.println("False Negatives: " + FalseNegatives);
		out.println("------------------------");
		out.println("Recall: " + recall(TruePositives, FalseNegatives));
		out.println("Precision: " + precision(TruePositives, FalsePositives));
		out.println("F-score: " + fscore(TruePositives, FalsePositives, FalseNegatives));
		out.println("########################");
	}

	private double recall(int truePositive, int falseNegative) {
		return truePositive / ((truePositive + falseNegative) * 1.0);
	}

	private double precision(int truePositive, int falsePositive) {
		return truePositive / ((truePositive + falsePositive) * 1.0);
	}

	private double fscore(int truePositive, int falsePositive, int falseNegative) {
		double p = precision(truePositive, falsePositive);
		double r = recall(truePositive, falseNegative);
		return 2 * ((p * r) / (p + r));
	}

	protected void collectAnnotation(ProtocolSection sect, List<Annotation> list, String type, boolean recurse) {
		list.addAll(sect.getTitleAnnotations(type));
		list.addAll(sect.getTextAnnotations(type));
		for (ASCIIImage img: sect.getImages()) {
			list.addAll(img.getAnnotations(type));
		}
		for (ASCIITable tbl: sect.getTables()) {
			list.addAll(tbl.getAnnotations(type));
		}

		if (recurse) {
			for (ProtocolSection s : sect.getSubsections()) {
				collectAnnotation(s, list, type, recurse);
			}
		}
		return;
	}
}
