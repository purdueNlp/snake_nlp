package nlp.snake.entityextractors;

import java.util.ArrayList;
import java.util.List;

import nlp.snake.config.Config;
import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Sentence;

public class EntitiesFromAnnotation extends EntityExtractor {
	private List<Entity> entities = new ArrayList<>();

	public EntitiesFromAnnotation() {
	}

	@Override
	public List<Entity> processDocument(InputProtocol proto) {
		entities.clear();

		System.out.println("##### FindEntities Doc: " + proto.getTitle());
		for (ProtocolSection s : proto.getSections()) {
			lookForEntities(s);
		}
		for (ProtocolSection s: proto.getSections()) {
			fillEntityValues(s);
		}

		if (Config.debug > 1) {
			for (Entity e : entities) {
				System.err.println("Entity: " + e.toString());
			}
		}
		if (Config.debug > 0) {
			System.err.println("Annotation Entities: " + entities.size());
		}
		
		//Add null entity
		entities.add(new Entity());

		proto.addEntities(entities,true);
		return entities;
	}

	private void lookForEntities(ProtocolSection sect) {
		System.out.println("##### FindEntities Section: " + sect.getTitle());
		Sentence title = sect.getTitle();
		ArrayList<Annotation> ann = new ArrayList<>();
		
		if (Config.entity_dofields) {
			collectAnnotation(sect,ann,"fieldname",false);
			for (Annotation a : ann) {
				for (Integer i : a.getRefs()) {
					String val = getFieldValue(sect, i);
					Entity e = new Entity(entitynum++, a.getExpression(),
							processTitle((title == null) ? "" : title.text()), "field", val, i);
					if (!entities.contains(e)) {
						entities.add(e);
					}
				}
			}
		}
		
		ann.clear();
		if (Config.entity_dotypes) {
			collectAnnotation(sect,ann,"pkt_type",false);
			for (Annotation a : ann) {
				for (Integer i : a.getRefs()) {
					String val = getTypeValue(sect, i);
					Entity e = new Entity(entitynum++, a.getExpression(),
							processTitle(title.text()), "pkt-type", val, i);
					if (!entities.contains(e)) {
						entities.add(e);
					}
				}
			}
		}

		for (ProtocolSection s : sect.getSubsections()) {
			lookForEntities(s);
		}
		return;
	}
	
	private void fillEntityValues(ProtocolSection sect) {
		ArrayList<Annotation> ann = new ArrayList<>();
		
		if (Config.entity_dotypes) {
			for(Entity e: entities) {
				if (e.getType().equals("pkt-type")) {
					collectAnnotation(sect,ann,"typeval",false);
					for (Annotation a: ann) {
						for (Integer i:a.getRefs()) {
							if (e.getRealId()==i) {
								e.setValue(a.getExpression());
								break;
							}
						}
					}
				}
			}
		}
		for (ProtocolSection s : sect.getSubsections()) {
			fillEntityValues(s);
		}
		return;
	}

	private String getFieldValue(ProtocolSection sect, Integer id) {
		ArrayList<Annotation> ann = new ArrayList<>();
		collectAnnotation(sect,ann,"fieldwidth",true);
		for (Annotation a : ann) {
			for (Integer i : a.getRefs()) {
				if (i.equals(id)) {
					return a.getExpression();
				}
			}
		}
		return "";
	}
	
	private String getTypeValue(ProtocolSection sect, Integer id) {
		ArrayList<Annotation> ann = new ArrayList<>();
		collectAnnotation(sect,ann,"typeval",true);
		for (Annotation a : ann) {
			for (Integer i : a.getRefs()) {
				if (i.equals(id)) {
					return a.getExpression();
				}
			}
		}
		return "";
	}

	private String processTitle(String entity) {
		if (entity.contains(":")) {
			entity = entity.split(":")[0];
		}
		return entity.trim();
	}

}
