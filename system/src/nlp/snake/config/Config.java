package nlp.snake.config;

public class Config {
	public static int debug = 1;
	
	//Preprocessing
	public static final boolean shortLinesAreSection = true;
	
	//Entity Extraction
	public static final boolean entity_dofields = true;
	public static final boolean entity_dotypes = true;
	public static final boolean only_types_with_values = true;
	
	//Mention Identification
	public static final boolean mention_dofields = true;
	public static final boolean mention_dotypes = true;
    public static final boolean mention_donames = true;
	
	//Evaluation
	public static final boolean eval_match_values = false;
	public static final boolean eval_rule_based_baseline = false;
	public static boolean eval_gold_entities = false;
}
