package nlp.snake.learn;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.InputProtocol;

public class NullLearner extends Learner {
	
	@Override
	public void train(List<Map<Integer, Integer>> features) {
	}

	@Override
	public void classify(List<Map<Integer, Integer>> features) {
	}

	@Override
	public void applyToDocument(InputProtocol proto) {
	}

	@Override
	public void evaluate(PrintStream out) {
	}

	@Override
	public void clear() {
	}

}
