package nlp.snake.learn;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;

import nlp.snake.ent.InputProtocol;

public abstract class Learner {
	protected static final int TRUTH_ID = 0;
	protected static final int REAL_ID = 1;
	protected static final int FIRST_ID = 2;
	protected static final int ENTITY_ID = 3;
	protected static final int CHUNK_ID = 4;
	protected static final int IS_MENTION_ID = 5;
	protected static final int FEATURES_START_AT = 10;
	protected static final int POSITIVE_EXAMPLE = 1;
	protected static final int NEGATIVE_EXAMPLE = -1;
	protected static final int UNKNOWN_EXAMPLE = 0;
	
	public abstract void train(List<Map<Integer, Integer>> features);

	public abstract void classify(List<Map<Integer, Integer>> features);

	public abstract void applyToDocument(InputProtocol proto);

	public abstract void evaluate(PrintStream out);

	public abstract void clear();

	protected double accuracy(int truePositive, int trueNegative, int total) {
		return (truePositive + trueNegative) / (total * 1.0);
	}

	protected double recall(int truePositive, int falseNegative) {
		return truePositive / ((truePositive + falseNegative) * 1.0);
	}

	protected double precision(int truePositive, int falsePositive) {
		return truePositive / ((truePositive + falsePositive) * 1.0);
	}

	protected double fscore(int truePositive, int falsePositive, int falseNegative) {
		double p = precision(truePositive, falsePositive);
		double r = recall(truePositive, falseNegative);
		return 2 * ((p * r) / (p + r));
	}
}
