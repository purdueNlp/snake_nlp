package nlp.snake.learn;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import nlp.snake.config.Config;
import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;
import nlp.snake.io.svmLightWrapper;

public class svmLightLearner extends Learner {
	svmLightWrapper svm = null;
	List<Map<Integer, Integer>> features = null;
	List<Boolean> predictions = null;

	public svmLightLearner(String bindir, String tmpdir, String model) {
		svm = new svmLightWrapper(bindir, tmpdir, model);
	}
	
	@Override
	public void train(List<Map<Integer, Integer>> features) {
		this.predictions = null;
		this.features = features;
		
		svm.train(features);
	}

	@Override
	public void classify(List<Map<Integer, Integer>> features) {
		this.predictions = null;
		this.features = features;

		predictions = svm.classify(this.features);

		if (predictions.size() != this.features.size()) {
			System.err.println("Error: Predictions.size() != Features.size()");
		}
	}

	@Override
	public void applyToDocument(InputProtocol proto) {
		if (features == null || predictions == null || proto == null) {
			return;
		}

		if (predictions.size() != this.features.size()) {
			return;
		}

		Iterator<Boolean> p = predictions.iterator();
		Iterator<Map<Integer, Integer>> f = features.iterator();

		for (ProtocolSection s : proto.getSections()) {
			applyToSection(s, p, f, proto.getEntities());
		}
	}

	private void applyToSection(ProtocolSection sect, Iterator<Boolean> p,
			Iterator<Map<Integer, Integer>> f, List<Entity> entities) {
		List<Annotation> annts = new LinkedList<>();

		for (Annotation c : sect.getChunks()) {
			for (Entity e : entities) {
				if (Config.mention_dofields && e.getType().equals("field")) {
					//pass through
				} else if (Config.mention_dotypes && e.getType().equals("pkt-type")) {
					//pass through
				} else if (e.getType().equals("null")) {
					//pass through
				} else {
					continue;
				}
				
				if (p.hasNext() && f.hasNext()) {
					Boolean pred = p.next();
					Map<Integer, Integer> feat = f.next();
					if (Config.debug > 0 && e.getId() != feat.get(ENTITY_ID)) {
						System.err.println("Error: Can't apply predictions to chunks-1");
						return;
					}
					if (pred) {
						Annotation m = new Annotation(c);
						m.clearRefs();
						m.clearRefGroups();
						m.setType("int:entityreference");
						m.setId(feat.get(ENTITY_ID).toString());
						m.addRef(feat.get(ENTITY_ID));
						annts.add(m);
					}
				} else if (Config.debug > 0) {
					System.err.println("Error: Can't apply predictions to chunks-2");
				}
			}
		}
		if (annts.size() > 0) {
			sect.addTextAnnotations(annts);
		}

		for (ProtocolSection s : sect.getSubsections()) {
			applyToSection(s, p, f, entities);
		}
		return;
	}

	@Override
	public void evaluate(PrintStream out) {
		if (features == null || predictions == null) {
			return;
		}

		if (predictions.size() != this.features.size()) {
			return;
		}

		Iterator<Boolean> p = predictions.iterator();
		Iterator<Map<Integer, Integer>> f = features.iterator();

		int TruePositives = 0;
		int TrueNegatives = 0;
		int FalsePositives = 0;
		int FalseNegatives = 0;
		while (p.hasNext() && f.hasNext()) {
			Boolean pred = p.next();
			Map<Integer,Integer> feat = f.next();
			int truth = feat.get(TRUTH_ID);
			int othermention = feat.getOrDefault(IS_MENTION_ID, 0);
			
			int num = 1 + othermention;
			if (truth > 0 || othermention > 0) {
				if (pred && truth > 0) {
					TruePositives++;
				} else {
					if (othermention > 0) {
						FalseNegatives+=othermention;
					} else {
						FalseNegatives++;
					}
				}
			} else {
				if (pred) {
					FalsePositives++;
				} else {
					TrueNegatives++;
				}
			}
		}

		/* Output */
		out.println("########################");
		out.println("Entity Reference Detection Evaluation");
		out.println("------------------------");
		out.println("True Positives: " + TruePositives);
		out.println("True Negatives: " + TrueNegatives);
		out.println("False Positives: " + FalsePositives);
		out.println("False Negatives: " + FalseNegatives);
		out.println("------------------------");
		out.println("Accuracy: "
				+ accuracy(TruePositives, TrueNegatives, TruePositives + TrueNegatives
						+ FalsePositives + FalseNegatives));
		out.println("Recall: " + recall(TruePositives, FalseNegatives));
		out.println("Precision: " + precision(TruePositives, FalsePositives));
		out.println("F-score: " + fscore(TruePositives, FalsePositives, FalseNegatives));
		out.println("########################");
	}

	@Override
	public void clear() {
		svm.cleanup();
		features = null;
		predictions = null;
	}

}
