package nlp.snake.docprocessors;

import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import nlp.snake.config.Config;
import nlp.snake.ent.ASCIIImage;
import nlp.snake.ent.ASCIITable;
import nlp.snake.ent.Annotation;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;

public class AnnotationStatistics extends DocProcessor {
	SortedMap<String, Integer> stats = new TreeMap<>();
	int total_entities = 0;
	int total_entity_references = 0;
	int total_attributes = 0;
	int total_relations = 0;

	public AnnotationStatistics() {
	}

	@Override
	public void processDocument(InputProtocol proto) {
		stats.clear();

		System.out.println("##### Process Doc: " + proto.getTitle());
		for (ProtocolSection s : proto.getSections()) {
			processSection(s);
		}

		printStats();

		return;
	}

	private void processSection(ProtocolSection sect) {
		System.out.println("##### Process Section: " + sect.getTitle());

		for (Annotation a : sect.getTitleAnnotations()) {
			Integer val = stats.getOrDefault(a.getType(), 0);
			val += 1;
			stats.put(a.getType(), val);
			add_to_totals(a.getType());
		}

		for (Annotation a : sect.getTextAnnotations()) {
			Integer val = stats.getOrDefault(a.getType(), 0);
			val += 1;
			stats.put(a.getType(), val);
			add_to_totals(a.getType());
		}
		
		for (ASCIITable t: sect.getTables()) {
			for (Annotation a: t.getAnnotations()) {
				Integer val = stats.getOrDefault(a.getType(), 0);
				val += 1;
				stats.put(a.getType(), val);
				add_to_totals(a.getType());
			}
		}
		
		for (ASCIIImage i: sect.getImages()) {
			for (Annotation a: i.getAnnotations()) {
				Integer val = stats.getOrDefault(a.getType(), 0);
				val += 1;
				stats.put(a.getType(), val);
				add_to_totals(a.getType());
			}
		}

		for (ProtocolSection s : sect.getSubsections()) {
			processSection(s);
		}
	}

	private void printStats() {
		System.out.println("##### Stats:");
		for (Entry<String, Integer> e : stats.entrySet()) {
			System.out.println(e.getKey() + ": " + e.getValue().toString());
		}
	}
	
	private void add_to_totals(String type) {
		if (type.equals("fieldname") && Config.entity_dofields) {
			total_entities++;
		} else if (type.equals("pkt_type") && Config.entity_dotypes) {
			total_entities++;
		} else if (type.equals("fieldref") && Config.entity_dofields) {
			total_entity_references++;
		} else if (type.equals("typeref") && Config.entity_dotypes) {
			total_entity_references++;
		}else if (type.equals("r_contains")) {
			total_relations++;
		}else if (type.equals("r_field_present")) {
			total_relations++;
		}else if (type.equals("r_significant")) {
			total_relations++;
		}else if (type.equals("r_offset")) {
			total_relations++;
		}else if (type.equals("r_greater")) {
			total_relations++;
		}else if (type.equals("r_less")) {
			total_relations++;
		}else if (type.equals("r_range")) {
			total_relations++;
		}else if (type.equals("r_sequence_number")) {
			total_attributes++;
		}else if (type.equals("r_checksum")) {
			total_attributes++;
		}else if (type.equals("r_port")) {
			total_attributes++;
		}else if (type.equals("r_packet_type")) {
			total_attributes++;
		}else if (type.equals("r_header_length")) {
			total_attributes++;
		}else if (type.equals("r_multiple")) {
			total_attributes++;
		}else if (type.equals("r_mbz")) {
			total_attributes++;
		}else if (type.equals("r_monotonically_inc")) {
			total_attributes++;
		}
	}

	@Override
	public void finish() {
		System.out.println("##### Stats:");
		System.out.println("Total Entities: " + total_entities);
		System.out.println("Total Entity References: " + total_entity_references);
		System.out.println("Total Attributes: " + total_attributes);
		System.out.println("Total Relations: " + total_relations);
	}

}
