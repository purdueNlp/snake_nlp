package nlp.snake.docprocessors;

import java.util.ArrayList;
import java.util.List;

import nlp.snake.ent.Annotation;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.ling.CoreAnnotations.BeginIndexAnnotation;
import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

public class SplitFields extends DocProcessor {
	private int num_fields = 0;
	private int num_split_fields = 0;

	public void processDocument(InputProtocol proto) {
		for (ProtocolSection s : proto.getSections()) {
			processSection(s);
		}

		System.out.println("Total Fields: " + num_fields);
		System.out.println("Split Fields: " + num_split_fields);
		System.out.println("Percent Split: " + (num_split_fields / (num_fields * 1.0)) * 100.0
				+ "%");
		return;
	}

	private void processSection(ProtocolSection sect) {
		System.out.println("##### New Section (" + sect.getTitle() + ")");
		Document d = sect.getText();

		if (d != null && d.text().length() > 0) {
			findSplitFields(sect);
		}

		for (ProtocolSection s : sect.getSubsections()) {
			processSection(s);
		}
	}

	private void findSplitFields(ProtocolSection sect) {
		for (Annotation a : sect.getTitleAnnotations()) {
			if (!a.getType().equals("fieldref") && !a.getType().equals("fieldname")
					&& !a.getType().equals("fieldwidth") && !a.getType().equals("typeval")
					&& !a.getType().equals("pkt_type") && !a.getType().equals("typeref")) {
				continue;
			}
			num_fields++;
			List<String> chunks = chunkwords(sect.getTitle());
			String c = chunks.get(a.getBeginWordIndex());
			for (int i = a.getBeginWordIndex(); i <= a.getEndWordIndex(); i++) {
				if (!c.equals(chunks.get(i))) {
					num_split_fields++;
					System.out.println(a.getExpression());
				}
			}
		}

		for (Annotation a : sect.getTextAnnotations()) {
			if (!a.getType().equals("fieldref") && !a.getType().equals("fieldname")
					&& !a.getType().equals("fieldwidth") && !a.getType().equals("typeval")
					&& !a.getType().equals("pkt_type") && !a.getType().equals("typeref")) {
				continue;
			}
			if (a.getBeginSentIndex() != a.getEndSentIndex()) {
				System.out.println("Annotation spanning sentences");
				continue;
			}
			num_fields++;
			List<String> chunks = chunkwords(sect.sentence(a.getBeginSentIndex()));
			String c = chunks.get(a.getBeginWordIndex());
			for (int i = a.getBeginWordIndex(); i <= a.getEndWordIndex(); i++) {
				if (!c.equals(chunks.get(i))) {
					num_split_fields++;
					System.out.println(a.getExpression());
				}
			}
		}
	}

	private List<String> chunkwords(Sentence s) {
		List<String> tags = new ArrayList<String>();
		for (int i = 0; i < s.words().size(); i++) {
			tags.add("X");
		}

		Tree parse = s.parse();
		parse.indexSpans();
		for (Tree l : parse.getLeaves()) {
			Tree p2 = l.ancestor(2, parse);
			if (p2 != null) {
				if ("S".equals(p2.label().value())) {
					continue;
				}

				CoreMap c = (CoreMap) l.label();
				int begin = c.get(BeginIndexAnnotation.class);
				if (l.value().equals(",")) {
					tags.set(begin, "X");
				} else {
					tags.set(begin, p2.label().value());
				}
			}
		}

		return tags;
	}

	private void printChunks(Sentence s) {
		// System.out.println(s.text().replace("\n", " "));

		List<String> labels = chunkwords(s);

		List<String> words = new ArrayList<String>();
		String lastLabel = "";
		int i = 0;
		int lasti = -1;
		boolean inPhrase = false;
		for (String w : s.words()) {
			String label = labels.get(i);
			if (!lastLabel.equals(label)) {

				if (lasti >= 0 && !lastLabel.equals("X")) {
					String c = words.get(lasti);
					c = c + "]";
					words.set(lasti, c);
					inPhrase = false;
				}

				if (!label.equals("X")) {
					w = "[(" + label + ")" + w;
					inPhrase = true;
				}

			}
			lastLabel = label;
			lasti = i;
			i++;
			words.add(w);
		}

		if (inPhrase) {
			String w = words.get(words.size() - 1);
			w = w + "]";
			words.set(words.size() - 1, w);
		}

		String o = "";
		for (String w : words) {
			o += w + " ";
		}
		System.out.println(o);
	}

	@Override
	public void finish() {
	}
}
