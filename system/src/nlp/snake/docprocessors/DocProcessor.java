package nlp.snake.docprocessors;

import nlp.snake.ent.InputProtocol;

public abstract class DocProcessor {

	public abstract void processDocument(InputProtocol proto);
	
	public abstract void finish();
}
