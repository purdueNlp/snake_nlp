package nlp.snake.featurizers;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.InputProtocol;

public abstract class Featurizer {
	protected static final int TRUTH_ID = 0;
	protected static final int REAL_ID = 1;
	protected static final int FIRST_ID = 2;
	protected static final int ENTITY_ID = 3;
	protected static final int CHUNK_ID = 4;
	protected static final int IS_MENTION_ID = 5;
	protected static final int FEATURES_START_AT = 10;
	protected static final int POSITIVE_EXAMPLE = 1;
	protected static final int NEGATIVE_EXAMPLE = -1;
	protected static final int UNKNOWN_EXAMPLE = 0;

	public abstract List<Map<Integer, Integer>> processDocument(InputProtocol proto);

	public abstract List<String> getDebugOutput();
}
