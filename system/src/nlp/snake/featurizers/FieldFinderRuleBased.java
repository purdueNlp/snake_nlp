package nlp.snake.featurizers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nlp.snake.config.Config;
import nlp.snake.ent.Annotation;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;
import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.util.Characters;

public class FieldFinderRuleBased extends Featurizer {
	protected static final String functionWords[] = { "of", "at", "in", "without", "between", "he",
			"she", "they", "it", "the", "a", "an", "that", "my", "more", "much", "neither",
			"either", "and", "that", "which", "when", "while", "although", "or", "be", "is", "am",
			"are", "have", "got", "do", "no", "not", "nor", "as" };
	protected static final String commonWords[] = { "to", "from", "each" };
	protected static final String rfcWords[] = { "section", "sections", "bits", "bit", "byte",
			"bytes", "packet", "packets", "header" };
	private int tp = 0;
	private int fp = 0;
	private int tn = 0;
	private int fn = 0;
	private List<String> headerWords = new ArrayList<>();

	public List<Map<Integer, Integer>> processDocument(InputProtocol proto) {
		tp = fp = tn = fn = 0;

		System.out.println("################### Process1:");
		for (ProtocolSection s : proto.getSections()) {
			processPass1Section(s);
		}

		System.out.println("################### Process2:");
		for (ProtocolSection s : proto.getSections()) {
			processPass2Section(s);
		}

		System.out.println("################### Evaluation:");
		for (ProtocolSection s : proto.getSections()) {
			evaluateSection(s);
		}

		double acc = accuracy(tp, tn, (tp + fp + tn + fn));
		double f = fscore(tp, fp, fn);

		System.out.println("################### Results:");
		System.out.println("Accuracy: " + acc);
		System.out.println("F-score: " + f);
		return null;
	}

	public List<String> getDebugOutput() {
		return null;
	}

	private void processPass1Section(ProtocolSection sect) {
		System.out.println("##### New Section (" + sect.getTitle() + ")");
		Document d = sect.getText();

		if (d != null) {
			populateWordsInHeader(sect);
		}

		for (ProtocolSection s : sect.getSubsections()) {
			processPass1Section(s);
		}
	}

	private void processPass2Section(ProtocolSection sect) {
		System.out.println("##### New Section (" + sect.getTitle() + ")");
		Document d = sect.getText();

		if (d != null) {
			lookForWordsInHeader(sect);
			// capitialization(sect);
			// frequentWords(sect);
		}

		for (ProtocolSection s : sect.getSubsections()) {
			processPass2Section(s);
		}
	}

	private void evaluateSection(ProtocolSection sect) {
		System.out.println("##### New Section (" + sect.getTitle() + ")");
		Document d = sect.getText();

		if (d != null) {
			evaluation(sect);
		}

		for (ProtocolSection s : sect.getSubsections()) {
			evaluateSection(s);
		}
	}

	private void capitialization(ProtocolSection sect) {
		/* Look at each sentence */
		for (int i = 0; i < sect.sentences().size(); i++) {
			Sentence s = sect.sentence(i);
			/* Each chunk of the sentence */
			for (Annotation chunk : sect.getChunks(i)) {
				/* Each word in the chunk */
				for (int j = chunk.getBeginWordIndex(); j < chunk.getEndWordIndex(); j++) {
					if (j == 0) {
						/* Skip first word of sentence */
						continue;
					}
					String word = s.originalText(j);

					if (wordInList(word, rfcWords) || wordInList(word, functionWords)
							|| wordInList(word, commonWords)) {
						continue;
					}
					/*
					 * Check for initial capitial and total number of capitals
					 * in word
					 */
					int upper = 0;
					for (int c = 0; c < word.length(); c++) {
						if (Character.isUpperCase(word.charAt(c))) {
							upper++;
						}
					}
					if (Character.isUpperCase(word.charAt(0)) && upper == 1) {
						/* Chunk has a capital word, add it */
						Annotation a = new Annotation(chunk);
						a.setType("int:entityreference");
						sect.addTextAnnotation(a);
					}
				}
			}
		}
	}

	private void frequentWords(ProtocolSection sect) {
		Map<String, Integer> numMap = new HashMap<>();

		for (Sentence s : sect.getText().sentences()) {
			for (String w : s.originalTexts()) {
				if (wordInList(w, functionWords) || wordInList(w, commonWords)
						|| wordInList(w, rfcWords)) {
					continue;
				}

				if (w.length() == 1 && Characters.isPunctuation(w.charAt(0))) {
					continue;
				}

				Integer num = numMap.getOrDefault(w, 0) + 1;
				numMap.put(w, num);
			}
		}

		List<Entry<String, Integer>> list = new ArrayList<>(numMap.entrySet());
		Collections.sort(list, new Comparator<Object>() {
			@SuppressWarnings("unchecked")
			public int compare(Object o1, Object o2) {
				return ((Comparable<Integer>) ((Map.Entry<String, Integer>) (o2)).getValue())
						.compareTo(((Map.Entry<String, Integer>) (o1)).getValue());
			}
		});

		for (Entry<String, Integer> e : list) {
			System.out.println(e.getKey() + "   " + e.getValue());
		}
	}

	private void populateWordsInHeader(ProtocolSection sect) {
		if (sect.getTitle() == null) {
			return;
		}
		headerWords.addAll(nonFunctionWords(sect.getTitle()));
	}

	private void lookForWordsInHeader(ProtocolSection sect) {
		boolean found;

		for (Annotation c : sect.getChunks()) {
			found = false;
			Sentence s = sect.getText().sentence(c.getBeginSentIndex());

			for (int i = c.getBeginWordIndex(); i <= c.getEndWordIndex(); i++) {
				String w = s.originalText(i);
				if (headerWords.contains(w)) {
					found = true;
					break;
				}
			}

			if (found) {
				Annotation a = new Annotation(c);
				a.setType("out:field");
				sect.addTextAnnotation(a);
			}
		}
	}

	private List<String> nonFunctionWords(Sentence s) {
		List<String> nfw = new ArrayList<>();

		for (String w : s.originalTexts()) {
			boolean found = false;
			String tmp = w.toLowerCase();

			/* Preset list of words */
			if (wordInList(tmp, functionWords) || wordInList(tmp, commonWords)
					|| wordInList(tmp, rfcWords)) {
				found = true;
			}

			/* Contains punctuation or numbers */
			for (int i = 0; i < tmp.length(); i++) {
				if (Character.isDigit(tmp.charAt(i))) {
					found = true;
					break;
				}
			}

			if (tmp.length() == 1 && Characters.isPunctuation(tmp.charAt(0))) {
				found = true;
			}

			if (!found) {
				nfw.add(w);
			}
		}
		return nfw;
	}

	private boolean wordInList(String word, String list[]) {
		String w = word.toLowerCase();
		for (int i = 0; i < list.length; i++) {
			if (w.equals(list[i])) {
				return true;
			}
		}
		return false;
	}

	private void evaluation(ProtocolSection sect) {
		List<Annotation> truth = new ArrayList<>();
		List<Annotation> result = new ArrayList<>();
		if (Config.mention_dofields) {
			truth.addAll(sect.getTextAnnotations("fieldref"));
		}
		if (Config.mention_dotypes) {
			truth.addAll(sect.getTextAnnotations("typeref"));
		}
		result = sect.getTextAnnotations("out:entityreference");
		List<Annotation> chunks = sect.getTextAnnotations("int:chunk");
		int myfp = 0;
		int mytp = 0;

		boolean found;
		for (Annotation t : truth) {
			found = false;
			for (Annotation r : result) {
				if (r.overlapping(t)) {
					System.out.println("TP: " + r.getExpression());
					tp++;
					mytp++;
					found = true;
					break;
				}
			}
			if (!found) {
				System.out.println("FN: " + t.getExpression());
				fn++;
			}
		}

		for (Annotation r : result) {
			found = false;
			for (Annotation t : truth) {
				if (r.overlapping(t)) {
					found = true;
					break;
				}
			}
			if (!found) {
				System.out.println("FP: " + r.getExpression());
				fp++;
				myfp++;
			}
		}

		tn += (chunks.size() - myfp - mytp);
	}

	private double accuracy(int truePositive, int trueNegative, int total) {
		return (truePositive + trueNegative) / (total * 1.0);
	}

	private double recall(int truePositive, int falseNegative) {
		return truePositive / ((truePositive + falseNegative) * 1.0);
	}

	private double precision(int truePositive, int falsePositive) {
		return truePositive / ((truePositive + falsePositive) * 1.0);
	}

	private double fscore(int truePositive, int falsePositive, int falseNegative) {
		double p = precision(truePositive, falsePositive);
		double r = recall(truePositive, falseNegative);
		return 2 * ((p * r) / (p + r));
	}
}
