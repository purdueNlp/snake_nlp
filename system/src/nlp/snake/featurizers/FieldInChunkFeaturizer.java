package nlp.snake.featurizers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import nlp.snake.config.Config;
import nlp.snake.ent.Annotation;
import nlp.snake.ent.Entity;
import nlp.snake.ent.InputProtocol;
import nlp.snake.ent.ProtocolSection;
import nlp.snake.features.AndFeatures;
import nlp.snake.features.Bias;
import nlp.snake.features.ChunkContainsNHeaderWordsInOrder;
import nlp.snake.features.ChunkIsPronoun;
import nlp.snake.features.ChunkSubsumesEntityAndNextWordPOS;
import nlp.snake.features.ChunkSubsumesEntityAndPriorWordPOS;
import nlp.snake.features.ChunkSubsumesEntityAndSpecialWords;
import nlp.snake.features.ChunkSubsumesPluralEntity;
import nlp.snake.features.EntityIsMultiWord;
import nlp.snake.features.Feature;
import nlp.snake.features.MetricChunkSubsumesEntity;
import nlp.snake.features.MetricInDefSection;
import nlp.snake.features.OneWordEntityInChunkAndNextWord;
import nlp.snake.features.OneWordEntityInChunkAndPriorWord;
import nlp.snake.features.OneWordEntityMatchesOneWordChunk;
import nlp.snake.features.OrFeatures;
import nlp.snake.features.ParensEntityMatchChunk;
import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;

public class FieldInChunkFeaturizer extends Featurizer {
	private List<Feature> features = new ArrayList<>();
	private List<Map<Integer, Integer>> output = new LinkedList<>();
	private List<String> details = new LinkedList<>();
	private List<Entity> entities = new ArrayList<>();
	private int nextfeature = FEATURES_START_AT;

	public FieldInChunkFeaturizer() {
		features.add(new Bias(nextfeature++));
		features.add(new ChunkContainsNHeaderWordsInOrder(nextfeature++, 2));
		features.add(new OrFeatures(nextfeature++,
				new ChunkSubsumesEntityAndSpecialWords(1, false), new ParensEntityMatchChunk(2)));
		features.add(new ChunkIsPronoun(nextfeature++));
		// starts at 14
		features.add(new AndFeatures(nextfeature++, new EntityIsMultiWord(1),
				new ChunkSubsumesEntityAndNextWordPOS(2, false, "CC")));
		features.add(new AndFeatures(nextfeature++, new EntityIsMultiWord(1),
				new ChunkSubsumesEntityAndNextWordPOS(2, false, "DT")));
		features.add(new AndFeatures(nextfeature++, new EntityIsMultiWord(1),
				new ChunkSubsumesEntityAndNextWordPOS(2, false, "NN")));
		// starts at 17
		features.add(new AndFeatures(nextfeature++, new EntityIsMultiWord(1),
				new ChunkSubsumesEntityAndPriorWordPOS(2, false, "CC")));
		features.add(new AndFeatures(nextfeature++, new EntityIsMultiWord(1),
				new ChunkSubsumesEntityAndPriorWordPOS(2, false, "DT")));
		features.add(new AndFeatures(nextfeature++, new EntityIsMultiWord(1),
				new ChunkSubsumesEntityAndPriorWordPOS(2, false, "JJ")));
		features.add(new AndFeatures(nextfeature++, new EntityIsMultiWord(1),
				new ChunkSubsumesEntityAndPriorWordPOS(2, false, "NN")));
		features.add(new AndFeatures(nextfeature++, new EntityIsMultiWord(1),
				new ChunkSubsumesEntityAndPriorWordPOS(2, false, "PR")));
		// starts at 22
		List<String> priorWords = new ArrayList<String>();
		priorWords.add("all");
		List<String> nextWords = new ArrayList<String>();
		nextWords.add("list");
		nextWords.add("field");
		nextWords.add("space");
		nextWords.add("area");
		features.add(new OrFeatures(nextfeature++, new OneWordEntityMatchesOneWordChunk(1),
				new OrFeatures(2, new OneWordEntityInChunkAndPriorWord(1, false, true, priorWords),
						new OneWordEntityInChunkAndNextWord(2, false, true, nextWords))));
		features.add(new ChunkSubsumesPluralEntity(nextfeature++, false));
		// starts at 24
		features.add(new MetricChunkSubsumesEntity(nextfeature++, false));
		features.add(new MetricChunkSubsumesEntity(nextfeature++, true));
		features.add(new MetricInDefSection(nextfeature++));
	}

	@Override
	public List<Map<Integer, Integer>> processDocument(InputProtocol proto) {
		output.clear();
		entities.clear();
		details.clear();

		entities = proto.getEntities();
		if (entities.size() == 0) {
			if (Config.debug > 1) {
				System.err.println("Warning: No Entities.");
			}
		}

		for (Feature f : features) {
			f.newDoc();
		}

		System.out.println("##### Process Doc: " + proto.getTitle());
		for (ProtocolSection s : proto.getSections()) {
			processSection(s);
		}

		return output;
	}

	@Override
	public List<String> getDebugOutput() {
		return new LinkedList<>(details);
	}

	public void printFeatures() {
		for (Feature f : features) {
			System.out.println("(" + f.getNum() + ")" + f.toString());
		}
	}

	private void processSection(ProtocolSection sect) {
		System.out.println("##### Process Section: " + sect.getTitle());
		Document d = sect.getText();

		if (d != null) {
			processChunks(sect);
		}

		for (ProtocolSection s : sect.getSubsections()) {
			processSection(s);
		}
	}

	private void processChunks(ProtocolSection sect) {
		Document d = sect.getText();

		/* Get ground truth annotations */
		List<Annotation> truth = new ArrayList<>();
		if (Config.mention_dofields) {
			truth.addAll(sect.getTextAnnotations("fieldref"));
		}
		if (Config.mention_dotypes) {
			truth.addAll(sect.getTextAnnotations("typeref"));
		}

		/* For each sentence */
		for (int s = 0; s < d.sentences().size(); s++) {
			Sentence sent = d.sentence(s);

			/* For each chunk */
			int chunknum = 0;
			for (Annotation c : sect.getChunks(s)) {

				/* Build String and list of words */
				List<String> cwords = new ArrayList<>();
				for (int i = c.getBeginWordIndex(); i <= c.getEndWordIndex(); i++) {
					cwords.add(sent.originalText(i));
				}

				/* For each entity */
				boolean newEntity = true;
				int mentions_in_chunk = 0;
				int was_found = 0;
				for (Annotation a : truth) {
					if (c.contains(a)) {
						mentions_in_chunk++;
					}
				}
				for (Entity e : entities) {
					if (Config.mention_dofields && e.getType().equals("field")) {
						//pass through
					} else if (Config.mention_dotypes && e.getType().equals("pkt-type")) {
						//pass through
					} else if (e.getType().equals("null")) {
						//pass through
					} else {
						continue;
					}
					/* Create new map for this entity,chunk */
					Map<Integer, Integer> values = new HashMap<>();
					values.put(CHUNK_ID, chunknum++);
					values.put(ENTITY_ID, e.getId());

					/* Apply Features */
					if (e.getName().length() > 0 || Config.eval_rule_based_baseline) {
						for (Feature f : features) {
							f.extract(e, c, cwords, sect, values);
						}
					}

					/* Determine truth for chunk, id pair */
					boolean found = false;
					
					/* Look for any truth annotation matching our entity */
					for (Annotation a : truth) {
						if (e.validId()) {
							if (c.contains(a)) {
								for (Integer type : a.getRefs()) {
									if (e.getRealId() == type) {
										found = true;
										was_found++;
										values.put(TRUTH_ID, POSITIVE_EXAMPLE);
										values.put(REAL_ID, type);
										break;
									}
								}
								if (found) {
									break;
								}
							}
						}
					}
					if (!found) {
						values.put(TRUTH_ID, NEGATIVE_EXAMPLE);
						values.put(REAL_ID, UNKNOWN_EXAMPLE);
						if (e.getType().equals("null") && mentions_in_chunk > was_found) {
							/* Add to null entity */
							values.put(IS_MENTION_ID, mentions_in_chunk-was_found);
						}
						if (newEntity) {
							values.put(FIRST_ID, POSITIVE_EXAMPLE);
							newEntity = false;
						}
					}

					/* Add this chunk to the output */
					output.add(values);
					details.add(c.getExpression() + "#####" + e.getName());
				}
			}
		}
	}
}
