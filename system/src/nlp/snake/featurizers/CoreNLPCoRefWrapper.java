package nlp.snake.featurizers;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.PropertiesUtils;

public class CoreNLPCoRefWrapper {
	static CoreNLPCoRefWrapper obj = null;
	StanfordCoreNLP pipeline = null;

	private CoreNLPCoRefWrapper() {
		pipeline = new StanfordCoreNLP(PropertiesUtils.asProperties("annotators",
				"tokenize,ssplit,pos,lemma,ner,parse,mention,coref"));
	}

	public static CoreNLPCoRefWrapper get() {
		if (obj == null) {
			obj = new CoreNLPCoRefWrapper();
		}
		return obj;
	}

	public StanfordCoreNLP pipeline() {
		return pipeline;
	}
}
