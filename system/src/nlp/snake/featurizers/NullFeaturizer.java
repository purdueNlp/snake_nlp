package nlp.snake.featurizers;

import java.util.List;
import java.util.Map;

import nlp.snake.ent.InputProtocol;

public class NullFeaturizer extends Featurizer {
	public NullFeaturizer() {
	}

	@Override
	public List<Map<Integer, Integer>> processDocument(InputProtocol proto) {
		return null;
	}

	@Override
	public List<String> getDebugOutput() {
		return null;
	}
}
