# snake_nlp

This repository contains the code for the paper [Leveraging Textual Specifications for Grammar-based Fuzzing of Network Protocols](https://ojs.aaai.org//index.php/AAAI/article/view/5002) by [Samuel Jero](https://www.sjero.net/), [Maria L Pacheco](https://mlpacheco.github.io/), [Dan Goldwasser](https://www.cs.purdue.edu/homes/dgoldwas/) and [Cristina Nita-Rotaru](https://www.ccs.neu.edu/home/crisn/), published in the 2019 Proceedings of the AAAI, IAAI Technical Track.

To cite our paper, please use the following

```
@article{Jero_Pacheco_Goldwasser_Nita-Rotaru_2019,
  title={Leveraging Textual Specifications for Grammar-Based Fuzzing of Network Protocols},
  volume={33},
  url={https://ojs.aaai.org/index.php/AAAI/article/view/5002},
  DOI={10.1609/aaai.v33i01.33019478},
  number={01},
  journal={Proceedings of the AAAI Conference on Artificial Intelligence},
  author={Jero, Samuel and Pacheco, Maria Leonor and Goldwasser, Dan and Nita-Rotaru, Cristina},
  year={2019},
  month={Jul.},
  pages={9478-9483} }
```

## Requirements

You will need `java 1.8` and `python3`. To install `python` requirements,
please do:
```
pip install -r requirements.txt
python
>>> import nltk
>>> nltk.download('omw-1.4')
```

The `java` requirements are included in `system/lib`

## Running our code

To run the full pipeline you can do:

```
./system/run.sh
```

This script consists of the following steps:

### Step 1: Preprocessing

In this step, we preprocess the RFCs and generate preprocessed JSON
files under `system/tmp/pre/`. It can be run directly by doing:

```
python system/run.py --preprocess Annotation/DCCP.annote.txt.short Annotation/TCP.annote.txt.short Annotation/IP.annote.txt.short Annotation/IPv6.annote.txt.short Annotation/GRE.annote.txt.short Annotation/SCTP.annote.txt.short
```

This script will output all intermediate results to `STDOUT`. Make sure
to redirect it to a file for persistence. 

### Step 2: Entity type extraction

In this step, we extract the entity types using the hierarchical
structure of the RFCs. The resulting entity types are stored under `system/tmp/ent/`. This step can be run directly by doing:

```
python system/run.py --entities system/tmp/pre/*.json

```

Note that this command expects the preprocessed files generated in the
previous step to be available
under `system/tmp/pre`

### Step 3: Entity mention identification

In this step, we identify entity mentions by using a binary classifier
that predicts whether an entity type is mentioned in a given text
chunk. Namely, we train a binary SVM over all pairs `(entity_type, text_chunk)`. We use a set of lexical and syntactic features. This step can be run directly by doing:

```
python $DIR/run.py --mentions $DIR/tmp/ent/*.json
```

Note that this command expects the entity files generated in the
previous steps to be available
under `system/tmp/ent`. If you want to use the ontology types, instead
of the extracted types, add a `-g` option as:

```
python $DIR/run.py -g --mentions $DIR/tmp/ent/*.json
```

To see the full set of extracted features, take a look at the files
under `system/src/nlp/snake/features/`. 

### Step 4: Property extraction

In this step, we identify mentions to properties by using a binary
classifier that predicts whether a chunk of text expresses a property.
This step can be run directly by running the following commands:

```
system/relationextract.sh -o system/tmp/ system/tmp/ent/*.json
mkdir -p system/tmp/rels
python system/classify_relns.py -r system/tmp/relations.json --unary --write
```

Note that this command expects the entity files generated in the
previous steps to be available
under `system/tmp/ent`. If you want to use the ontology types, instead
of the extracted types, add a `-g` option as:

```
system/relationextract.sh -g -o system/tmp/ system/tmp/ent/*.json
mkdir -p system/tmp/rels
python system/classify_relns.py -r system/tmp/relations.json --unary --write
```

### Step 5: Postprocessing

This step prepares the files for SNAKE, you can run this step by doig:

```
python system/run.py --formats system/tmp/rels/*_pred.csv
```

Note that this command expects the relation files generated in the
previous step to be available
under `system/tmp/rels`.


## Note

This code was recently adapted to work with `python3`, but only the
scripts used in the main pipeline were tested. Other files, for example,
those to test NLP baselines, have been kept in python.7 format. If you encounter something that does not work as expected, please feel free to
open a GitHub Issue reporting the problem, and we will do our best to
resolve it.

