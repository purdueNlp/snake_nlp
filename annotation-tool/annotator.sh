#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -Dswing.aatext=true -Dswing.plaf.metal.controlFont=Tahoma-15 -Dswing.plaf.metal.userFont=Tahoma-20 -cp $DIR/bin:$DIR/html-parser.jar edu.stanford.nlp.annotation.Annotator
