#!/bin/env python
# Samuel Jero <sjero@purdue.edu>
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import os
import sys
import argparse
import re
# NoneType not in python3
#from types import NoneType

debug = 0
NoneType = type(None)

class SnakeConverter:
    def __init__(self, is_transport=False, ipnum=None):
        #Protocol Metadata
        self.ipnum = ipnum
        self.is_transport = is_transport
        self.type_field = None
        #Input from NLP
        self.fields = []
        self.types = []
        self.attributes = []
        self.relationships = []
        self.sub_fields = []
        #Quick lookup for fields
        self.field_ids = dict()
        #Per-type packet formats with C-split fields
        self.pkts = dict()
        self.mask_fields = {}

        if self.is_transport and type(self.ipnum)is not int:
            print("Warning: No IP Protocol Number set!")
            self.ipnum = -1

    def load_entities(self, entity_file):
        self.fields = []
        self.types = []
        self.field_ids.clear()

        for line in entity_file:
            parts = line.split(",")
            if parts[0] == "field":
                f = self._parse_field(parts)
                if f is not None:
                    self.fields.append(f)
                    self.field_ids[f['id']] = f
            elif parts[0] == "pkt-type":
                t = self._parse_type(parts)
                if t is not None:
                    self.types.append(t)
            elif parts[0] == "null":
                continue
            else:
                print("Warning: Unhandled Entity: %s" % (line.strip()))

        if len(self.fields) <= 0:
            print("Warning: No packet fields!!")
        if len(self.types) <= 0:
            print("Warning: No packet types!!")

    def load_relations(self, rel_file):
        self.attributes = []

        for line in rel_file:
            if len(line) > 0 and line[0] == "#":
                continue
            rel = self._parse_relation(line)
            if rel is None:
                continue
            if rel['attribute'] == True:
                self.attributes.append(rel)
            if rel['relationship'] == True:
                self.relationships.append(rel)

        if len(self.attributes) <= 0:
            print("Warning: No Attributes!!")
        if len(self.relationships) <= 0:
            print("Warning: No Relationships!!")

    def process(self):
        self.pkts.clear()
        self._clean_fields()
        self._clean_types()

        self._fix_and_merge_attributes()
        self._fix_and_merge_relationships()
        self._build_masks()
        self._build_pkts()

        for k in self.pkts:
            self.pkts[k]['fields'],success = self._fix_fields(self.pkts[k]['fields'])
            if not success:
                return

    def set_type_field(self, field):
        self.type_field = field
        if self.type_field is not None:
            print("Note: Forcing type field to be \"%s\"" % (str(self.type_field)))

    def _parse_field(self, parts):
        field = dict()
        if len(parts) != 5:
            return None
        if parts[0] != "field":
            return None
        name = ""
        anid = -1
        idv = -1
        length = 0
       
        try:
            idv = int(parts[1])
        except Exception as e:
            print("Error: cannot convert ID (%s) to integer... \"%s\"" % (idv,line.strip()))
            return None
        try:
            anid = int(parts[2])
        except Exception as e:
            print("Error: cannot convert Annotation ID (%s) to integer... \"%s\"" % (anid,line.strip()))
            return None
        
        name = parts[3].strip()
        name = name.lower().split("(")[0].strip().replace(" ", "_")

        lstr = parts[4].strip()
        if lstr.find("variable") >=0:
            length = -1
        mo = re.search("([0-9]+) or ([0-9]+) bits?",lstr)
        if length == 0 and type(mo) is not NoneType:
            length = int(mo.group(1))
            field['altlength'] = mo.group(2)
            print("Warning: Changeable length fields are not really supported... Picking first length option (%d,%s)" % (length,name))
        mo = re.search("([0-9]+) bits?",lstr)
        if length == 0 and type(mo) is not NoneType:
            length = int(mo.group(1))
        mo = re.search("bit ([0-9]+)",lstr)
        if length == 0 and type(mo) is not NoneType:
            length = 1
        mo = re.search("bits ([0-9]+)-([0-9]+)", lstr)
        if length == 0 and type(mo) is not NoneType:
            length = int(mo.group(2)) - int(mo.group(1))
        mo = re.search("([0-9]+) octets?",lstr)
        if length == 0 and type(mo) is not NoneType:
            length = int(mo.group(1))*8

        field['name'] = name
        field['length'] = length
        field['id'] = idv
        field['anid'] = anid

        #Check uniquness
        found = True
        i = 2
        oldname = field['name']
        while found:
            found = False
            for f in self.fields:
                if f['name'] == field['name']:
                    found = True
                    field['name'] = oldname + str(i)
                    break
            i += 1

        return field
    
    def _parse_type(self, parts):
        ptype = dict()
        if len(parts) != 5:
            return None
        if parts[0] != "pkt-type":
            return None
        name = ""
        value = -1
        idv = -1
        anid = -1
       
        try:
            idv = int(parts[1])
        except Exception as e:
            print("Error: cannot convert ID (%s) to integer... \"%s\"" % (idv,line.strip()))
            return None
        try:
            anid = int(parts[2])
        except Exception as e:
            print("Error: cannot convert Annotation ID (%s) to integer... \"%s\"" % (anid,line.strip()))
            return None
        
        name = parts[3].strip()
        name = name.lower().split("(")[0].strip().replace(" ", "_").replace("-","_")

        if len(parts[4]) == 0:
            value = -1
        else:
            try:
                value = int(parts[4])
            except Exception as e:
                print("Error: cannot convert Packet Type (%s) to integer... \"%s\"" % (value,line.strip()))
                return None

        ptype['name'] = name
        ptype['value'] = value
        ptype['id'] = idv
        ptype['anid'] = anid

        #Check uniquness
        found = True
        i = 2
        oldname = ptype['name']
        while found:
            found = False
            for t in self.types:
                if t['name'] == ptype['name']:
                    found = True
                    ptype['name'] = oldname + str(i)
                if t['value'] == ptype['value']:
                    return None
            i += 1

        return ptype


    def _parse_relation(self, line):
        rel = dict()
        parts = line.split(";")
        if len(parts) != 11:
            return None

        #Get Relationship/Attribute Type
        rel_type = parts[6].strip()
        if rel_type == "None":
            return None
        rel['type'] = rel_type
        
        #Get Entity IDs
        entity_anid0 = parts[7].strip()
        if entity_anid0 != "None":
            try:
                entity_anid0 = int(entity_anid0)
            except Exception as e:
                print("Error: cannot convert Annotation ID (%s) to integer... \"%s\"" % (entity_anid0,line.strip()))
                return None
        else:
            entity_anid0 = None
        entity_anid1 = parts[8].strip()
        if entity_anid1 != "None":
            try:
                entity_anid1 = int(entity_anid1)
            except Exception as e:
                print("Error: cannot convert Annotation ID (%s) to integer... \"%s\"" % (entity_anid1,line.strip()))
                return None
        else:
            entity_anid1 = None
        entity_anid2 = parts[9].strip()
        if entity_anid2 != "None":
            try:
                entity_anid2 = int(entity_anid2)
            except Exception as e:
                print("Error: cannot convert Annotation ID (%s) to integer... \"%s\"" % (entity_anid2,line.strip()))
                return None
        else:
            entity_anid2 = None

        #Get Sentence
        sent = parts[10].strip()

        #Determine whether Attribute or Relationship
        if entity_anid0 != None and entity_anid1 == None and entity_anid2 == None:
            #Attribute
            rel['anid'] = entity_anid0
            rel['attribute'] = True
            rel['relationship'] = False
            if rel['type'] == "r_multiple":
                val = self.get_additional_value("r_multiple",sent)
                if val is not None:
                    rel['value'] = val
        elif entity_anid0 == None and entity_anid1 != None and entity_anid2 != None:
            #Relationship
            rel['anid1'] = entity_anid1
            rel['anid2'] = entity_anid2
            rel['attribute'] = False
            rel['relationship'] = True
            if rel['type'] == "r_range":
                val = self.get_additonal_value("r_range",sent)
                if val is not None and type(val) is list and len(val) == 2:
                    rel['value1'] = val[0]
                    rel['value2'] = val[1]
            if rel['type'] == "r_significant":
                val = self.get_additional_value("r_significant",sent)
                if val is not None:
                    rel['value'] = val
        else:
            return None

        #Check for duplicates
        if rel in self.attributes or rel in self.relationships:
            return None
        return rel

    def get_additional_value(self, rtype, sent):
        if rtype == "r_multiple":
            mo = re.search("([0-9]+) bits?",sent)
            if type(mo) is not NoneType:
                v = int(mo.group(1))
                if v > 7:
                    v = v/8
                return v
            mo = re.search("([0-9]+)-bits?",sent)
            if type(mo) is not NoneType:
                v = int(mo.group(1))
                if v > 7:
                    v = v/8
                return v
            mo = re.search("([0-9]+) bytes?",sent)
            if type(mo) is not NoneType:
                v = int(mo.group(1))
                return v
            mo = re.search("([0-9]+)-bytes?",sent)
            if type(mo) is not NoneType:
                v = int(mo.group(1))
                return v
            mo = re.search("([0-9]+)",sent)
            if type(mo) is not NoneType:
                v = int(mo.group(1))
                return v
            return None
        elif rtype == "r_significant":
            if sent.find(" not set ") >= 0:
                return 0
            if sent.find(" unset ") >= 0:
                return 0
            if sent.find(" set ") >= 0:
                return 1
            return None
        elif rtype == "r_range":
            mo = re.findall("([0-9]+)",sent)
            vals = []
            for m in mo:
                vals.append(int(m.group(1)))
            if len(vals) < 2:
                return None
            m1 = max(vals)
            m2 = min(vals)
            vals = [m2,m1]
            return vals
        else:
            print("Warning: %s is not really supported right now..." % (rtype))
            return None

    def _fix_and_merge_attributes(self):
        attrs_by_type = dict()
        new_attrs = []
        type_attr = None
        length_attr = None
        checksum_attr = None

        #Match to fields
        for f in self.fields: #Ensure that attributes are ordered from first field to last field
            for t in self.attributes:
                if f['anid'] != t['anid']:
                    continue
                if t['type'] == "r_multiple" and 'value' not in t:
                    continue
                t['field'] = f['id']
                t['name'] = f['name']

                if t['type'] not in attrs_by_type:
                    attrs_by_type[t['type']] = []
                attrs_by_type[t['type']].append(t)

        #Some attributes only occur once
        for ftype in attrs_by_type:
            if ftype == "r_packet_type":
                #Packet type field attribute
                real = None
                if len(attrs_by_type[ftype]) > 1:
                    #Multiple possible attributes
                    alts = []
                    #Pick by guessing name
                    for t in attrs_by_type[ftype]:
                        pk = self.field_ids[t['field']]
                        if pk['length'] <= 0 or pk['length'] > 16:
                            continue
                        if 'altlength' in pk:
                            continue
                        alts.append(pk['id'])
                        if t['name'].find("type") >= 0:
                            real = t
                            break
                    if real == None:
                        #Pick first field with valid constraints
                        for t in attrs_by_type[ftype]:
                            pk = self.field_ids[t['field']]
                            if pk['length'] <= 0:
                                continue
                            if 'altlength' in pk:
                                continue
                            real = t
                            break
                    if real == None:
                        #Just pick the first field
                        real = attrs_by_type[ftype][0]
                    real['alts'] = alts
                else:
                    #Only one option
                    real = attrs_by_type[ftype][0]
                type_attr = real
                new_attrs.append(real)
            elif ftype == "r_header_length":
                #Header length field attribute
                real = None
                if len(attrs_by_type[ftype]) > 1:
                    #Multiple possible attribute
                    alts = []
                    #Pick by guessing name
                    for t in attrs_by_type[ftype]:
                        pk = self.field_ids[t['field']]
                        if pk['length'] <= 1:
                            continue
                        if 'altlength' in pk:
                            continue
                        alts.append(pk['id'])
                        if f['name'].find("offset") >= 0:
                            real = t
                            break
                    if real == None:
                        #Pick first field with valid constraints
                        for t in attrs_by_type[ftype]:
                            pk = self.field_ids[t['field']]
                            if pk['length'] <= 1:
                                continue
                            if 'altlength' in pk:
                                continue
                            real = t
                            break
                    if real == None:
                        #Just pick the first field
                        real = attrs_by_type[ftype][0]
                    real['alts'] = alts
                else:
                    #Only one option
                    real = attrs_by_type[ftype][0]
                length_attr = real
                new_attrs.append(real)
            elif ftype == "r_checksum":
                #Header checksum field attribute
                real = None
                if len(attrs_by_type[ftype]) > 1:
                    #Multiple possible attributes
                    alts = []
                    #Pick by guessing name and length
                    for t in attrs_by_type[ftype]:
                        pk = self.field_ids[t['field']]
                        if pk['length'] <= 8:
                            continue
                        alts.append(pk['id'])
                        if real is not None and f['name'].find("checksum") >= 0 and pk['length'] > self.field_ids[real['field']]['length']:
                            real = t
                        if real is None and f['name'].find("checksum") >= 0:
                            real = t
                    if real == None:
                        #Pick first field with valid constraints
                        for t in attrs_by_type[ftype]:
                            pk = self.field_ids[t['field']]
                            if pk['length'] <= 8:
                                continue
                            real = t
                            break
                    if real == None:
                        #Just pick the first field
                        real = attrs_by_type[ftype][0]
                    real['alts'] = alts
                else:
                    #only one option
                    real = attrs_by_type[ftype][0]
                checksum_attr = real
                new_attrs.append(real)
            else:
                #Just pass through all other attributes
                for t in attrs_by_type[ftype]:
                    new_attrs.append(t)

        #Guess type,length,checksum attributes if we don't have them
        if type_attr is None:
            for f in self.fields:
                if f['length'] <= 0 or f['length'] > 8:
                    continue
                if 'altlength' in f:
                    continue
                if f['name'] == "type":
                    attr = {'type':'r_packet_type','field':f['id'],'name':f['name'],'guess':True}
                    new_attrs.append(attr)
                    break
        if length_attr is None:
            for f in self.fields:
                if f['length'] <= 1:
                    continue
                if 'altlength' in f:
                    continue
                if f['name'].find("offset") >= 0:
                    attr = {'type':'r_header_length','field':f['id'],'name':f['name'],'guess':True}
                    new_attrs.append(attr)
                    break
        if checksum_attr is None:
            for f in self.fields:
                if f['length'] <= 8:
                    continue
                if f['name'] == "checksum":
                    attr = {'type':'r_checksum','field':f['id'],'name':f['name'],'guess':True}
                    new_attrs.append(attr)
                    break

        #Remove conflicting attributes
        self.attributes = []
        for t in new_attrs:
            if type_attr is not None and t['field'] == type_attr['field'] and t['type'] != 'r_packet_type':
                continue
            if length_attr is not None and t['field'] == length_attr['field']:
                if t['type'] != 'r_header_length' and t['type'] != 'r_multiple':
                    continue
            if checksum_attr is not None and t['field'] == checksum_attr['field'] and t['type'] != 'r_checksum':
                continue
            self.attributes.append(t)

        #Override type attribute, if needed
        if self.type_field is not None:
            field = None
            for f in self.fields:
                if f['name'] == self.type_field:
                    field = f
                    break
            if field is not None:
                for t in self.attributes:
                    if t['type'] == 'r_packet_type':
                        del t
                        break
                self.attributes.append({'type':'r_packet_type','field':field['id'],'name':field['name'],'forced':True})

        #Label attributes in fields
        for t in self.attributes:
            f = self.field_ids[t['field']]
            f[t['type']] = True

        #Tags are ordered by type (arbitrarily) then in field order
        return


    def _fix_and_merge_relationships(self):
        f2f_rels = []
        f2t_rels = []

        #Attempt to guess r_significant value
        for r in self.relationships:
            if r['type'] == 'r_significant' and 'value' not in r:
                field = None
                for f in self.fields:
                    if f['anid'] == r['anid2']:
                        field = f
                        break
                if field != None:
                    if field['length'] == 1: #if trigger field is one bit, assume it must be set
                        r['value'] = 1

        #Match to fields
        for r in self.relationships:
            if r['type'] == "r_range" and 'value1' not in r and 'value2' not in r:
                continue
            if r['type'] == "r_significant" and 'value' not in r:
                continue
            if r['type'] in ['r_field_present']:
                r['vers'] = 'f2t'
                for f in self.fields:
                    if f['anid'] == r['anid1']:
                        r['field'] = f['id']
                        r['fname'] = f['name']
                for t in self.types:
                    if t['anid'] == r['anid2']:
                        r['type'] = t['id']
                        r['tname'] = t['name']
                if 'field' in r and 'type' in r:
                    f2t_rels.append(r)
            if r['type'] in ['r_offset','r_significant','r_contains','r_greater','r_less', 'r_range']:
                r['vers'] = 'f2f'
                for f in self.fields:
                    if f['anid'] == r['anid1']:
                        r['field1'] = f['id']
                        r['name1'] = f['name']
                    if f['anid'] == r['anid2']:
                        r['field2'] = f['id']
                        r['name2'] = f['name']
                if 'field1' in r and 'field2' in r:
                    f2f_rels.append(r)

        #Filter field2field relationships
        self.relationships = []
        for r in f2f_rels:
            if r['field1'] == r['field2']: #Self-relationships are invalid
                continue

            #cyclical relations of the same type are invalid
            rev = dict(r)
            rev['field1'] = r['field2']
            rev['name1'] = r['name2']
            rev['anid1'] = r['anid2']
            rev['field2'] = r['field1']
            rev['name2'] = r['name1']
            rev['anid2'] = r['anid1']
            if rev in f2f_rels:
                continue
            
            #Can't have a less and greater relationship for same fields
            if r['type'] == 'r_less':
                o = dict(r)
                o['type'] = 'r_greater'
                if o in f2f_rels:
                    continue
            if r['type'] == 'r_greater':
                o = dict(r)
                o['type'] = 'r_less'
                if o in f2f_rels:
                    continue

            if r['type'] == 'r_contains':
                #Check that field1 is larger than field2
                fa = self.field_ids[r['field1']]
                fb = self.field_ids[r['field2']]
                if fa['length'] <= fb['length']:
                    continue
                o = dict(r)

            #Check for conflicts with attributes
            found = False
            for a in self.attributes:
                if a['field'] == r['field1'] or a['field'] == r['field2']:
                    if a['type'] == 'r_checksum':
                        found = True
                        break
                    if a['type'] == 'r_mbz':
                        found = True
                        break
                    if r['type'] == 'r_contains' and a['type'] in ['r_port','r_sequence_number']:
                        found = True
                        break
            if found:
                continue

            self.relationships.append(r)

        #r_contains conflicts with r_greater, r_less, r_range, r_offset
        f2f_map = {}
        for r in self.relationships:
            if r['field1'] not in f2f_map:
                f2f_map[r['field1']] = []
            f2f_map[r['field1']].append(r)
        self.relationships = []
        for k in f2f_map.keys():
            found = None
            for r in f2f_map[k]:
                if r['type'] == 'r_contains':
                    found = r
                    break
            if found == None:
                for x in f2f_map[k]:
                    self.relationships.append(x)
            else:
                for x in f2f_map[k]:
                    if x['type'] not in ['r_greater', 'r_less', 'r_range', 'r_offset']:
                        self.relationships.append(x)


        #Filter field2type relations
        for r in f2t_rels:
            #Check for conflicts with attributes
            found = False
            for a in self.attributes:
                if a['field'] == r['field']:
                    if a['type'] in [ 'r_checksum', 'r_packet_type', 'r_header_length']:
                        found = True
                        break
            if not found:
                self.relationships.append(r)
            

        #Label relationships in fields
        for r in self.relationships:
            if r['vers'] == 'f2f':
                f = self.field_ids[r['field1']]
                if r['type'] not in f:
                    f[r['type']] = []
                f[r['type']].append({'name2':r['name2'],'field2':r['field2']})
                f = self.field_ids[r['field2']]
                if r['type'] not in f:
                    f[r['type']] = []
                f[r['type']].append({'name1':r['name1'],'field1':r['field1']})
            elif r['vers'] == 'f2t':
                f = self.field_ids[r['field']]
                if r['type'] not in f:
                    f[r['type']].append({'type':r['type'], 'tname':r['tname']})


        #Apply r_contains
        for r in self.relationships:
            if r['type'] == 'r_contains':
                f = self.field_ids[r['field2']]
                if f in self.fields:
                    self.sub_fields.append(f)
                    self.fields.remove(f)
        return

    def _build_masks(self):
        #Collect Subfields by Top Field
        for f in self.sub_fields:
            k = f['r_contains'][0]['field1']
            if k not in self.mask_fields:
                self.mask_fields[k] = []
            self.mask_fields[k].append(f)
        
        #Compute shifts
        for t in self.mask_fields:
            if self.field_ids[t]['length'] in ['16','32']:
                #Fields that will be converted to little endian
                sz = 0
                for f in self.mask_fields[t]:
                    f['shift'] = sz
                    sz+= f['length']
                if sz > self.field_ids['t']['length']:
                    print("Warning: Too many subfields for field (%s)(%s)" % (self.field_ids[t]['name'],self.mask_fields[t]))
                    self.mask_fields[t] = []
            else:
                #Fields assumed to be big endian
                sz = self.field_ids[t]['length']
                for f in self.mask_fields[t]:
                    sz-= f['length'] 
                    f['shift'] = sz
                if sz < 0:
                    print("Warning: Too many subfields for field (%s)(%s)" % (self.field_ids[t]['name'],self.mask_fields[t]))
                    self.mask_fields[t] = []

        #Filter out entries without subfields
        dkeys = []
        for t in self.mask_fields:
            if len(self.mask_fields[t]) == 0:
                dkeys.append(t)
        for k in dkeys:
            del self.mask_fields[k]
        return
            

    def _clean_fields(self):
        for f in self.fields:
            for a in f.keys():
                if a == 'length':
                    continue
                if a == 'name':
                    continue
                if a == 'id':
                    continue
                if a == 'anid':
                    continue
                if a == 'altlength':
                    continue
                del f[a]

    def _clean_types(self):
        for t in self.types:
            for a in t.keys():
                if a == 'value':
                    continue
                if a == 'name':
                    continue
                if a == 'id':
                    continue
                if a == 'anid':
                    continue
                del[a]

        if len(self.types) <= 1:
            self.types = []

    def _build_pkts(self):
        if len(self.types) == 0:
            #No types
            #Single pkt format
            
            #pkt format
            pkfmt = []
            variable = False
            for f in self.fields:
                if f['length'] == 0:
                    f['excluded'] = True
                    continue
                if f['length'] == -1:
                    f['excluded'] = True
                    variable = True
                    continue
                if variable:
                    continue
                pkfmt.append(f)

            #Check for Type field
            has_type = False
            typefield = None
            for f in self.fields:
                if 'r_packet_type' in f:
                    has_type = True
                    typefield = f
                    break
            
            if has_type:
                #Has a defined type
                types = (2**typefield['length'])
                for i in range(0,types):
                    name = "type%d" % (i)
                    if i == 0:
                        name = "BaseMessage"
                    pkt = {'fields':pkfmt,'type':i, 'name':name}
                    self.pkts[name] = pkt
            else:
                #No type
                self.pkts['BaseMessage'] = {'fields':pkfmt,'type':None, 'name':'BaseMessage'}
        else:
            #Packet types
            #Possibly multiple packet formats

            for t in self.types:
                #pkt format
                pkfmt = []
                variable = False
                for f in self.fields:
                    #Handle fields that may not be present
                    found = False
                    if 'r_field_present' in f:
                        for o in f['r_field_present']:
                            if o['type'] == t['id']:
                                found = True
                                break
                        if not found:
                            continue

                    #Handle invalid length fields
                    if f['length'] == 0:
                        f['excluded'] = True
                        continue

                    #Handle variabl length fields
                    if f['length'] == -1:
                        f['excluded'] = True
                        variable = True
                        continue
                    if variable:
                        continue

                    pkfmt.append(f)

                #Check for Type field
                has_type = False
                for f in pkfmt:
                    if 'r_packet_type' in f:
                        has_type = True
                        break

                if not has_type:
                    print("Error: No packet type field found in packet type \"%s\"" % (t['name']))
                
                #Create Packet Type
                pkt = {'fields':pkfmt, 'type':t['value'], 'name':t['name']}
                self.pkts[t['name']] = pkt

            #Add BaseMessage
            pktfmt = []
            has_type = False
            for f in self.fields:
                if 'r_field_present' in f:
                    continue
                if f['length'] == 0:
                    continue
                if f['length'] == -1:
                    continue
                if 'r_packet_type' in f:
                    has_type = True
                pktfmt.append(f)

            if not has_type:
                print("Error: No packet type in BaseMessage!")

            pkt = {'fields':pktfmt, 'name':'BaseMessage', 'type':-1}
            self.pkts['BaseMessage'] = pkt

        return


    def possible_type_fields(self):
        possible = []

        #Look for existing attributes
        for t in self.attributes:
            if t['type'] == 'r_packet_type':
                f = self.field_ids[t['field']]
                if f is None:
                    continue
                if 'guess' not in f:
                    possible.append(f['name'])
        if len(possible) > 0:
            return possible

        #Guess from fields
        for f in self.fields:
            if f['length'] <= 0 or f['length'] > 16:
                continue
            if 'altlength' in f:
                continue
            if 'subfields' in f:
                continue
            found = False
            for t in self.attributes:
                if t['field'] == f['id']:
                    if t['type'] == "r_checksum" and 'guess' not in f:
                        found = True
                    if t['type'] == "r_header_length" and 'guess' not in f:
                        found = True
                    if t['type'] == 'r_port':
                        found = True
                    if t['type'] == 'r_sequence_number':
                        found = True
                    break
            if found:
                continue
            for v in self.pkts.values():
                if f not in v['fields']:
                    found = True
                    break
            if found:
                continue
            possible.append(f['name'])

        return possible

    def output_format(self, format_file):
        #Write Header
        if self.is_transport:
            format_file.write("DEFINE IS_TRANSPORT yes\n")
            format_file.write("DEFINE IP_PROTO_NUM %s\n" % (self.ipnum))

        #Label Special fields
        ports = 0
        seq = 0
        for t in self.attributes:
            if t['field'] not in self.field_ids:
                continue
            field = self.field_ids[t['field']]
            if field['length'] <= 0:
                continue
            if t['type'] == 'r_checksum':
                if 'subfields' in field:
                    print("Warning: Multiple implementation fields for one logical field not supported in attribute (%s)!!" % (t))
                    continue
                format_file.write("DEFINE CHECKSUM_FIELD %s\n" % (field['name']))
            elif t['type'] == 'r_packet_type':
                if 'subfields' in field:
                    print("Warning: Multiple implementation fields for one logical field not supported in attribute (%s)!!" % (t))
                    continue
                format_file.write("DEFINE TYPE_FIELD %s\n" % (field['name']))
            elif t['type'] == 'r_header_length':
                if 'subfields' in field:
                    print("Warning: Multiple implementation fields for one logical field not supported in attribute (%s)!!" % (t))
                    continue
                format_file.write("DEFINE SIZE_FIELD %s\n" % (field['name']))
            elif t['type'] == 'r_multiple' and 'r_header_length' in field:
                if 'subfields' in field:
                    print("Warning: Multiple implementation fields for one logical field not supported in attribute (%s)!!" % (t))
                    continue
                format_file.write("DEFINE SIZE_MULT %d\n" % (t['value']))
            elif t['type'] == 'r_port' and ports == 1:
                if 'subfields' in field:
                    print("Warning: Multiple implementation fields for one logical field not supported in attribute (%s)!!" % (t))
                    continue
                format_file.write("DEFINE DEST_PORT_FIELD %s\n" % (field['name']))
                ports+=1
            elif t['type'] == 'r_port' and ports == 0:
                if 'subfields' in field:
                    print("Warning: Multiple implementation fields for one logical field not supported in attribute (%s)!!" % (t))
                    continue
                format_file.write("DEFINE SOURCE_PORT_FIELD %s\n" % (field['name']))
                ports+=1
            elif t['type'] == 'r_sequence_number' and seq == 0:
                if 'subfields' in field:
                    field = field['subfields'][0]
                format_file.write("DEFINE SEQUENCE_FIELD %s\n" % (field['name']))
                i = 0
                if len(self.pkts.keys()) > 0:
                    pkts_keys = list(self.pkts.keys())
                    for f in self.pkts[pkts_keys[0]]['fields']:
                        if f['name']==field['name']:
                            format_file.write("DEFINE SEQUENCE_FIELD_NUM %i\n" % (i))
                            break
                        i+= 1
                seq+=1
            elif t['type'] == 'r_sequence_number' and seq == 1:
                if 'subfields' in field:
                    field = field['subfields'][0]
                format_file.write("DEFINE ACKNOWLEDGEMENT_FIELD %s\n" % (field['name']))
                i=0
                if len(self.pkts.keys()) > 0:
                    pkts_keys = list(self.pkts.keys())
                    for f in self.pkts[pkts_keys[0]]['fields']:
                        if f['name']==field['name']:
                            format_file.write("DEFINE ACKNOWLEDGEMENT_FIELD_NUM %i\n" % (i))
                            break
                        i+= 1
                seq+=1
            elif t['type'] == 'r_sequence_number' and seq == 2:
                if 'subfields' in field:
                    field = field['subfields'][0]
                format_file.write("DEFINE WINDOW_FIELD %s\n" % (field['name']))
                i=0
                if len(self.pkts.keys()) > 0:
                    pkts_keys = list(self.pkts.keys())
                    for f in self.pkts[pkts_keys[0]]['fields']:
                        if f['name']==field['name']:
                            format_file.write("DEFINE WINDOW_FIELD_NUM %i\n" % (i))
                            break
                        i+= 1
                seq+=1
        format_file.write("\n\n")


        #Output Attributes
        for t in self.attributes:
            if t['field'] not in self.field_ids:
                continue
            field = self.field_ids[t['field']]
            if field['length'] <= 0:
                continue
            if 'subfields' in field:
                if 'value' in t or 'value1' in t or 'value2' in t:
                    print("Warning: Multiple implementation fields for one logical field not supported in attribute (%s)!!" % (t))
                    continue
                for s in set([x['name'] for x in field['subfields']]):
                    format_file.write("ATTR %s %s\n" % (t['type'],s))
            else:
                if 'value' in t:
                    format_file.write("ATTR %s %s %s\n" % (t['type'],field['name'],str(t['value'])))
                elif 'value1' in t and 'value2' in t:
                    format_file.write("ATTR %s %s %s %s\n" % (t['type'],field['name'],str(t['value1']),str(t['value2'])))
                else:
                    format_file.write("ATTR %s %s\n" % (t['type'],field['name']))

        format_file.write("\n\n")

        #Output Relationships
        for r in self.relationships:
            if r['field1'] not in self.field_ids or r['field2'] not in self.field_ids:
                continue
            field1 = self.field_ids[r['field1']]
            if field1['length'] <= 0:
                continue
            field2 = self.field_ids[r['field2']]
            if field2['length'] <= 0:
                continue
            if 'subfields' in field1:
                if 'value' in r:
                    print("Warning: Multiple implementation fields for one logical field not supported in relationship (%s)!!" % (r))
                    continue
                for s1 in set([x['name'] for x in field1['subfields']]):
                    if 'subfields' in field2:
                        for s2 in set([x['name'] for x in field2['subfields']]):
                            format_file.write("REL %s %s %s\n" % (r['type'],s1,s2))
                    else:
                            format_file.write("REL %s %s %s\n" % (r['type'],s1,field2['name']))
            else:
                    if 'subfields' in field2:
                        if 'value' in r:
                            print("Warning: Multiple implementation fields for one logical field not supported in relationship (%s)!!" % (r))
                            continue
                        for s2 in set([x['name'] for x in field2['subfields']]):
                            format_file.write("REL %s %s %s\n" % (r['type'],field1['name'],s2))
                    else:
                        if 'value' in r:
                            format_file.write("REL %s %s %s %s\n" % (r['type'],field1['name'],field2['name'],str(r['value'])))
                        else:
                            format_file.write("REL %s %s %s\n" % (r['type'],field1['name'],field2['name']))
        format_file.write("\n\n")


        #Output Subfields
        for f in self.mask_fields:
            topfield = self.field_ids[f]
            if "subfields" in topfield:
                print("Warning: Multiple implementation fields for one logical field not supported with subfields! (%s)!!" % (topfield['name']))
            for subfield in self.mask_fields[f]:
                format_file.write("SUB %s %s %s %s\n" % (subfield['name'],topfield['name'],subfield['shift'],subfield['length']))
        format_file.write("\n\n")
                

        #Output pkt formats
        for k in self.pkts:
            format_file.write("%s {\n" % (k))
            for i in range(0,len(self.pkts[k]['fields'])):
                f = self.pkts[k]['fields'][i]
                s = self._format_field(f,self.pkts[k]['type'])
                if s is not None:
                    format_file.write("%s\n" % s)
                else:
                    print("Error: Failed to format field %s correctly!\n" % (f['name']))
                    return
            format_file.write("}\n\n")

    def _fix_fields(self, fields):
        newfields = []
        bflst = []
        bfsize = 0
        for f in fields:
            if bfsize > 0:
                #Try as bitfield
                bflst.append(f)
                bfsize+=f['length']
                tmpf,bflst,bfsize =  self._handle_bitfields(bflst,bfsize)
                newfields += tmpf
                if bfsize > 64:
                    print("Error: Set of Bitfields of greater than 64 bits! Parts: %s" % (str(bflst)))
                    return newfields,False
            elif f['length'] == 64 or f['length'] == 32 or f['length'] == 16 or f['length'] == 8:
                #Normal field
                newfields.append(f)
            elif f['length'] % 8 == 0:
                #Integral, but non-native byte width
                tmpf = self._handle_non_native_widths(f)
                newfields +=tmpf
            elif f['length'] % 8 != 0:
                #Bitfield
                bflst.append(f)
                bfsize+=f['length']
                tmpf,bflst,bfsize =  self._handle_bitfields(bflst,bfsize)
                newfields += tmpf
            else:
                #Unknown
                print("Error: Unsupported field width (%d,%s)" % (f['length'],f['name']))
                return newfields,False

        if bfsize > 0:
            tmpf,bflst,bfsize =  self._handle_bitfields(bflst,bfsize)
            newfields += tmpf
        if bfsize > 0:
            print("Error: Incomplete bitfield!! Parts: %s" % (str(bflst)))
        return newfields,True


    def _handle_non_native_widths(self, field):
        newfields = []
        width = field['length']

        i=1
        while width >= 64:
            f = dict(field)
            f['name'] = f['name'] + "{0:03d}".format(i)
            f['length'] = 64
            if 'subfields' in f:
                del f['subfields']
            newfields.append(f)
            width -=64
            i += 1
        while width >= 32:
            f = dict(field)
            f['name'] = f['name'] + "{0:03d}".format(i)
            f['length'] = 32
            if 'subfields' in f:
                del f['subfields']
            newfields.append(f)
            width -=32
            i += 1
        while width >= 16:
            f = dict(field)
            f['name'] = f['name'] + "{0:03d}".format(i)
            f['length'] = 16
            if 'subfields' in f:
                del f['subfields']
            newfields.append(f)
            width -=16
            i += 1
        while width >= 8:
            f = dict(field)
            f['name'] = f['name'] + "{0:03d}".format(i)
            f['length'] = 8
            if 'subfields' in f:
                del f['subfields']
            newfields.append(f)
            width -=8
            i += 1

        newfields.reverse()

        if len(newfields) > 1:
            if 'subfields' in field:
                field['subfields'] += newfields
            else:
                field['subfields'] = list(newfields)
        return newfields


    def _handle_bitfields(self, fields, total_size):
        newfields = []
        if total_size == 64 or total_size == 32 or total_size == 16 or total_size == 8:
            cum_size = 0
            fields_in_byte = []

            #Loop over bitfields
            for f in fields:
                f['fsize'] = total_size
                cum_size += f['length']
                fields_in_byte.append(f)

                while cum_size >= 8:
                    #Byte boundary, must reverse these fields
                    #Assumes network byte order
                    remainder = cum_size - 8

                    if remainder > 0:
                        #Must split a field

                        #Grab the field that needs to be split
                        splitf = fields_in_byte.pop()

                        #Split field
                        keep = splitf['length'] - remainder
                        f1 = dict(splitf)
                        f2 = dict(splitf)
                        if 'subfields' in f1:
                            del f1['subfields']
                        if 'subfields' in f2:
                            del f2['subfields']
                        f1['name'] = f1['name'] + '001'
                        f1['length'] = keep
                        f2['name'] = f2['name'] + '002'
                        f2['length'] = remainder
                        newfields.append(f1)

                        #Reverse all other fields
                        for i in range(0,len(fields_in_byte)):
                            newfields.append(fields_in_byte.pop())

                        #Hold onto remainder
                        fields_in_byte.append(f2)
                        cum_size = remainder

                        #Label subfields
                        if 'subfields' in splitf:
                            splitf['subfields'].append(f1)
                            splitf['subfields'].append(f2)
                        else:
                            splitf['subfields'] = []
                            splitf['subfields'].append(f1)
                            splitf['subfields'].append(f2)
                    else:
                        cum_size = 0
                        for i in range(0,len(fields_in_byte)):
                            newfields.append(fields_in_byte.pop())
            fields = []
            total_size = 0
        oldfields = fields
        return newfields, oldfields,total_size

    def _format_field(self,field,  pkttype):
        length = ""
        plen = ""
        if 'fsize' in field:
            if field['fsize'] == 64:
                length = "uint64_t"
                plen = ":%d" % (field['length'])
            elif field['fsize'] == 32:
                length = "uint32_t"
                plen = ":%d" % (field['length'])
            elif field['fsize'] == 16:
                length = "uint16_t"
                plen = ":%d" % (field['length'])
            elif field['fsize'] == 8:
                length = "uint8_t"
                plen = ":%d" % (field['length'])
            else:
                print("Error: Unsupported field width (%d,%d,%s)" % (field['length'],field['fsize'],field['name']))
                return None
        elif field['length'] == 64:
            length = "uint64_t"
        elif field['length'] == 32:
            length = "uint32_t"
        elif field['length'] == 16:
            length = "uint16_t"
        elif field['length'] == 8:
            length = "uint8_t"
        else:
            print("Error: Unsupported field width (%d,%s)" % (field['length'],field['name']))
            return None
            
        if 'r_packet_type' in field:
            return "\t%s %s%s = %d;" % (length,field['name'],plen,pkttype)
        else:
            return "\t%s %s%s;" % (length,field['name'],plen)

def main(args):
    global debug
    #Parse args
    argp = argparse.ArgumentParser(description='Create Inputs for SNAKE')
    argp.add_argument('--verbose','-v',action='count')
    argp.add_argument('--ipnum','-i',type=int,help="IP Protocol Number for this protocol")
    argp.add_argument('--typefield','-t',type=str,help="Name of field indicating packet type")
    argp.add_argument('--possibletypes','-p',action='store_true',default=False, help="Show possible type fields")
    argp.add_argument('entity_file', help="Input: Entity File")
    argp.add_argument('rel_file', help="Input: Relations File")
    argp.add_argument('format_file', help="Output: Format File")
    args = vars(argp.parse_args(args[1:]))
    debug = args['verbose']

    entity_file = None
    try:
        entity_file = open(args['entity_file'],"r")
    except Exception as e:
        print("Failed to Open Entity File %s:%s" % (args['entity_file'],e))
        return
    
    rel_file = None
    try:
        rel_file = open(args['rel_file'],"r")
    except Exception as e:
        print("Failed to Open Relations File %s:%s" % (args['rel_file'],e))
        return

    format_file = None
    try:
        format_file = open(args['format_file'],"w")
    except Exception as e:
        print("Failed to Open Format File %s:%s" % (args['format_file'],e))
        return


    sc = SnakeConverter(is_transport=True,ipnum=args['ipnum'])
    sc.load_entities(entity_file)
    entity_file.close()
    sc.load_relations(rel_file)
    rel_file.close()
    if args['typefield'] is not None:
        sc.set_type_field(args['typefield'])
    sc.process()
    if args['possibletypes'] is True:
        for t in sc.possible_type_fields():
            print(t)
        return
    sc.output_format(format_file)
    format_file.close()



if __name__ == "__main__":
    main(sys.argv)
