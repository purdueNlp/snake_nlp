#!/bin/env python
# Samuel Jero <sjero@purdue.edu>
# 2016-11-21

import sys
import argparse
import string

feat_in_def_section = 26
feat_substring_case = 25
feat_substring_nocase = 24

def accuracy(true_positive, true_negative, total):
    if total <= 0:
        return 0
    return (true_positive + true_negative) / (total *1.0)

def precision(true_positive, false_positive):
    if true_positive + false_positive <= 0:
        return 0
    return true_positive / ((true_positive + false_positive)*1.0)

def recall(true_positive, false_negative):
    if true_positive + false_negative <= 0:
        return 0
    return true_positive / ((true_positive + false_negative)*1.0)

def fscore(true_positive, false_positive, false_negative):
    p = precision(true_positive, false_positive)
    r = recall(true_positive, false_negative)
    if p <= 0 or r <= 0:
        return 0
    return 2*((p*r)/(p+r))

def output_stats(name, tp, fp, fn, tn):
    print "#################"
    print name
    print "-----------------"
    print "Positive Examples: " + str(tp+fn)
    print "Negative Examples: " + str(fp+tn)
    print "-----------------"
    print "True Positives: " + str(tp)
    print "True Negatives: " + str(tn)
    print "False Positives: " + str(fp)
    print "False Negatives: " + str(fn)
    print "-----------------"
    print "Accuracy: " + str(accuracy(tp,tn,tp+fp+fn+tn))
    print "Recall: " + str(recall(tp,fn))
    print "Precision: " + str(precision(tp,fp))
    print "F-score: " + str(fscore(tp,fp,fn))


def main(args):
    global feat_in_def_section, feat_substring_case, feat_substring_nocase
    #Parse Args
    argp = argparse.ArgumentParser(description='ML analysis tool')
    argp.add_argument('ml_file', help="ML output")
    argp.add_argument('truth_file', help="Feature File")
    argp.add_argument('debug_file', help="Debug output from featurizing")
    args = vars(argp.parse_args(args[1:]))

    #Open Files
    mlf = None
    dbgf = None
    truthf = None
    try:
        mlf = open(args['ml_file'],"r")
    except Exception as e:
        print "Error: couldn't Open ML output Input File"
        sys.exit(1)
    try:
        dbgf = open(args['debug_file'],"r")
    except Exception as e:
        print "Error: couldn't Open Debug Featurizing Input File"
        sys.exit(1)
    try:
        truthf = open(args['truth_file'], 'r')
    except Exception as e:
        print "Error: couldn't Open Truth Input File"
        sys.exit(1)

    ml = mlf.readlines()
    dbg = dbgf.readlines()
    truth = truthf.readlines()
    mlf.close()
    dbgf.close()
    truthf.close()

    if len(ml) != len(dbg) or len(ml) != len(truth):
        print "Warning: Number of elements not equal!!!"
        return -1

    total_tp = 0
    total_fp = 0
    total_fn = 0
    total_tn = 0
    sect_tp = 0
    sect_fp = 0
    sect_fn = 0
    sect_tn = 0
    nsect_tp = 0
    nsect_fp = 0
    nsect_fn = 0
    nsect_tn = 0
    subc_tp = 0
    subc_fp = 0
    subc_fn = 0
    subc_tn = 0
    subnc_tp = 0
    subnc_fp = 0
    subnc_fn = 0
    subnc_tn = 0
    for i in range(0,len(ml)):
        (chunk,entity) = dbg[i].split("#####")
        entity = entity.strip()
        pred = float(ml[i])
        tru = float(truth[i].split()[0])
        feat = []
        for f in truth[i].split():
            if not ":" in f:
                continue
            fnum,val = f.split(":")
            feat.append(int(fnum))

        if tru > 0:
            if pred > 0:
                total_tp += 1
            else:
                total_fn += 1
        else:
            if pred > 0:
                total_fp += 1
            else:
                total_tn += 1

        if not feat_in_def_section in feat:
            if tru > 0:
                if pred > 0:
                    nsect_tp += 1
                else:
                    nsect_fn += 1
            else:
                if pred > 0:
                    nsect_fp += 1
                else:
                    nsect_tn +=1

        if feat_in_def_section in feat:
            if tru > 0:
                if pred > 0:
                    sect_tp += 1
                else:
                    sect_fn += 1
            else:
                if pred > 0:
                    sect_fp += 1
                else:
                    sect_tn += 1
        
        if feat_substring_case in feat:
            if tru > 0:
                if pred > 0:
                    subc_tp += 1
                else:
                    subc_fn += 1
            else:
                if pred > 0:
                    subc_fp += 1
                else:
                    subc_tn += 1

        if feat_substring_nocase in feat:
            if tru > 0:
                if pred > 0:
                    subnc_tp += 1
                else:
                    subnc_fn += 1
            else:
                if pred > 0:
                    subnc_fp += 1
                else:
                    subnc_tn += 1
    output_stats("Total", total_tp, total_fp, total_fn, total_tn)
    output_stats("In Section", sect_tp, sect_fp, sect_fn, sect_tn)
    output_stats("Outside Section", nsect_tp, nsect_fp, nsect_fn, nsect_tn)
    output_stats("Substring, case-senstive", subc_tp, subc_fp, subc_fn, subc_tn)
    output_stats("Substring, non-case-senstive", subnc_tp, subnc_fp, subnc_fn, subnc_tn)


        
if __name__ == "__main__":
    main(sys.argv)
