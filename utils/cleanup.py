#!/bin/env python
# Samuel Jero <sjero@purdue.edu>
# 2016-09-23

import sys
import argparse
import string
import re
from types import NoneType

def swaptags(old, new):
    offset = 0
    removed = 0
    whitespace = 0
    while offset < len(new) and (new[offset] == ' ' or new[offset] == '\t'):
        offset += 1

    while old.find("<tag") >= 0:
        s = old.find("<tag")
        e = old.find("/>")
        add = e + 2 - s
        while offset + removed + whitespace + s < len(new) and new[offset + removed + whitespace + s - 1] == ' ' and new[offset + removed + whitespace + s] == ' ':
            whitespace += 1
        new = new[:offset + removed + whitespace +  s] + old[s:e+2] + new[offset + removed + whitespace + s:]
        removed += add
        s1 = old[:s]
        s2 = old[e+2:]
        if len(s1) > 0 and len(s2) > 0 and s1[len(s1)-1] != ' ' and s1[len(s1)-1] != '\t' and s1[len(s1)-1] != '\n' and s1[len(s1)-1] != '-' and s1[len(s1)-1] != '(' and s1[len(s1)-1] != '['and s2[0] != " " and s2[0] != '\t' and s2[0] != '\n' and s2[0] != "," and s2[0] != ':' and s2[0] != ")" and s2[0] != "]" and s2[0] != '.' and s2[0] != "!" and s2[0] != "?" and s2[0] != '-' and s2[0] != "<":
            old = old[:s] + " "  + old[e+2:]
        else:
            old = old[:s] + old[e+2:]
    return new

def main(args):
    #Parse Args
    argp = argparse.ArgumentParser(description='Timing Graph Generator')
    argp.add_argument('formated_file', help="Formatted Input File")
    argp.add_argument('annotated_file', help="Annotated Input File")
    argp.add_argument('out_file', help="Output File")
    args = vars(argp.parse_args(args[1:]))

    #Open Files
    frmtf = None
    anntf = None
    outf = None
    try:
        frmtf = open(args['formated_file'],"r")
    except Exception as e:
        print "Error: couldn't Open Formatted Input File"
        sys.exit(1)
    try:
        anntf = open(args['annotated_file'],"r")
    except Exception as e:
        print "Error: couldn't Open Annotated Input File"
        sys.exit(1)
    try:
        outf = open(args['out_file'], "w")
    except Exception as e:
        print "Error: couldn't Open Output File"
        sys.exit(1)

    frmt = frmtf.readlines()
    annt = anntf.readlines()
    frmtf.close()
    anntf.close()

    aline = 0
    for fline in range(0, len(frmt)):
        if aline >= len(annt):
            outf.write(frmt[fline])
            continue
        while aline < len(annt) and (annt[aline] == "\n" or all(c in string.whitespace for c in annt[aline])):
            aline += 1
        if frmt[fline] == "\n" or all(c in string.whitespace for c in frmt[fline]):
            outf.write(frmt[fline])
            continue
        #print "fline" + str(fline) + " len" + str(len(frmt))
        #print "aline" + str(aline) + " len" + str(len(annt))
        tln = swaptags(annt[aline],frmt[fline])
        outf.write(tln)
        aline += 1

    outf.close()

if __name__ == "__main__":
    main(sys.argv)
