#!/bin/env python
# Samuel Jero <sjero@purdue.edu>
# 2016-11-21

import sys
import argparse
import string

def main(args):
    #Parse Args
    argp = argparse.ArgumentParser(description='Feature analysis tool')
    argp.add_argument('feat_file', help="Feature file")
    argp.add_argument('debug_file', help="Debug output from featurizing")
    args = vars(argp.parse_args(args[1:]))

    #Open Files
    dbgf = None
    featf = None
    try:
        dbgf = open(args['debug_file'],"r")
    except Exception as e:
        print "Error: couldn't Open Debug Featurizing Input File"
        sys.exit(1)
    try:
        featf = open(args['feat_file'], 'r')
    except Exception as e:
        print "Error: couldn't Open Feature File"
        sys.exit(1)

    dbg = dbgf.readlines()
    feat = featf.readlines()
    dbgf.close()
    featf.close()

    if  len(dbg) != len(feat):
        print "Warning: Number of elements not equal!!!"
        return -1
        
    num = 0
    numtrue = 0

    for i in range(0,len(feat)):
        (chunk,entity) = dbg[i].split("#####")
        tru = float(feat[i].split()[0])
        features = []
        for f in feat[i].split():
            if not ":" in f:
                continue
            fnum,val = f.split(":")
            features.append(int(fnum))

        #if tru > 0:
        #    print tru, "###", chunk, "###", entity

        #if  tru > 0 and len(features)==1:
        #    print tru, "###", chunk, "###", entity
        if 24 in features:
             print tru, "###", chunk, "###", entity
             num += 1
             if tru > 0:
                numtrue += 1
            

    print "Num: " + str(num)
    print "Num True: " + str(numtrue)

if __name__ == "__main__":
    main(sys.argv)
