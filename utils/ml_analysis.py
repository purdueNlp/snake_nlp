#!/bin/env python
# Samuel Jero <sjero@purdue.edu>
# 2016-11-21

import sys
import argparse
import string

def main(args):
    #Parse Args
    argp = argparse.ArgumentParser(description='ML analysis tool')
    argp.add_argument('ml_file', help="ML output")
    argp.add_argument('truth_file', help="Feature file")
    argp.add_argument('debug_file', help="Debug output from featurizing")
    args = vars(argp.parse_args(args[1:]))

    #Open Files
    mlf = None
    dbgf = None
    truthf = None
    try:
        mlf = open(args['ml_file'],"r")
    except Exception as e:
        print "Error: couldn't Open ML output Input File"
        sys.exit(1)
    try:
        dbgf = open(args['debug_file'],"r")
    except Exception as e:
        print "Error: couldn't Open Debug Featurizing Input File"
        sys.exit(1)
    try:
        truthf = open(args['truth_file'], 'r')
    except Exception as e:
        print "Error: couldn't Open Truth Input File"
        sys.exit(1)

    ml = mlf.readlines()
    dbg = dbgf.readlines()
    truth = truthf.readlines()
    mlf.close()
    dbgf.close()
    truthf.close()

    if len(ml) != len(dbg) or len(ml) != len(truth):
        print "Warning: Number of elements not equal!!!"
        return -1

    chunk_last = None
    chunk_preds = 0
    chunk_truth = 0
    for i in range(0,len(ml)):
        (chunk,entity) = dbg[i].split("#####")
        entity = entity.strip()
        pred = float(ml[i])
        tru = float(truth[i].split()[0])
        feat = []
        for f in truth[i].split():
            if not ":" in f:
                continue
            fnum,val = f.split(":")
            feat.append(int(fnum))

        #False Positives
        #if tru < 0 and pred > 0:
        #    print str(feat) +"####" +chunk + " #### " + entity
        
        #False Negatives
        if tru > 0 and pred < 0 and 26 in feat:
            print str(feat) +"####" + chunk + " #### " + entity

if __name__ == "__main__":
    main(sys.argv)
