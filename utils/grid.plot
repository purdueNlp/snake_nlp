#sort -n -k2,4 grid_results.txt > tmp.txt
set term png small linewidth 2 medium enhanced
set output "grid.png"
set xlabel "C"
set ylabel "gamma"
set contour
set cntrparam levels incremental 0,0.1,1
unset surface
unset ztics
set yrange[0:0.1]
set view 0,0
set title "Grid"
unset label
set key at screen 0.9,0.9
splot "./tmp.txt" using 2:3:1 with lines
